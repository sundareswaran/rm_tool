<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Login</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-theme.min.css" rel="stylesheet">
<link href="css/sticky-footer-navbar.css" rel="stylesheet">
<script>
function alerts() {
	var alert = location.search.split('alert=')[1] ? location.search.split('alert=')[1] : 'false';
	if(alert === "IncorrectCredentials") {
		document.getElementById("div_alert").innerHTML = '<div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Invalid Login Credetials!</strong> <br/>Please check your Username and Password. <br /> <small><small>Username is your CEC ID</small></small> </div>';
	}
}
</script>
</head>
<body style="min-height: auto;" onload="alerts()">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.jsp">Global Resource Manager</a>
			</div>
		</div>
	</nav>
	<div class="container" style="margin-top: 75px;">
		<div class="row">
			<div class="col-md-4">
				<form name="login" action="LoginAuth" method="post">
					<h2>Login</h2>
					<div id="div_alert"></div>
					<div class="form-group">
						<label for="username">Username</label> <input type="text"
							class="form-control" id="username" placeholder="Username"
							name="email">
					</div>
					<div class="form-group">
						<label for="password1">Password</label> <input type="password"
							class="form-control" id="password1" placeholder="Password"
							name="password">
					</div>
					<div class="checkbox">
						<label> <input type="checkbox">Remember me on this
							computer
						</label>
					</div>
					<button type="submit" class="btn btn-primary">Login</button>
				</form>
				<a href="#">Forgot password?</a>
			</div>
			<div class="col-md-8">
				<h2>What is GRM?</h2>
				<p>Global Resource Manager is a one stop shop for project creation and resource allocation and management in created project. So, do away with unmanageable excel sheets and move onto GRM today!</p>
				<h2>Who can use GRM?</h2>
				<p>GRM has a role based access system in place for</p>
				<ul>
					<li>Project Managers</li>
					<li>Managers</li>
					<li>PMO</li>
					<li>Developers/QA</li>
				</ul>
			</div>
		</div>
	</div>

	<%@include file="projectmanager/master/footer.jsp"%>
	
	<script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>