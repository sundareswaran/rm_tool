<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>API Documentation</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-theme.min.css" rel="stylesheet">
<link href="css/sticky-footer-navbar.css" rel="stylesheet">
</head>
<body style="min-height: auto;" onload="alerts()">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.jsp">Global Resource Manager</a>
			</div>
		</div>
	</nav>
	<div class="container" style="margin-top: 75px;">
		<div class="row">
			<div class="col-md-10">
				<h2>REST API</h2>
				<p>We developed REST API's with the current trend of Smart Phone
					growth in the mind.</p>
				<h2>Who can use?</h2>
				<p>
					<strong>Developers</strong> can make use of the API to develop <strong><i>Native
							Applications</i></strong> for,
				</p>
				<ul>
					<li>Project Managers</li>
					<li>Reporting Managers</li>
				</ul>
			</div>
			<br> <br>
			<div class="col-md-11">
				<h4>Registering to REST API</h4>
				<p>
					Send the CEC ID and password to the /RestLoginAuth page in GET or
					POST. <br> The server will responding with a cookie named "HASH",
					use it for further transactions.<br> Send the cookie along with the GET request,
				</p>
				<p>
					Example<br> <span class="tab"><pre>http://localhost:8081/RestLoginAuth?cecid=ajananda&password=asdfg</pre></span>
				</p>
			</div>
			<div class="col-md-11">
				<h3>For Project Manager,</h3>
				<p>
					1. Getting the active projects at any given quarter, send a GET
					request, with the quarter to,<br>
				<pre>http://localhost:8081/RM_Tool/rest/api/v1/projectmanager/projects/{quarter}</pre>
				</p>
				<p>
					Get Request,<br> <span class="tab"><pre>http://localhost:8081/RM_Tool/rest/api/v1/projectmanager/projects/Q1FY16</pre></span>
				</p>
				<pre>
[
    {
        "name": "AI-Whiptail",
        "id": "2209",
        "status": "ACTIVE",
        "progress": "129",
        "TotalEmployees": "1"
    },
    {
        "name": "1P-EoDB 4.0",
        "id": "9996",
        "status": "ACTIVE",
        "progress": "147",
        "TotalEmployees": "1"
    },
    {
        "name": "Portfolio Management",
        "id": "242",
        "status": "ACTIVE",
        "progress": "67",
        "TotalEmployees": "3"
    }
]
		</pre>
			</div>
			<div class="col-md-11">
				<p>
					2. Getting a project resource allocation details for a quarter,
					send a GET request, with the project id and quarter to,<br>
				<pre>http://localhost:8081/RM_Tool/rest/api/v1/projectmanager/getProjectResource/{project_id}/{quarter}</pre>
				</p>
				<p>
					Get Request,<br> <span class="tab"><pre>http://localhost:8081/RM_Tool/rest/api/v1/projectmanager/getProjectResource/2209/Q1FY16</pre></span>
				</p>
				<pre>
[
    {
        "name": "Sankaranarayanan A. (sankaa)",
        "cec_id": "sankaa",
        "emp_id": "340857",
        "m1": "Sankaranarayanan A. (sankaa)",
        "m2": "25",
        "m3": "50",
        "status": "25"
    },
    {
        "name": "Srikumar Gopal (sgopal)",
        "cec_id": "sgopal",
        "emp_id": "48270",
        "m1": "Srikumar Gopal (sgopal)",
        "m2": "70",
        "m3": "20",
        "status": "65"
    }
]
		</pre>
			</div>
			<div class="col-md-11">
				<p>
					3. Getting a project details, send a GET request, with the project
					id to,<br>
				<pre>http://localhost:8081/RM_Tool/rest/api/v1/projectmanager/getprojdetails/{project_id}</pre>
				</p>
				<p>
					Get Request,<br> <span class="tab"><pre>http://localhost:8081/RM_Tool/rest/api/v1/projectmanager/getprojdetails/2209</pre></span>
				</p>
				<pre>
{
    "id": "2209",
    "start": "2015-10-10 00:00:00",
    "end": "2016-02-01 00:00:00",
    "budget": "76889",
    "deliveryManager": "siganesa",
    "solutionManager": "vsahbas",
    "description": "A project for Al-Whiptail",
    "targetReleaseQuarter": "Q3FY15",
    "project_name": "AI-Whiptail"
}		</pre>
			</div>
			<div class="col-md-11">
				<p>
					4. Getting Employee Number of Reporting to different RM, send a GET
					request, with project id and quarter to,<br>
				<pre>http://localhost:8081/RM_Tool/rest/api/v1/projectmanager/getRMsandEmployeesCount/{project_id}/{quarter}</pre>
				</p>
				<p>
					Get Request,<br> <span class="tab"><pre>http://localhost:8081/RM_Tool/rest/api/v1/projectmanager/getRMsandEmployeesCount/2209/Q3FY16</pre></span>
				</p>
				<pre>
[
    {
        "noOfEmployees": "4",
        "RM_id": "siganesa",
        "RM_name": "Ganesan Sivasamy (siganesa)"
    },
    {
        "noOfEmployees": "1",
        "RM_id": "jingli2",
        "RM_name": "Jing Li (jingli2)"
    }
]		</pre>
			</div>
			<div class="col-md-11">
				<h3>For Reporting Manager,</h3>
				<p>
					1. Getting the List of Employees reporting, their allocation for
					each month in the quarter and the projects they work in, send a GEt
					request to,<br>
				<pre>http://localhost:8081/RM_Tool/rest/api/v1/reportingmanager/repempdetails/{quarter}</pre>
				</p>
				<p>
					Get Request,<br> <span class="tab"><pre>http://localhost:8081/RM_Tool/rest/api/v1/reportingmanager/repempdetails/Q3FY16</pre></span>
				</p>
				<pre>
[
    {
        "name": "Stony Shi (xilshi)",
        "cec_id": "xilshi",
        "m1": "30",
        "m2": "50",
        "m3": "40",
        "TotalProjects": "1",
        "project": {},
        "projects": [
            {
                "name": "AI-Whiptail",
                "id": "2209",
                "projectManager": "Aja Nanda (ajnanda)",
                "PM_id": "ajnanda",
                "M1": "30",
                "M2": "50",
                "M3": "40"
            }
        ]
    },
    {
        "name": "Wil Morga (wilmorga)",
        "cec_id": "wilmorga",
        "project": {},
        "projects": []
    },
    {
        "name": "Sunny Jiang (sunjiang)",
        "cec_id": "sunjiang",
        "project": {},
        "projects": []
    },
    {
        "name": "Lorena Hu (wenjhu)",
        "cec_id": "wenjhu",
        "project": {},
        "projects": []
    }
]		</pre>
			</div>
			<div class="col-md-11">
				<h3>For Reporting Manager,</h3>
				<p>
					1. Getting the List of Employees reporting, their allocation for
					each month in the quarter and the projects they work in, send a GEt
					request to,<br>
				<pre>http://localhost:8081/RM_Tool/rest/api/v1/reportingmanager/repempdetails/{quarter}</pre>
				</p>
				<p>
					Get Request,<br> <span class="tab"><pre>http://localhost:8081/RM_Tool/rest/api/v1/reportingmanager/repempdetails/Q3FY16</pre></span>
				</p>
				<pre>
[
    {
        "name": "Stony Shi (xilshi)",
        "cec_id": "xilshi",
        "m1": "30",
        "m2": "50",
        "m3": "40",
        "TotalProjects": "1",
        "project": {},
        "projects": [
            {
                "name": "AI-Whiptail",
                "id": "2209",
                "projectManager": "Aja Nanda (ajnanda)",
                "PM_id": "ajnanda",
                "M1": "30",
                "M2": "50",
                "M3": "40"
            }
        ]
    },
    {
        "name": "Wil Morga (wilmorga)",
        "cec_id": "wilmorga",
        "project": {},
        "projects": []
    },
    {
        "name": "Sunny Jiang (sunjiang)",
        "cec_id": "sunjiang",
        "project": {},
        "projects": []
    },
    {
        "name": "Lorena Hu (wenjhu)",
        "cec_id": "wenjhu",
        "project": {},
        "projects": []
    }
]		</pre>
			</div>
			<div class="col-md-11">
				<p>
					2. Getting a list of all employees reporting and their allocation
					for each month in the qiven quarter, send a GET request, with
					quarter to,<br>
				<pre>http://localhost:8081/RM_Tool/rest/api/v1/reportingmanager/allempalloc/{quarter}</pre>
				</p>
				<p>
					Get Request,<br> <span class="tab"><pre>http://localhost:8081/RM_Tool/rest/api/v1/reportingmanager/allempalloc/Q3FY16</pre></span>
				</p>
				<pre>
[
    {
        "name": "Stony Shi (xilshi)",
        "cec_id": "xilshi",
        "m1": "30",
        "m2": "50",
        "m3": "40"
    },
    {
        "name": "Ted Wang (yinawang)",
        "cec_id": "yinawang",
        "m1": "0",
        "m2": "0",
        "m3": "0"
    },
    {
        "name": "Wil Morga (wilmorga)",
        "cec_id": "wilmorga",
        "m1": "0",
        "m2": "0",
        "m3": "0"
    },
    {
        "name": "Sunny Jiang (sunjiang)",
        "cec_id": "sunjiang",
        "m1": "0",
        "m2": "0",
        "m3": "0"
    },
    {
        "name": "Lorena Hu (wenjhu)",
        "cec_id": "wenjhu",
        "m1": "0",
        "m2": "0",
        "m3": "0"
    },
    {
        "name": "Xinyu Wang (xinyuwan)",
        "cec_id": "xinyuwan",
        "m1": "0",
        "m2": "0",
        "m3": "0"
    },
    {
        "name": "Sally Li (sallyli)",
        "cec_id": "sallyli",
        "m1": "0",
        "m2": "0",
        "m3": "0"
    }
]</pre>
			</div>
			<div class="col-md-11">
				<p>
					3. Getting the allocated Resources for a Project, send a GEt
					request, with project id and quarter to,<br>
				<pre>http://localhost:8081/RM_Tool/rest/api/v1/reportingmanager/getprojectresources/{project_id}/{quarter}</pre>
				</p>
				<p>
					Get Request,<br> <span class="tab"><pre>http://localhost:8081/RM_Tool/rest/api/v1/reportingmanager/getprojectresources/2209/Q3FY16</pre></span>
				</p>
				<pre>
[
    {
        "name": "Stony Shi (xilshi)",
        "cec_id": "xilshi",
        "emp_id": "212489",
        "m1": "30",
        "m2": "50",
        "m3": "40",
        "reportingManager": "Jing Li (jingli2)",
        "status": "APPROVED"
    },
    {
        "name": "Venkatakishore Bondada (vebondad)",
        "cec_id": "vebondad",
        "emp_id": "351160",
        "m1": "30",
        "m2": "30",
        "m3": "0",
        "reportingManager": "Ganesan Sivasamy (siganesa)",
        "status": "APPROVED"
    },
    {
        "name": "Tridiv Nandi (trnandi)",
        "cec_id": "trnandi",
        "emp_id": "343736",
        "m1": "30",
        "m2": "30",
        "m3": "30",
        "reportingManager": "Ganesan Sivasamy (siganesa)",
        "status": "APPROVED"
    },
    {
        "name": "Viswanatha V Reddy (vvemired)",
        "cec_id": "vvemired",
        "emp_id": "854115",
        "m1": "30",
        "m2": "30",
        "m3": "30",
        "reportingManager": "Ganesan Sivasamy (siganesa)",
        "status": "APPROVED"
    }
]</pre>
			</div>

		</div>
	</div>
	<script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>