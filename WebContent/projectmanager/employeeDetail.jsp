<%@page import="com.cisco.rmtool.ProjectManagerDao"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="com.cisco.rmtool.EmployeeDao"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="master/datejava.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		String cec_id = request.getParameter("cec_id").toString();
		String flag = request.getParameter("flag").toString();
		List<EmployeeData> empData = EmployeeDao.getAllDetail(cec_id, flag);
		List<ProjectData> emp_proj = EmployeeDao.getProjectsAlloc(cec_id, q, y, flag);
		int unnot = ProjectManagerDao.getNumberofUnreadNotifications(id);
		
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="GRM Development Team - Amrita Vishwa Vidyapeetham, Coimbatore">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" href="../css/sticky-footer-navbar.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../css/buttons.dataTables.min.css">
<title>Employee Detail</title>
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<%@include file="master/brand.jsp"%>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
						<li><a href="dashboard.jsp">Dashboard</a></li>
						<li><a href="resourceAllocation.jsp">Request Resource</a></li>
						<li><a href="Reports.jsp">Reports</a></li>
						<li><a href="notifications.jsp">Notifications<% if(unnot != 0) {
							out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
						}%></a></li>
				</ul>
			<%@include file="master/navbar_right.jsp"%>
			</div>
		</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">Employee Details - <% out.print(EmployeeDao.getName(cec_id).split("\\(")[0]); %> </h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<table class="table">
									<%for(int i =0; i <empData.size(); i++) { %>
									   <tr>
									      <td class="col-md-3">Name</td>
									      <td><% out.print(empData.get(i).getName()); %></td>
									   </tr>
									   <tr>
									      <td class="col-md-3">Employee ID</td>
									      <td><% out.print(empData.get(i).getEmp_id()); %></td>
									   </tr>
									   <tr>
									      <td class="col-md-3">CEC_ID</td>
									      <td><% out.print(empData.get(i).getCec_id()); %></td>
									   </tr>
									   <tr>
									      <td class="col-md-3">Designation</td>
									      <td><% out.print(empData.get(i).getDesignation()); %></td>
									   </tr>
									   <tr>
									      <td class="col-md-3">Region</td>
									      <td><% out.print(empData.get(i).getRegion()); %></td>
									   </tr>
									   <tr>
									      <td class="col-md-3">Location</td>
									      <td><% out.print(empData.get(i).getLocation()); %></td>
									   </tr>
									   <tr>
									      <td class="col-md-3">Mail ID</td>
									      <td><% out.print(empData.get(i).getMail()); %></td>
									   </tr>
									   <tr>
									      <td class="col-md-3">Contact Number</td>
									      <td><% out.print(empData.get(i).getContact()); %></td>
									   </tr>
									   <tr>
									      <td class="col-md-3">Reporting Manager</td>
									      <td><% out.print(empData.get(i).getReportingManager()); %></td>
									   </tr>
									   <% } %>
								</table>
								<br />
								<br />
								<table id="EmployeeProjectAllocation" class="display table" style="border-radius: 2px;">
									<thead>
										<tr>
											<th>Project Name</th>
											<th>Project Manager</th>
											<th>M1</th>
											<th>M2</th>
											<th>M3</th>
										</tr>
									</thead>
									<tbody>
									<% for(int i = 0; i <emp_proj.size(); i++) { %>
										<tr>
											<td><%= emp_proj.get(i).getName() %></td>
											<td><%= emp_proj.get(i).getProjectManager().split("\\(")[0] %></td>
											<td><%= emp_proj.get(i).getM1() %></td>
											<td><%= emp_proj.get(i).getM2() %></td>
											<td><%= emp_proj.get(i).getM3() %></td>
										</tr>
									<% } %>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@include file="master/footer.jsp"%>
	<script type="text/javascript" src="../js/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="../js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="../js/jszip.min.js"></script>
	<script type="text/javascript" src="../js/pdfmake.min.js"></script>
	<script type="text/javascript" src="../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/yandex.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
		    $('#EmployeeProjectAllocation').DataTable( {
		    	aaSorting: [],
		        dom: 'Bfrtip',
		        buttons: [
		            {
		                extend:    'copyHtml5',
		                text:      '<i class="fa fa-files-o"></i>',
		                titleAttr: 'Copy'
		            },
		            {
		                extend:    'excelHtml5',
		                text:      '<i class="fa fa-file-excel-o"></i>',
		                titleAttr: 'Excel'
		            },
		            {
		                extend:    'csvHtml5',
		                text:      '<i class="fa fa-file-text-o"></i>',
		                titleAttr: 'CSV'
		            },
		            {
		                extend:    'pdfHtml5',
		                text:      '<i class="fa fa-file-pdf-o"></i>',
		                titleAttr: 'PDF'
		            }
		        ]
		    } );
		} );
	</script>
</body>
</html>
<%
	}
%>