<%@page import="com.cisco.rmtool.ProjectData"%>
<%@page import="com.cisco.rmtool.ProjectManagerDao"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@page import="java.util.List"%>
<%
	List<EmployeeData> data = ProjectManagerDao.getProjectResources("3", "Q1","2015");
%>

<!DOCTYPE HTML>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/tablesort.css">
<link rel="stylesheet" type="text/css"
	href="../css/styles_tablesort.css">
<title>jQuery Tablesort Plugin Demo</title>
</head>
<body>
	<table class="table-sort table-sort-search">
		<thead>
			<tr>
				<th>#</th>
				<th class="table-sort">Name</th>
				<th class="table-sort">M1</th>
				<th class="table-sort">M2</th>
				<th class="table-sort">M3</th>
				<th class="table-sort">Reporting Manager</th>
				<th class="table-sort">Status</th>
			</tr>
		</thead>
		<tbody>
			<%
				for (int i = 0; i < data.size(); i++) {
			%>
			<tr>
				<td>
					<%
						out.print((i + 1) + "");
					%>
				</td>
				<td><a
					onclick="getEmployeeData(<%=data.get(i).getCec_id()%>,<%=data.get(i).getName()%>,<%=data.get(i).getReportingManager()%>);"
					href="javascript:void(0);"> <%
 	out.print(data.get(i).getName());
 %>
				</a></td>
				<td>
					<%
						out.print(data.get(i).getM1());
					%>
				</td>
				<td>
					<%
						out.print(data.get(i).getM2());
					%>
				</td>
				<td>
					<%
						out.print(data.get(i).getM3());
					%>
				</td>
				<td>
					<%
						out.print(data.get(i).getReportingManager());
					%>
				</td>
				<td>
					<%
						out.print(data.get(i).getStatus());
					%>
				</td>
			</tr>
			<%
				}
			%>
		</tbody>
	</table>
	<!-- Table script -->
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script src="../js/yandex.js"></script>
	<script type="text/javascript" src="../js/tablesort.js"></script>
	<script type="text/javascript">
		//This script loads all the necessary data from the tablesort.js file
		$(function() {
			$('table.table-sort').tablesort();
			hljs.initHighlightingOnLoad(); // Syntax Hilighting
		});
	</script>
</body>

</html>
