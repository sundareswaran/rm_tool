<ul class="nav navbar-nav navbar-right">
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false"><%=session.getAttribute("name")%><span
							class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="myProjects.jsp" style="color: black;">My Projects</a></li>
							<li><a href="employeeDetail.jsp?cec_id=<%=session.getAttribute("id")%>&flag=p" style="color: black;">My Profile</a></li>
							<li><a href="logout.jsp" style="color: black;">Logout</a></li>
						</ul></li>
				</ul>