<%@page import="com.cisco.rmtool.QandY"%>
<%@page import="java.util.Calendar"%>
<%
	Calendar c = Calendar.getInstance();
	int month = c.get(Calendar.MONTH);
	int quarter = ((int) ((month + 1) / 3)) + 1;
	String q = "Q" + quarter;
	if (request.getParameter("quarter") != null) {
		q = request.getParameter("quarter");
	}
	int cur_year = c.get(Calendar.YEAR);
	String y = cur_year + "";
	String year = QandY.getCombi("" + month, "" + cur_year);
	if (request.getParameter("year") != null) {
		year = request.getParameter("year");
	}
	QandY qY = new QandY(year);
	q = qY.getQuarter();
	y = qY.getYear();
%>