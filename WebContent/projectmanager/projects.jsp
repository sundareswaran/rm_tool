<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="com.cisco.rmtool.EmployeeDao"%>
<%@page import="com.cisco.rmtool.ProjectManagerDao"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="master/datejava.jsp"%>
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		String pfm_id = request.getParameter("pfm_id");
		String status = request.getParameter("status");
		ProjectData pdata = ProjectManagerDao.getProjectDetails(pfm_id);
		int unnot = ProjectManagerDao.getNumberofUnreadNotifications(id);
		List<EmployeeData> data = ProjectManagerDao.getProjectEmpDetails(pfm_id, q, y, status);
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Project Details for <%=pdata.getPrjName()%> - <%=name.split("\\(")[0]%>(Project Manager)</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" href="../css/sticky-footer-navbar.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../css/buttons.dataTables.min.css">
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<%@include file="master/brand.jsp"%>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="dashboard.jsp">Dashboard</a></li>
					<li><a href="resourceAllocation.jsp">Request Resource</a></li>
					<li><a href="Reports.jsp">Reports</a></li>
					<li><a href="notifications.jsp">Notifications<% if(unnot != 0) {
						out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
					}%></a></li>
				</ul>
				<%@include file="master/navbar_right.jsp"%>
			</div>
		</div>
	</nav>
<div class="container-fluid" style="margin-top: 75px;">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary" style="min-height: 410px;">
				<div class="panel-heading">
					<h4 class="panel-title"><span id="project_name">Project Details - <%=pdata.getPrjName()%></span></h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-8">
							<form>
								<div class="alert alert-info" role="alert" style="padding-top: 5px; padding-bottom: 5px;">
									<table style="background: transparent; box-shadow: none; border: 0px; color: inherit;">
										<tr>
											<td>Quarter - </td>
											<td><%@ include file="master/DateJsp.jsp"%></td>
											<td>&nbsp;Allocation Status - </td>
											<td><select id="status" class="form-control" style="width: auto" name="status" onchange="this.form.submit()">
													<%
														if (request.getParameter("status").equalsIgnoreCase("APPROVED"))
																out.print(
																		"<option selected>APPROVED</option><option>PENDING</option><option>DECLINED</option>");
															else if (request.getParameter("status").equalsIgnoreCase("PENDING"))
																out.print(
																		"<option>APPROVED</option><option selected>PENDING</option><option>DECLINED</option>");
															else
																out.print(
																		"<option>APPROVED</option><option>PENDING</option><option selected>DECLINED</option>");
													%>
												</select>
											</td>
										</tr>
									</table>
								</div>
								<input type="hidden" name="pfm_id" value="<%=pfm_id%>">
							</form>
							<div align="right">
								<button type="button" class="btn btn-primary" onclick="window.location.href='SkillSelection.jsp?pfm_id=<%=pfm_id%>&year=<%=year%>'">Select Manually</button>
								<button type="button" class="btn btn-primary" onclick="window.location.href='cloneResource.jsp?pfm_id=<%=pfm_id%>&year=<%=year%>'">Clone</button>
								<button type="button" class="btn btn-primary" onclick="window.location.href='selectTemporaryResource.jsp?pfm_id=<%=pfm_id%>&year=<%=year%>'">Select Temporary Resource</button>
							</div>
							<br />
							<table id="myTable" class="display table" style="border-radius: 2px;">
								<thead>
									<tr>
										<th class="table-sort">Employee ID</th>
										<th class="table-sort">Employee Name</th>
										<th class="table-sort">Reporting Manager</th>
										<th class="table-sort">M1</th>
										<th class="table-sort">M2</th>
										<th class="table-sort">M3</th>
									</tr>
								</thead>
								<tbody>
									<% for (int i = 0; i < data.size(); i++) { %>
									<tr>
										<td>
											<% out.print(data.get(i).getEmp_id()); %>
										</td>
										<td><a data-toggle="modal" href="employee.jsp?cec_id=<%out.print(data.get(i).getCec_id());%>&flag=<%out.print(data.get(i).getType()); %>" data-target="#myModal" onclick="getEmployeeData(<%=data.get(i).getCec_id()%>,<%=data.get(i).getName()%>,<%=data.get(i).getReportingManager()%>);">
												<% out.print(data.get(i).getName().split("\\(")[0]); %>
											</a>
										</td>
										<td>
											<% out.print(data.get(i).getReportingManager().split("\\(")[0]); %>
										</td>
										<td>
											<% out.print(data.get(i).getM1()); %>
										</td>
										<td>
											<% out.print(data.get(i).getM2()); %>
										</td>
										<td>
											<% out.print(data.get(i).getM3()); %>
										</td>
									</tr>
									<% } %>
								</tbody>
							</table>
						</div>
						<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										 <h4 class="modal-title">Employee Detail</h4>
									</div>
									<div class="modal-body"><div class="te"></div></div>
									<div class="modal-footer">
										<button type="button" class="btn btn-primary">More Details</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="list-group">
								<a class="list-group-item active"><strong><%=pdata.getPrjName()%></strong></a>
								<a class="list-group-item ">Start Date: <strong><%=pdata.getStart().split(" ")[0]%></strong></a>
								<a class="list-group-item ">Target Date: <strong><%=pdata.getEnd().split(" ")[0]%></strong></a>
								<a class="list-group-item ">Budget: <strong>$<%=pdata.getBudget()%></strong></a>
							</div>
							<div class="list-group">
								<%
									List<ProjectData> rdata = ProjectManagerDao.getRMsandEmployeesCount(pfm_id, q, y, status);
										if (rdata.size() == 0) {
											out.print("<center>No data to display charts</center>");
										} else {
											out.print("<div id=\"container\" style=\"min-width: 200px; height: 200px; margin: 0 auto\"></div>");
										}
								%>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<%@include file="master/footer.jsp"%>
	<script type="text/javascript" src="../js/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="../js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="../js/jszip.min.js"></script>
	<script type="text/javascript" src="../js/pdfmake.min.js"></script>
	<script type="text/javascript" src="../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/yandex.js"></script>
	<script type="text/javascript" src="../js/highcharts.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
	    $('#myTable').DataTable( {
	        dom: 'Bfrtip',
	        buttons: [
	            {
	                extend:    'copyHtml5',
	                text:      '<i class="fa fa-files-o"></i>',
	                titleAttr: 'Copy'
	            },
	            {
	                extend:    'excelHtml5',
	                text:      '<i class="fa fa-file-excel-o"></i>',
	                titleAttr: 'Excel'
	            },
	            {
	                extend:    'csvHtml5',
	                text:      '<i class="fa fa-file-text-o"></i>',
	                titleAttr: 'CSV'
	            },
	            {
	                extend:    'pdfHtml5',
	                text:      '<i class="fa fa-file-pdf-o"></i>',
	                titleAttr: 'PDF'
	            }
	        ]
	    } );
	} );
	</script>
	<script type="text/javascript">
	$('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Manager wise Employee Contribution<br />Status: <%out.print(status);%>'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        }
        <%=ProjectManagerDao.getRMsandEmployeesCountChart(pfm_id, q, y, status)%>
    });
	</script>
</body>
<%
	}
%>
</html>