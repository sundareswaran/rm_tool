<%@page import="com.google.gson.Gson"%>
<%@page import="com.cisco.rmtool.ProjectManagerDao"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@page import="java.util.*"%>
<%
	String year = request.getParameter("year");
	String quarter = request.getParameter("quarter");
	if (request.getParameterValues("data[]") != null) {

		String[] arr = request.getParameterValues("data[]");
		String s = "";
		for (int i = 0; i < arr.length; i++) {
			if (i > 0)
				s = s + ",";
			s = s + arr[i];
		}

		List<EmployeeData> data = ProjectManagerDao.getEmployeeForSkill(s, year, quarter);
		StringBuilder builder = new StringBuilder("");
		for (int i = 0; i < data.size(); i++) {
			builder.append(data.get(i).getCec_id());
			builder.append("|");
			builder.append(" &" + data.get(i).getName() + "& ");
			builder.append(" ?" + data.get(i).getReportingManager() + "? ");
			// Add id to the title 
			// This next line has the skills in it add it to the title
			//builder.append("" + data.get(i).getSkills() + "");
			if (i < (data.size() - 1))
				builder.append("|");
		}
		out.println(builder.toString());
	} else {
		Gson gson = new Gson();
		String[] arr = gson.fromJson(ProjectManagerDao.getSkillsJson(), String[].class);
		String s = "";
		for (int i = 0; i < arr.length; i++) {
			if (i > 0)
				s = s + ",";
			s = s + arr[i];
		}
		List<EmployeeData> data = ProjectManagerDao.getEmployeeForSkill(s, year, quarter);
		StringBuilder builder = new StringBuilder("");
		for (int i = 0; i < data.size(); i++) {
			builder.append(data.get(i).getCec_id());
			builder.append("|");
			builder.append(" &" + data.get(i).getName() + "& ");
			builder.append(" ?" + data.get(i).getReportingManager() + "? ");
			// Add id to the title 
			// This next line has the skills in it add it to the title
			// builder.append("" + data.get(i).getSkills() + "");
			if (i < (data.size() - 1))
				builder.append("|");
		}
		out.println(builder.toString());
	}
%>