<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="org.omg.CORBA.PUBLIC_MEMBER"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="com.cisco.rmtool.ProjectManagerDao"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@ include file="master/datejava.jsp"%>
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		String pfm_id = request.getParameter("pfm_id");
		int unnot = ProjectManagerDao.getNumberofUnreadNotifications(id);
%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../css/buttons.dataTables.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.4/css/bootstrap-select.min.css">
<title>Project Manager</title>
<script>
function alerts() {
	var alert = location.search.split('alert=')[1] ? location.search.split('alert=')[1] : 'false';
	if(alert === "success") {
		document.getElementById("div_alert").innerHTML = '<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> <br/>New temporary resource added!</div>';
	} else if(alert === "failed") {
		document.getElementById("div_alert").innerHTML = '<div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Failure!</strong> <br/>New resource was not added!</div>';
	}
}
</script>
</head>
<body style="min-height: auto;"  onload="alerts();">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">

			<!-- Navbar left end, collapsable header Brand info-->
			<%@include file="master/brand.jsp"%>

			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="dashboard.jsp">Dashboard</a></li>
					<li><a href="resourceAllocation.jsp">Request Resource</a></li>
					<li><a href="Reports.jsp">Reports</a></li>
					<li><a href="notifications.jsp">Notifications<% if(unnot != 0) {
						out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
					}%></a></li>
				</ul>

				<!-- Navbar right profile and logout -->
				<%@include file="master/navbar_right.jsp"%>

			</div>
		</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<!--/.sidebar-offcanvas-->
			<!--/row-->
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Welcome, <span id="pm_user_name"><%=name.split("\\(")[0]%></span>
						</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-4">
							<h4>Add New Temporary Resource</h4>
							<form method="post" action="../TempAllocation">
									<table>
										<tbody>
											<tr>
												<td>First Name</td>
												<td><input type="text" class="form-control" id="firstname" name="firstname" required></td>
											</tr>
											<tr>
												<td>Last Name</td>
												<td><input type="text" class="form-control" id="lastname" name="lastname" required></td>
											</tr>
											<tr>
												<td>Email</td>
												<td><input type="email" class="form-control" id="email" name="email" required></td>
											</tr>
											<tr>
											<td>Reporting Manager</td>
											<td>
											<select class="form-control selectpicker" data-live-search="true" name="rm">
											<option value="">&nbsp;</option>
											  <% List<EmployeeData> data = ProjectManagerDao.getReportingManagers();
											  	 for(int i = 0; i<data.size(); i++)
											  	 {
											  		 out.println("<option value=\"" + data.get(i).getCec_id() + "\">" +  data.get(i).getReportingManager().split("\\(")[0] + "</option>");
											  	 }
											  %>
											</select>
											</td>
											</tr>
											<tr>
												<td>Phone</td>
												<td><input type="text" class="form-control" id="phone" name="phone"></td>
											</tr>
											<tr>
												<td>Location</td>
												<td><input type="text" class="form-control" id="location" name="location"></td>
											</tr>
											<tr>
												<td>Company</td>
												<td><input type="text" class="form-control" id="company" name="company"></td>
											</tr>
										</tbody>
									</table>
									<br />
									<input type="submit"  class="btn btn-primary" value="Submit">
									<input type="hidden" class="form-control" id="emptype" name="emptype" value="-">
									<input type="hidden" class="form-control" id="designation" name="designation" value="-">
									<input type="hidden" class="form-control" id="region" name="region" value="-">
									</form>
								</div>
								<div class="col-md-8">
								<div id="div_alert"></div>
								<% List<EmployeeData> emp = ProjectManagerDao.getTempResources(); %>
								<h4>Available Temporary Resource</h4>
								<table id="myTable" class="display table"
										style="border-radius: 2px;">
										<thead>
											<tr>
												<th class="table-sort">Employee ID</th>
												<th class="table-sort">CEC ID</th>
												<th class="table-sort">Name</th>
												<th class="table-sort">Reporting Manager</th>
												<th class="table-sort">Added By</th>
											</tr>
										</thead>
										<tbody>
											<%
												for (int i = 0; i < emp.size(); i++) {
											%>
											<tr>
												<td><%=emp.get(i).getEmp_id()%></td>
												<td><%=emp.get(i).getCec_id()%></td>
												<td><%=emp.get(i).getName()%></td>
												<td><%=emp.get(i).getReportingManager()%></td>
												<td><%=emp.get(i).getComment()%></td>
											</tr>
											<%
												}
											%>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	<!-- Footer -->
	<%@include file="master/footer.jsp"%>

	<!-- Table script -->
	<script type="text/javascript" src="../js/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="../js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="../js/jszip.min.js"></script>
	<script type="text/javascript" src="../js/pdfmake.min.js"></script>
	<script type="text/javascript" src="../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../js/buttons.html5.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/yandex.js"></script>
	<script>
	$(document).ready(function() {
	    $('#myTable').DataTable( {
	    	aaSorting: [],
	        dom: 'Bfrtip',
	        buttons: [
	            {
	                extend:    'copyHtml5',
	                text:      '<i class="fa fa-files-o"></i>',
	                titleAttr: 'Copy'
	            },
	            {
	                extend:    'excelHtml5',
	                text:      '<i class="fa fa-file-excel-o"></i>',
	                titleAttr: 'Excel'
	            },
	            {
	                extend:    'csvHtml5',
	                text:      '<i class="fa fa-file-text-o"></i>',
	                titleAttr: 'CSV'
	            },
	            {
	                extend:    'pdfHtml5',
	                text:      '<i class="fa fa-file-pdf-o"></i>',
	                titleAttr: 'PDF'
	            }
	        ]
	    } );
	} );
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.4/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.4/js/i18n/defaults-*.min.js"></script>
</body>
</html>
<%
	}
%>