<%@page import="com.cisco.rmtool.ProjectManagerDao"%>
<%@page import="com.cisco.rmtool.ProjectManagerDao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.cisco.rmtool.EmployeeDao"%>
<%@page import="com.cisco.rmtool.Notification"%>
<%@page import="com.cisco.rmtool.NotificationData"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	out.print(session.getAttribute("session"));
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		// Here i will use the ProjectManagerProjects class to get the data

		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		int unnot = ProjectManagerDao.getNumberofUnreadNotifications(id);
		List<NotificationData> data = Notification.getUnreadPMNotifications(id);
		List<NotificationData> data1 = Notification.getReadPMNotifications(id);
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Project Manager</title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../css/buttons.dataTables.min.css">
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">

		<!-- Navbar left end, collapsable header Brand info-->
		<%@include file="master/brand.jsp"%>

		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="dashboard.jsp">Dashboard</a></li>
				<li><a href="resourceAllocation.jsp">Request Resource</a></li>
				<li><a href="Reports.jsp">Reports</a></li>
				<li class="active"><a href="notifications.jsp">Notifications<% if(unnot != 0) {
						out.print("<span class=\"label label-danger\">" + unnot + "</span>");
					}%></a></li>
			</ul>

			<!-- Navbar right profile and logout -->
			<%@include file="master/navbar_right.jsp"%>

		</div>
	</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Welcome, <span id="pm_user_name"><%=name.split("\\(")[0]%></span>
						</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<h4>Notifications</h4>
								<ul class="nav nav-tabs">
									<li class="active"><a data-toggle="tab" href="#notiunread">Unread<% if(unnot != 0) { out.print("(" + unnot + ")"); }%></a></li>
									<li><a data-toggle="tab" href="#notiread">Read</a></li>
								</ul>
								<div class="tab-content">
									<div id="notiunread" class="tab-pane fade in active">
										<br />
										<table id="Unread" class="display table" style="border-radius: 2px;">
											<thead>
												<tr>
													<th>Reporting Manager</th>
													<th>Project</th>
													<th>Action Taken</th>
												</tr>
											</thead>
											<%
												for (int i = 0; i < data.size(); i++) {
											%>
											<tr data-toggle="modal" href="remote/notifyResReq.jsp?groupId=<%out.print(data.get(i).getGroupId());%>&reportingManager=<%out.print(data.get(i).getEmployee().getReportingManager());%>&seen_status=unseen" data-target="#myModal">
												<td>
													<%
														out.println(EmployeeDao.getName(data.get(i).getEmployee().getReportingManager()).split("\\(")[0]);
													%>
												</td>
												<td>
													<%
														out.println(data.get(i).getProject().getPrjName());
													%>
												</td>
												<td>
													<%
														out.println(data.get(i).getCount());
													%>
												</td>
											</tr>
											<%
												}
											%>
										</table>
									</div>
									<div id="notiread" class="tab-pane fade">
										<br />
										<table id="Read" class="display table" style="border-radius: 2px;">
											<thead>
												<tr>
													<th>Reporting Manager</th>
													<th>Project</th>
													<th>Action Taken</th>
												</tr>
											</thead>
											<%
												for (int i = 0; i < data1.size(); i++) {
											%>
											<tr data-toggle="modal" href="remote/notifyResReq.jsp?groupId=<%out.print(data1.get(i).getGroupId());%>&reportingManager=<%out.print(data1.get(i).getEmployee().getReportingManager());%>&seen_status=seen" data-target="#myModal">
												<td>
													<%
														out.println(EmployeeDao.getName(data1.get(i).getEmployee().getReportingManager()).split("\\(")[0]);
													%>
												</td>
												<td>
													<%
														out.println(data1.get(i).getProject().getPrjName());
													%>
												</td>
												<td>
													<%
														out.println(data1.get(i).getCount());
													%>
												</td>
											</tr>
											<%
												}
											%>
										</table>
									</div>
								</div>
								<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												 <h4 class="modal-title">Resources Requested</h4>
											</div>
											<div class="modal-body"><div class="te"></div></div>
											<div class="modal-footer">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Footer -->
	<%@include file="master/footer.jsp"%>
	<script type="text/javascript" src="../js/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="../js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="../js/jszip.min.js"></script>
	<script type="text/javascript" src="../js/pdfmake.min.js"></script>
	<script type="text/javascript" src="../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/yandex.js"></script>
	<script>
		$(document).ready(function() {
			var table = $('#Unread').DataTable({
				aaSorting: [],
				dom: 'Bfrtip',
				buttons: [
					{
						extend:    'copyHtml5',
						text:      '<i class="fa fa-files-o"></i>',
						titleAttr: 'Copy'
					},
					{
						extend:    'excelHtml5',
						text:      '<i class="fa fa-file-excel-o"></i>',
						titleAttr: 'Excel'
					},
					{
						extend:    'csvHtml5',
						text:      '<i class="fa fa-file-text-o"></i>',
						titleAttr: 'CSV'
					},
					{
						extend:    'pdfHtml5',
						text:      '<i class="fa fa-file-pdf-o"></i>',
						titleAttr: 'PDF'
					}
				]
			});
		} );
	</script>
	<script>
		$(document).ready(function() {
			$('#Read').dataTable({
				aaSorting: [],
		        dom: 'Bfrtip',
		        buttons: [
		            {
		                extend:    'copyHtml5',
		                text:      '<i class="fa fa-files-o"></i>',
		                titleAttr: 'Copy'
		            },
		            {
		                extend:    'excelHtml5',
		                text:      '<i class="fa fa-file-excel-o"></i>',
		                titleAttr: 'Excel'
		            },
		            {
		                extend:    'csvHtml5',
		                text:      '<i class="fa fa-file-text-o"></i>',
		                titleAttr: 'CSV'
		            },
		            {
		                extend:    'pdfHtml5',
		                text:      '<i class="fa fa-file-pdf-o"></i>',
		                titleAttr: 'PDF'
		            }
		        ]
		    });
		});
	</script>
</body>
</html>
<%
	}
%>