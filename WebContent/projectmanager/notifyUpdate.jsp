<%@page import="com.cisco.rmtool.JsonProvider"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.cisco.rmtool.ProjectManagerDao"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String[] ids = request.getParameterValues("id[]");
		String[] m1 = request.getParameterValues("M1[]");
		String[] m2 = request.getParameterValues("M2[]");
		String[] m3 = request.getParameterValues("M3[]");
		String quarter = request.getParameter("quarter");
		String year = request.getParameter("year");
		String pfm_id = request.getParameter("pfm_id");
		List<EmployeeData> data = new ArrayList<EmployeeData>();
		for (int i = 0; i < ids.length; i++) {
			EmployeeData emp = new EmployeeData();
			emp.setCec_id(ids[i]);
			emp.setM1(m1[i]);
			emp.setM2(m2[i]);
			emp.setM3(m3[i]);
			emp.setQuarter(quarter);
			emp.setYear(year);
			data.add(emp);
		}

		session.setAttribute("dataForUpload", data);
		//out.print(JsonProvider.getGson().toJson(data));
		//response.sendRedirect("c.jsp");
		boolean upload = ProjectManagerDao.enterAllocation(data, pfm_id, id);
		if (upload)
			response.sendRedirect("resourceAllocation.jsp?alert=resourceRequestSuccess");
		else
			response.sendRedirect("resourceAllocation.jsp?alert=resourceRequestFailed");
	}
%>