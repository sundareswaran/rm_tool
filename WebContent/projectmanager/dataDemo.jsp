<%@page import="com.cisco.rmtool.ProjectData"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@page import="java.util.List"%>
<%@page import="com.cisco.rmtool.ReportingManagerDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<p>
		Number of Employees =
		<%=ReportingManagerDao.getNumberOfEmployees("16","Q4", "2016")%></p>
	<p>
		List of employees reporting -
		<%
		out.print(ReportingManagerDao.JSgetReportingEmployee("16", "Q4"));
	%>
	</p>
	<p>
		List of Projects in which RM's employees work in -
		<%
		out.print(ReportingManagerDao.JSgetProjectsOfAllEmployees("16", "Q4"));
	%>
	</p>
	<p>
		Total no of employees in each project under RM-
		<%
		out.print(ReportingManagerDao.JSgetTotalEmployeesInEachProjectUnderRM("16", "Q4"));
	%>
	</p>
	<p>
		Total no of employees in each project (irrespective of the RM) -
		<%
		out.print(ReportingManagerDao.JSgetTotalEmployeesInEachProjectIrrespectiveOfRM("16", "Q4", "2016"));
	%>
	</p>
	<p>
		List of Regions that a RM has employees in -
		<%
		out.print(ReportingManagerDao.JSgetAllRegionsOfEmployees("16", "Q4", "2016"));
	%>
	</p>
	<p>
		Number of Regions =
		<%=ReportingManagerDao.getNoOfRegionsOfAllEmployees("16", "Q4", "2016")%></p>
	<p>
	<p>
		Total number of Employees under the RM who are from a particular
		region -
		<%
		out.print(ReportingManagerDao.JSgetRegionAndEmployees("16","Q4"));
	%>
	</p>
	<p>
		List of Employees under the RM who are under allocated -
		<%
		out.print(ReportingManagerDao.JSgetUnderAllocated("16", "Q4", "2016"));
	%>
	</p>
	<p>
		Number of Employees under the RM who are under allocated =
		<%=ReportingManagerDao.getNoOfUnderAllocatedEmployees("16", "Q4", "2016")%></p>
	<p>
	<p>
		List of Employees under the RM who are over allocated -
		<%
		out.print(ReportingManagerDao.JSgetOverAllocatedEmployees("16", "Q4", "2016"));
	%>
	</p>
	<p>
		Number of Employees under the RM who are Over allocated =
		<%=ReportingManagerDao.getNoOfOverAllocatedEmployees("16", "Q4", "2016")%></p>
	<p>
</body>
</html>