<%@page import="org.omg.CORBA.PUBLIC_MEMBER"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="com.cisco.rmtool.ProjectManagerDao"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@ include file="master/datejava.jsp"%>
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		// Here i will use the ProjectManagerProjects class to get the data
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		String pfm_id =  request.getParameter("pfm_id");
		int unnot = ProjectManagerDao.getNumberofUnreadNotifications(id);
%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Resource Allocation - <%=ProjectManagerDao.getProjectName(pfm_id)%></title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link href="../css/tables.css" rel="stylesheet">


<link rel="stylesheet" type="text/css" href="../css/tablesort.css">
<link rel="stylesheet" type="text/css"
	href="../css/styles_tablesort.css">
<link rel="stylesheet" href="../css/jquery-ui.css">
<link rel="stylesheet"
	href="http://twitter.github.io/typeahead.js/css/examples.css">
<script src="../js/jquery.js"></script>
<script src="../js/jquery-ui.js"></script>
<style type="text/css">
table, th, td {
	border: 10px solid white;
	cellspacing: 10px;
}

#skillOption {
	overflow: hidden;
	padding: 0;
	width: 100vw;
}

select {
	padding: 3px;
	margin: 0;
	width: 100%;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	-webkit-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
	-moz-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
	box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
	background: #f8f8f8;
	color: #888;
	border: none;
	outline: none;
	display: inline-block;
	-webkit-appearance: none;
	-moz-appearance: none;
	appearance: none;
	cursor: pointer;
	-webkit-border-radius: 4px;
}
</style>
<!-- Java script for select list -->
<script>
	function changeAvailable() {
		var values = $.map($("#selectedSkill option"), function(option) {
			return option.value;
		});
		$.ajax({
			url : "../List",
			method : "post",
			data : {
				data : values
	},
			success : function(result) {
				populateAvailable(result);
				// Call function with data tables jquery at bottom.
			}
		});
	}

	function populateAvailable(str) {
		$('#available').empty();
		s2 = document.getElementById("available");
		var optionArray = str.split("|");
		var emp = str.split("&");
		var RM = str;
		RM = RM.split("?");
		var i = 0;
		while (i < (optionArray.length / 2)) {
			var newOption = document.createElement("option");
			newOption.value = optionArray[i * 2];
			newOption.innerHTML = emp[i * 2 + 1];
			newOption.name = "Emp_Id";
			newOption.title = "RM: " + RM[i * 2 + 1];
			newOption.id = RM[i * 2 + 1];
			newOption.setAttribute("data-toggle", "tooltip");
			s2.options.add(newOption);
			i++;
		}
	}
	function submitData() {
		var f = document.getElementById("formList");
		selectAllOptions('selected');
		f.submit();
	}
	function adding(index, v, y, z, title) {
		s1 = document.getElementById("selected");
		s2 = document.getElementById("available");
		var newOption = document.createElement("option");
		newOption.value = v;
		newOption.innerHTML = y;
		newOption.title = title;
		newOption.id = z;
		s1.options.add(newOption);
		s2.remove(index);
	}
	function removing(index, v, y, z, title) {
		s2 = document.getElementById("available");
		s1 = document.getElementById("selected");
		var newOption = document.createElement("option");
		newOption.value = v;
		newOption.innerHTML = y;
		newOption.id = z;
		newOption.title = title;
		s2.options.add(newOption);
		var opt = s1.remove(index);
	}
	function selectAllOptions(selStr) {
		var selObj = document.getElementById(selStr);
		for (var i = 0; i < selObj.options.length; i++) {
			selObj.options[i].selected = true;
		}
	}

	function removeSkill(index) {
		skills = document.getElementById("selectedSkill");
		skills.remove(index);
		changeAvailable();
	}

	function addSkill() {
		text = document.getElementById("skillText");
		skills = document.getElementById("selectedSkill");
		var skill = text.value;
		var newOption = document.createElement("option");
		newOption.value = skill;
		newOption.innerHTML = skill;
		skills.options.add(newOption);
		text.value = "";
		changeAvailable();
	}

	// Jquery function for autocomplete
	var availableSkills =
<%=ProjectManagerDao.getSkillsJson()%>
	;
	$(document).ready(function() {
		$("#skillText").autocomplete({
			source : availableSkills
		});
	});
	$(document).ready(function() {
		$("#skillText").keyup(function(e) {

			if (e.which == 13) {
				addSkill();
				changeAvailable();
			}
		});

	});

	// To load all the data initially
	changeAvailable();

	//Search script for the options
	// Search within the resource list
	var showOnlyOptionsSimilarToText = function(selectionEl, str,
			isCaseSensitive) {
		if (typeof isCaseSensitive == 'undefined')
			isCaseSensitive = true;
		if (isCaseSensitive)
			str = str.toLowerCase();

		var $el = $(selectionEl);

		$el.children("option:selected").removeAttr('selected');
		$el.val('');
		$el.children("option").hide();

		$el.children("option").filter(function() {
			var text = $(this).text();
			text = text + $(this).attr("title");
			text = text + $(this).attr("value");
			if (isCaseSensitive)
				text = text.toLowerCase();

			if (text.indexOf(str) > -1)
				return true;

			return false;
		}).show();

	};
	// Search within the available list
	$(document).ready(function() {
		var timeout;
		$("#SearchBoxAvailable").on("keyup", function() {
			var userInput = $("#SearchBoxAvailable").val();
			window.clearTimeout(timeout);
			timeout = window.setTimeout(function() {
				showOnlyOptionsSimilarToText($("#available"), userInput, true);
			}, 500);

		});

		// Search within the Selected list
		$("#SearchBoxSelected").on("keyup", function() {
			var userInput = $("#SearchBoxSelected").val();
			window.clearTimeout(timeout);
			timeout = window.setTimeout(function() {
				showOnlyOptionsSimilarToText($("#selected"), userInput, true);
			}, 500);

		});

	});
</script>
<script>
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});
</script>
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">

			<!-- Navbar left end, collapsable header Brand info-->
			<%@include file="master/brand.jsp"%>

			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="dashboard.jsp">Dashboard</a></li>
					<li class="active"><a href="resourceAllocation.jsp">Request Resource</a></li>
					<li><a href="Reports.jsp">Reports</a></li>
					<li><a href="notifications.jsp">Notifications<% if(unnot != 0) {
						out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
					}%></a></li>
				</ul>

				<!-- Navbar right profile and logout -->
				<%@include file="master/navbar_right.jsp"%>

			</div>
		</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<!--/.sidebar-offcanvas-->
			<!--/row-->
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Project : <span id="pm_user_name"><%=ProjectManagerDao.getProjectName(pfm_id)%></span> (<%=year%>)
						</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="input-group">
										<input id="skillText" name="skill" type="text"
											class="form-control" autocomplete="on" role="textbox"
											aria-autocomplete="list" aria-haspopup="true"
											placeholder="Add Skill" style="padding-left: 5px;"> <span
											class="input-group-btn">
											<button class="btn btn-primary" id="addSkill"
												onclick="addSkill();" type="button">Go!</button>
										</span>
									</div>
								</div>

								<div class="col-md-4">
									<select class="form-control" id="selectedSkill" name="selected"
										id="skillOption" style="max-height: 80px;" multiple
										onclick="removeSkill(this.selectedIndex);">

									</select>
								</div>
								<div class="col-md-1">
									<button class="btn btn-primary" onclick="submitData();">Submit</button>
								</div>
							</div>
						</div>
						<br />
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<input type="text" class="form-control"
										name="SearchBoxSelected"
										style="width: 100%; padding-left: 5px;" id="SearchBoxSelected"
										placeholder="Search Selected"><br />
									<form id="formList" action="AuthenticateResource.jsp"
										method="post" onsubmit="selectAllOptions('selected');">
										<select class="form-control" id="selected" name="selected[]"
											size="16" multiple
											onclick="removing(this.selectedIndex, this.value, this.options[this.selectedIndex].innerHTML, this.options[this.selectedIndex].id, this.options[this.selectedIndex].title);" required>
										</select>
										<input type="hidden" name="pfm_id" value="<%=pfm_id%>">
										<input type="hidden" name="year" value="<%=year%>">
									</form>
								</div>
								<div class="col-md-6">
									<input type="text" class="form-control"
										name="SearchBoxAvailable"
										style="width: 100%; padding-left: 5px;"
										id="SearchBoxAvailable" placeholder="Search Available"><br />
									<select class="form-control" id="available" name="available"
										size="16" multiple
										onclick="adding(this.selectedIndex, this.value, this.options[this.selectedIndex].innerHTML, this.options[this.selectedIndex].id, this.options[this.selectedIndex].title);">
										<%=ProjectManagerDao.getSkillsJson()%>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<div id="dvDiv"
		style="display: none; position: absolute; padding: 1px; border: 1px solid #333333;; background-color: #fffedf; font-size: smaller; z-index: 999;"></div>
	<iframe id="frm"
		style="display: none; position: absolute; z-index: 998"></iframe>
	<%@include file="master/footer.jsp"%>
	<script type="text/javascript" src="../js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script>
		$(document).ready(function() {
			$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
</body>
</html>
<%
	}
%>