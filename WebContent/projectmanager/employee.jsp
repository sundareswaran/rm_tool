<%@page import="com.cisco.rmtool.ProjectManagerDao"%>
<%@page import="com.cisco.rmtool.EmployeeDao"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@page import="java.util.List"%>
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		String cec_id = request.getParameter("cec_id");
		String flag = request.getParameter("flag");
		List<EmployeeData> empData = EmployeeDao.getAllDetail(cec_id, flag);
%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
<title>Employee Details</title>
</head>
<body>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Employee Detail</h4>
	</div>
	<div class="modal-body">
		<table class="table table-striped">
			<%for(int i =0; i <empData.size(); i++) { %>
										   <tr>
										      <td class="col-md-3">Name</td>
										      <td><% out.print(empData.get(i).getName()); %></td>
										   </tr>
										   <tr>
										      <td class="col-md-3">Employee ID</td>
										      <td><% out.print(empData.get(i).getEmp_id()); %></td>
										   </tr>
										   <tr>
										      <td class="col-md-3">CEC_ID</td>
										      <td><% out.print(empData.get(i).getCec_id()); %></td>
										   </tr>
										   <tr>
										      <td class="col-md-3">Designation</td>
										      <td><% out.print(empData.get(i).getDesignation()); %></td>
										   </tr>
										   <tr>
										      <td class="col-md-3">Region</td>
										      <td><% out.print(empData.get(i).getRegion()); %></td>
										   </tr>
										   <tr>
										      <td class="col-md-3">Location</td>
										      <td><% out.print(empData.get(i).getLocation()); %></td>
										   </tr>
										   <tr>
										      <td class="col-md-3">Mail ID</td>
										      <td><% out.print(empData.get(i).getMail()); %></td>
										   </tr>
										   <tr>
										      <td class="col-md-3">Contact Number</td>
										      <td><% out.print(empData.get(i).getContact()); %></td>
										   </tr>
										   <% } %>
		</table>
	</div>
	<div class="modal-footer">
    <button type="button" class="btn btn-primary" onclick="location.href='employeeDetail.jsp?cec_id=<%out.print(cec_id);%>&flag=<%out.print(flag);%>';">More Details</button>
	</div>
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
</body>
</html>
<%
	}
%>