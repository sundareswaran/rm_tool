<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="org.omg.CORBA.PUBLIC_MEMBER"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="com.cisco.rmtool.ProjectManagerDao"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@ include file="master/datejava.jsp"%>
<%
	out.print(session.getAttribute("session"));
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		String pfm_id = request.getParameter("pfm_id");
		int unnot = ProjectManagerDao.getNumberofUnreadNotifications(id);
		
%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Resource Allocation</title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../css/buttons.dataTables.min.css">
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">

			<!-- Navbar left end, collapsable header Brand info-->
			<%@include file="master/brand.jsp"%>

			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="dashboard.jsp">Dashboard</a></li>
					<li class="active"><a href="resourceAllocation.jsp">Request Resource</a></li>
					<li><a href="Reports.jsp">Reports</a></li>
					<li><a href="notifications.jsp">Notifications<% if(unnot != 0) {
						out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
					}%></a></li>
				</ul>

				<!-- Navbar right profile and logout -->
				<%@include file="master/navbar_right.jsp"%>

			</div>
		</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<%@include file="master/left_sidebar.jsp"%>
			<!--/.sidebar-offcanvas-->
			<!--/row-->
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Welcome, <span id="pm_user_name"><%=name.split("\\(")[0]%></span>
						</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-9">
							<form action="" method="get">
							<input type="hidden" name="pfm_id" value=<%=pfm_id %> >
								<div class="alert alert-info" role="alert"
									style="padding-top: 5px; padding-bottom: 5px;">
									<table
										style="background: transparent; box-shadow: none; border: 0px; color: inherit;">
										<tr>
											<td>Quarter - </td>
											<td><%@ include file="master/DateJsp.jsp"%></td>
										</tr>
									</table>
								</div>
								</form>
								<%
													List<ProjectData> data = ProjectManagerDao.getProjects(id, q, y);

														String[] projectsId = new String[data.size()];
														String[] projectName = new String[data.size()];

														for (int i = 0; i < data.size(); i++) {
															projectsId[i] = data.get(i).getId();
															projectName[i] = data.get(i).getName();
														}

														ProjectData pdata = ProjectManagerDao.getProjectDetails(request.getParameter("pfm_id"));
												%>
								Select a projects resource you want to clone to <b><%=pdata.getPrjName()%></b>
								<br /> <br />
								<div id="pm_as_pm">
									<table id="myTable" class="display table"
										style="border-radius: 2px;">
										<thead>
											<tr>
												<th>Project Name</th>
												<th></th>
											</tr>
										</thead>
										<%
											for (int i = 0; i < data.size(); i++) {
													if (!request.getParameter("pfm_id").equalsIgnoreCase(projectsId[i])) {
										%>
										<tr>
											<td><a
												href="projects.jsp?pfm_id=<%out.print(projectsId[i]);%>&year=<%=year%>&status=APPROVED">
													<%
														out.print(projectName[i]);
													%>
											</a></td>
											<td><a
												href="CloneAuthenticate.jsp?pfm_id=<%out.print(projectsId[i]);%>&to_pfm_id=<%=pdata.getId()%>&year=<%out.print(year);%>">
													Clone this projects resource </a></td>
										</tr>
										<%
											}
												}
										%>
									</table>
								</div>

							</div>
							<div class="col-md-3">
								<div class="list-group">
									<a class="list-group-item active"><strong><%=pdata.getPrjName()%></strong></a>
									<a class="list-group-item ">Start Date: <strong><%=pdata.getStart().split(" ")[0]%></strong></a>
									<a class="list-group-item ">End Date: <strong><%=pdata.getEnd().split(" ")[0]%></strong></a>
									<a class="list-group-item ">Target Release Quarter: <strong><%=pdata.getTargetReleaseQuarter()%></strong></a>
									<a class="list-group-item ">Budget: <strong>$<%=pdata.getBudget()%></strong></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<%@include file="master/footer.jsp"%>

	<!-- Tables script -->
	<script type="text/javascript" src="../js/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="../js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="../js/jszip.min.js"></script>
	<script type="text/javascript" src="../js/pdfmake.min.js"></script>
	<script type="text/javascript" src="../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../js/buttons.html5.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/yandex.js"></script>
	<script>
	$(document).ready(function() {
	    $('#myTable').DataTable( {
	        dom: 'Bfrtip',
	        buttons: [
	            {
	                extend:    'copyHtml5',
	                text:      '<i class="fa fa-files-o"></i>',
	                titleAttr: 'Copy'
	            },
	            {
	                extend:    'excelHtml5',
	                text:      '<i class="fa fa-file-excel-o"></i>',
	                titleAttr: 'Excel'
	            },
	            {
	                extend:    'csvHtml5',
	                text:      '<i class="fa fa-file-text-o"></i>',
	                titleAttr: 'CSV'
	            },
	            {
	                extend:    'pdfHtml5',
	                text:      '<i class="fa fa-file-pdf-o"></i>',
	                titleAttr: 'PDF'
	            }
	        ]
	    } );
	} );
</script>
</body>
</html>
<%
	}
%>