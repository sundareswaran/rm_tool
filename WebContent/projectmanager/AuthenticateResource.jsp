<%@page import="java.util.Calendar"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.lang.reflect.Array"%>
<%@page import="java.util.List"%>
<%@page import="com.cisco.rmtool.ProjectManagerDao"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@page import="com.cisco.rmtool.EmployeeDao"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="master/datejava.jsp"%>
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		String pfm_id =  request.getParameter("pfm_id");
		String[] ids = request.getParameterValues("selected[]");
		int unnot = ProjectManagerDao.getNumberofUnreadNotifications(id);
		List<String> idlist = Arrays.asList(ids);
		List<EmployeeData> data = ProjectManagerDao.getEmployeeForIds(idlist);
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Project Manager</title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../css/buttons.dataTables.min.css">
<style type="text/css">
.fsSubmitButton {
	border-top: 2px solid #a3ceda;
	border-left: 2px solid #a3ceda;
	border-right: 2px solid #4f6267;
	border-bottom: 2px solid #4f6267;
	padding: 10px 20px !important;
	font-size: 14px !important;
	background-color: #c4f1fe;
	font-weight: bold;
	color: #2d525d;
	padding-top: 2px;
}
</style>
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<%@include file="master/brand.jsp"%>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
						<li><a href="dashboard.jsp">Dashboard</a></li>
						<li class="active"><a href="resourceAllocation.jsp">Request Resource</a></li>
						<li><a href="Reports.jsp">Reports</a></li>
						<li><a href="notifications.jsp">Notifications<% if(unnot != 0) {
							out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
						}%></a></li>
				</ul>
				<%@include file="master/navbar_right.jsp"%>
			</div>
		</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">

			<%@include file="master/left_sidebar.jsp"%>
			<!--/.sidebar-offcanvas-->

			<!--/row-->
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Project : <span id="pm_user_name"><%=ProjectManagerDao.getProjectName(pfm_id)%></span> (<%=year%>)
						</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
							<h4>Allocation</h4>
								<p>Please select required resources and add allocation percentage.</p>
								<div id="pm_as_pm">
									<form method="post" action="notifyUpdate.jsp">
										<table id="myTable" class="display table"
										style="border-radius: 2px;">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Employee CEC ID</th>
													<th>Reporting Manager</th>
													<th>M1</th>
													<th>M2</th>
													<th>M3</th>
												</tr>
											</thead>
											<tr>
												<%
													for (int i = 0; i < data.size(); i++) {
												%>
												<td><a href="employeeDetail.jsp?cec_id=<%=data.get(i).getCec_id()%>"><%out.print(data.get(i).getName().split("\\(")[0]); %></a></td>
												<td><% out.print(data.get(i).getCec_id()); %></td>
												<td><%out.print("<span data-toggle=\"tooltip\" data-placement=\"right\" title=" + "\"CEC ID: " + EmployeeDao.getReportingManagerId(data.get(i).getCec_id()) + "\">" + EmployeeDao.getReportingManager(data.get(i).getCec_id()).split("\\(")[0] + "</span>");%></td>
												<td><input type="number" min="0" max="100" class="form-control" name="M1[]" id="allocation" value="0"></td>
												<td><input type="number" min="0" max="100" class="form-control" name="M2[]" id="allocation" value="0"></td>
												<td><input type="number" min="0" max="100" class="form-control" name="M3[]" id="allocation" value="0"></td>
												<input type="hidden" name="id[]" value="<%=data.get(i).getCec_id() %>">
											</tr>
											<%
												}
											%>
										</table>
										<input type="hidden" name="quarter" value="<%=q%>">
										<input type="hidden" name="year" value="<%=y%>">
										<input type="hidden" name="pfm_id"
											value="<%=pfm_id%>"> <br/>
										<input
											type="submit"  class="btn btn-primary" style="float: right;"
											value="Submit">
									</form>
								</div>

							</div>


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<%@include file="master/footer.jsp"%>

	<!-- Table scripts -->
	<script type="text/javascript" src="../js/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="../js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="../js/jszip.min.js"></script>
	<script type="text/javascript" src="../js/pdfmake.min.js"></script>
	<script type="text/javascript" src="../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../js/buttons.html5.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/yandex.js"></script>
	<script>
	$(document).ready(function() {
	    $('#myTable').DataTable( {
	    	aaSorting: [],
	        dom: 'Bfrtip',
	        buttons: [
	            {
	                extend:    'copyHtml5',
	                text:      '<i class="fa fa-files-o"></i>',
	                titleAttr: 'Copy'
	            },
	            {
	                extend:    'excelHtml5',
	                text:      '<i class="fa fa-file-excel-o"></i>',
	                titleAttr: 'Excel'
	            },
	            {
	                extend:    'csvHtml5',
	                text:      '<i class="fa fa-file-text-o"></i>',
	                titleAttr: 'CSV'
	            },
	            {
	                extend:    'pdfHtml5',
	                text:      '<i class="fa fa-file-pdf-o"></i>',
	                titleAttr: 'PDF'
	            }
	        ]
	    } );
	} );
	</script>
	<script>
		$(document).ready(function() {
			$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
</body>
</html>
<%
	}
%>
