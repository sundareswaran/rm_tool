<%@page import="com.cisco.rmtool.ProjectManagerDao"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		int unnot = ProjectManagerDao.getNumberofUnreadNotifications(id);
%>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Project Manager</title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../css/buttons.dataTables.min.css">
<style type="text/css">
table, th, td {
	border: "0";
	padding: 6px;
}
</style>
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">

		<!-- Navbar left end, collapsable header Brand info-->
		<%@include file="master/brand.jsp"%>

		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="dashboard.jsp">Dashboard</a></li>
					<li class="active"><a href="resourceAllocation.jsp">Request Resource</a></li>
					<li><a href="Reports.jsp">Reports</a></li>
					<li><a href="notifications.jsp">Notifications<% if(unnot != 0) {
						out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
					}%></a></li>
			</ul>

			<!-- Navbar right profile and logout -->
			<%@include file="master/navbar_right.jsp"%>

		</div>
	</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<%@include file="master/left_sidebar.jsp"%>
			<!--/.sidebar-offcanvas-->
			<!--/row-->
			<div class="col-md-10">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Welcome, <span id="pm_user_name"><%=name.split("\\(")[0]%></span>
						</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="alert alert-info" role="alert"
									style="padding-top: 5px; padding-bottom: 5px;">
									Current Quarter - <span id="cur_quarter">FY15 - 3rd
										Quarter </span>
								</div>
								<p>Resource Allocation has been sent to Reporting manager
									for approval.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<%} %>
	<!-- Footer -->
	<%@include file="master/footer.jsp"%>
	<script src="../js/bootstrap.min.js"></script>

</body>
</html>
