<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="org.omg.CORBA.PUBLIC_MEMBER"%>
<%@page import="com.cisco.rmtool.ProjectManagerDao"%>
<%@page import="com.cisco.rmtool.EmployeeDao"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@ include file="master/datejava.jsp"%>
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		String pfm_id = request.getParameter("pfm_id");
		String to_pfm_id = request.getParameter("to_pfm_id");
		int unnot = ProjectManagerDao.getNumberofUnreadNotifications(id);
%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Resource Allocation</title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../css/buttons.dataTables.min.css">
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<%@include file="master/brand.jsp"%>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="dashboard.jsp">Dashboard</a></li>
					<li class="active"><a href="resourceAllocation.jsp">Request Resource</a></li>
					<li><a href="Reports.jsp">Reports</a></li>
					<li><a href="notifications.jsp">Notifications<% if(unnot != 0) {out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");}%></a></li>
				</ul>
				<%@include file="master/navbar_right.jsp"%>
			</div>
		</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<%@include file="master/left_sidebar.jsp"%>
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Welcome, <span id="pm_user_name"><%=name.split("\\(")[0]%></span>
						</h4>
					</div>
					<div class="panel-body">
						<div class="row">		
						<div class="col-md-12">
							<%List<EmployeeData> data = ProjectManagerDao.cloneRequest(pfm_id,q,y);%>
							<br />
							Select a projects resource you want to clone to <b><%=ProjectManagerDao.getProjectName(to_pfm_id)%></b> from the resources of - <b><% out.print(ProjectManagerDao.getProjectName(pfm_id)); %></b> in <%=year%> 
							<br /> <br />
								<form method="post" action="notifyUpdate.jsp">
									<table id="myTable" class="display table"
										style="border-radius: 2px;">
										<thead>
											<tr>
												<th>Employee Name</th>
												<th>Employee CEC ID</th>
												<th>Reporting Manager</th>
												<th>M1</th>
												<th>M2</th>
												<th>M3</th>
												<th>Select</th>
											</tr>
										</thead>
											<%
												for (int i = 0; i < data.size(); i++) {
											%>
											
										<tr>
											<td><a href="employeeDetail.jsp?cec_id=<%=data.get(i).getCec_id()%>"><%out.print(data.get(i).getName().split("\\(")[0]); %></a></td>
											<td><% out.print(data.get(i).getCec_id()); %></td>
											<td><%out.print("<span data-toggle=\"tooltip\" data-placement=\"right\" title=" + "\"CEC ID: " + EmployeeDao.getReportingManagerId(data.get(i).getCec_id()) + "\">" + EmployeeDao.getReportingManager(data.get(i).getCec_id()).split("\\(")[0] + "</span>");%></td>
											<td><input type="text" class="form-control" name="M1[]" id="allocation" value="0"></td>
											<td><input type="text" class="form-control" name="M2[]" id="allocation" value="0"></td>
											<td><input type="text" class="form-control" name="M3[]" id="allocation" value="0"></td>
											<td><input type="checkbox" name="select[]" value="1"></td>
											<input type="hidden" name="id[]" value="<%out.print(data.get(i).getCec_id());%>">
										</tr>
										<%
											}
										%>
									</table>
									<input type="hidden" name="quarter" value="<%=q%>">
									<input type="hidden" name="year" value="<%=y%>">
									<input type="hidden" name="pfm_id" value="<%=pfm_id%>">
									<input type="submit" class="btn btn-primary" value="Submit Request">
								</form>
								<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								    <div class="modal-dialog">
								        <div class="modal-content">
								            <div class="modal-header">
								                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								                 <h4 class="modal-title">Employee Detail</h4>
								            </div>
								            <div class="modal-body"><div class="te"></div></div>
								            <div class="modal-footer">
								                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								                <button type="button" class="btn btn-primary">More Details</button>
								            </div>
								        </div>
								    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<%@include file="master/footer.jsp"%>

	<!-- Tables script -->
	<script type="text/javascript" src="../js/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="../js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="../js/jszip.min.js"></script>
	<script type="text/javascript" src="../js/pdfmake.min.js"></script>
	<script type="text/javascript" src="../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../js/buttons.html5.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/yandex.js"></script>
	<script>
	$(document).ready(function() {
	    $('#myTable').DataTable( {
	        dom: 'Bfrtip',
	        buttons: [
	            {
	                extend:    'copyHtml5',
	                text:      '<i class="fa fa-files-o"></i>',
	                titleAttr: 'Copy'
	            },
	            {
	                extend:    'excelHtml5',
	                text:      '<i class="fa fa-file-excel-o"></i>',
	                titleAttr: 'Excel'
	            },
	            {
	                extend:    'csvHtml5',
	                text:      '<i class="fa fa-file-text-o"></i>',
	                titleAttr: 'CSV'
	            },
	            {
	                extend:    'pdfHtml5',
	                text:      '<i class="fa fa-file-pdf-o"></i>',
	                titleAttr: 'PDF'
	            }
	        ]
	    } );
	} );
	</script>
</body>
</html>
<%
	}
%>