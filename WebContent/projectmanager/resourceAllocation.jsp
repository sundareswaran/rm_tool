<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="org.omg.CORBA.PUBLIC_MEMBER"%>
<%@page import="com.cisco.rmtool.ProjectManagerDao"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@ include file="master/datejava.jsp"%>
<%
	out.print(session.getAttribute("session"));
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		String pfm_id = request.getParameter("pfm_id");
		int unnot = ProjectManagerDao.getNumberofUnreadNotifications(id);
		List<ProjectData> data = ProjectManagerDao.getProjects(id, q, y);
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Request Resource  - <%=name.split("\\(")[0]%>(Project Manager)</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" href="../css/sticky-footer-navbar.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../css/buttons.dataTables.min.css">
<script type="text/javascript">
	function getRadioVal(form, name) {
		var val;
		var radios = form.elements[name];
		for (var i=0, len=radios.length; i<len; i++) {
			if ( radios[i].checked ) { 
				val = radios[i].value;
				break; 
			}
		}
		return val;
	}
	function redirectClone() {
		var val = getRadioVal( document.getElementById('proj'), 'pfm_id' );
		if(val === undefined) {
			document.getElementById("div_alert").innerHTML = '<div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Select a project!</strong> <br/>Please select a project to clone.</div>';
		} else {
			window.location="cloneResource.jsp?pfm_id=" + val + "&year=<%out.print(year);%>"; 
		}
	}
	function redirectSkillSelection() {
		var val = getRadioVal( document.getElementById('proj'), 'pfm_id' );
		if(val === undefined) {
			document.getElementById("div_alert").innerHTML = '<div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Select a project!</strong> <br/>Please select a project to select resources manually.</div>';
		} else {
			window.location="SkillSelection.jsp?pfm_id=" + val + "&year=<%out.print(year);%>"; 
		}
	}
	function redirectTemporary() {
		var val = getRadioVal( document.getElementById('proj'), 'pfm_id' );
		if(val === undefined) {
			document.getElementById("div_alert").innerHTML = '<div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Select a project!</strong> <br/>Please select a project to select temporary resources.</div>';
		} else {
			window.location="selectTemporaryResource.jsp?pfm_id=" + val + "&year=<%out.print(year);%>"; 
		}
	}
	function alerts() {
		var alert = location.search.split('alert=')[1] ? location.search.split('alert=')[1] : 'false';
		if(alert === "resourceRequestSuccess") {
			document.getElementById("div_alert").innerHTML = '<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> <br/>Your request for resource has been submitted!</div>';
		} else if(alert === "resourceRequestFailed") {
			document.getElementById("div_alert").innerHTML = '<div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Failure!</strong> <br/>Your request for resource has not been submitted! Try again!</div>';
		}
	}
</script>
</head>
<body style="min-height: auto;" onload="alerts();">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<%@include file="master/brand.jsp"%>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="dashboard.jsp">Dashboard</a></li>
					<li class="active"><a href="resourceAllocation.jsp">Request Resource</a></li>
					<li><a href="Reports.jsp">Reports</a></li>
					<li><a href="notifications.jsp">Notifications<% if(unnot != 0) {
						out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
					}%></a></li>
				</ul>
				<%@include file="master/navbar_right.jsp"%>
			</div>
		</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">Welcome, <span id="pm_name"><%=name.split("\\(")[0]%></span></h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<form action="" method="get">
									<div id="div_alert"></div>
									<div class="alert alert-info" role="alert"
										style="padding-top: 5px; padding-bottom: 5px;">
										<table
											style="background: transparent; box-shadow: none; border: 0px; color: inherit;">
											<tr>
												<td>
													Quarter - 
												</td>
												<td>
													<%@ include file="master/DateJsp.jsp"%>
												</td>
											</tr>
										</table>
									</div>
									<div class="row">
										<div class="col-md-6">
												<button type="button" class="btn btn-primary" onclick="location.href='addTemporaryResource.jsp'">Add Temporary Resource</button>
										</div>
										<div class="col-md-6" align="right">
												<button type="button" class="btn btn-primary" onclick="redirectSkillSelection()">Select Manually</button>
												<button type="button" class="btn btn-primary" onclick="redirectClone()">Clone</button>
												<button type="button" class="btn btn-primary" onclick="redirectTemporary()">Select Temporary Resource</button>
										</div>
									</div>
								</form>
								<form id="proj">
									<table id="project_selection" class="display table" style="border-radius: 2px;">
										<thead>
											<tr>
												<th>Name</th>
												<th>Total Number of Employees</th>
												<th>Status</th>
												<th>Select</th>
											</tr>
										</thead>
										<%
											for (int i = 0; i < data.size(); i++) {
										%>
										<tr>
											<td>
												<a href="projects.jsp?pfm_id=<%out.print(data.get(i).getId());%>&year=<%=year%>&status=APPROVED">
													<%out.print(data.get(i).getName());%>
												</a>
											</td>
											<td>
													<%out.print(data.get(i).getTotalEmployees());%>
											</td>
											<td>
													<%out.print(data.get(i).getStatus());%>
											</td>
											<td>
													<input type="radio" name="pfm_id" value="<%out.print(data.get(i).getId());%>">
											</td>
											 <%
												}
											 %>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@include file="master/footer.jsp"%>
	<script type="text/javascript" src="../js/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="../js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="../js/jszip.min.js"></script>
	<script type="text/javascript" src="../js/pdfmake.min.js"></script>
	<script type="text/javascript" src="../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/yandex.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
	    $('#project_selection').DataTable( {
	    	aaSorting: [],
	        dom: 'Bfrtip',
	        buttons: [
	            {
	                extend:    'copyHtml5',
	                text:      '<i class="fa fa-files-o"></i>',
	                titleAttr: 'Copy'
	            },
	            {
	                extend:    'excelHtml5',
	                text:      '<i class="fa fa-file-excel-o"></i>',
	                titleAttr: 'Excel'
	            },
	            {
	                extend:    'csvHtml5',
	                text:      '<i class="fa fa-file-text-o"></i>',
	                titleAttr: 'CSV'
	            },
	            {
	                extend:    'pdfHtml5',
	                text:      '<i class="fa fa-file-pdf-o"></i>',
	                titleAttr: 'PDF'
	            }
	        ]
	    } );
	} );
</script>
</body>
</html>
<% } %>