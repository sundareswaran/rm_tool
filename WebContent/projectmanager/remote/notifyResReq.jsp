<%@page import="com.cisco.rmtool.ReportingManager"%>
<%@page import="com.cisco.rmtool.Employee"%>
<%@page import="com.cisco.rmtool.ProjectManager"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@ include file="../master/datejava.jsp"%>
<%@page import="java.util.List"%>
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String groupId = request.getParameter("groupId");
		String rm_cec_id = request.getParameter("reportingManager");
		String seen_status = request.getParameter("seen_status");
		List<EmployeeData> emp = ProjectManager.getNotifyResReq(id, rm_cec_id, groupId, seen_status);
		List<ProjectData> prj = ProjectManager.getNotifyResReqQandY(id, rm_cec_id, groupId, seen_status);
%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<title>Resource Request</title>
</head>
<body>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Requested Resource</h4>
	</div>
	<% Boolean r = ProjectManager.updateNotificationStatus(id,rm_cec_id,groupId); %>
	<div class="modal-body">
		<table>
			<%
				for(int i=0; i<prj.size(); i++) {
			%>
				<tr>
					<td><b>Project Name</b></td>
					<td>&nbsp;&nbsp;&nbsp;<%=prj.get(i).getPrjName()%></td>
				</tr>
				<tr>
					<td><b>Financial Year</b></td>
					<td>&nbsp;&nbsp;&nbsp;<%=prj.get(i).getQFY() %></td>
				</tr>
			<%
				}
			%>
		</table>
		<br/>
		<table id="myTable" class="display table" style="border-radius: 2px;">
			<thead>
				<tr>
					<th>Name</th>
					<th>M1</th>
					<th>M2</th>
					<th>M3</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<%
					for (int i = 0; i < emp.size(); i++) {
				%>
				<tr>
					<td>
						<a href="employeeDetail.jsp?cec_id=<%out.print(emp.get(i).getProject().getId());%>&flag=<%out.print(emp.get(i).getType());%>"><%out.print(emp.get(i).getProject().getPrjName());%></a>
					</td>
					<td>
						<%=emp.get(i).getM1() %>
					</td>
					<td>
						<%=emp.get(i).getM2() %>
					</td>
					<td>
						<%=emp.get(i).getM3() %>
					</td>
					<td>
						<%=emp.get(i).getStatus() %>
					</td>
				</tr>
				<%
					}
				%>
			</tbody>
		</table>
	</div>
	<div class="modal-footer">
	</div>
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
</body>
</html>
<%
	}
%>