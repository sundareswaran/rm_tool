<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="org.omg.CORBA.PUBLIC_MEMBER"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="com.cisco.rmtool.ReportingManagerDao"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@ include file="master/datejava.jsp"%>
<%
	out.print(session.getAttribute("session"));
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		// Here i will use the ProjectManagerProjects class to get the data

		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		int unnot = ReportingManagerDao.getNumberofUnreadNotifications(id);
%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Resource Request</title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.min.css">
<style>
td { 
    vertical-align: middle;
}
</style>
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">

			<!-- Navbar left end, collapsable header Brand info-->
			<%@include file="master/brand.jsp"%>

			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="dashboard.jsp">Dashboard</a></li>
					<li><a href="Reports.jsp">Reports</a></li>
					<li class="active"><a href="resourcerequest.jsp">Resource Request</a></li>
					<li><a href="notifications.jsp">Notifications<% if(unnot != 0) {
						out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
					}%></a></li>
				</ul>

				<!-- Navbar right profile and logout -->
				<%@include file="master/navbar_right.jsp"%>

			</div>
		</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<!--/.sidebar-offcanvas-->
			<!--/row-->
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Welcome, <span id="rm_name"><%=name.split("\\(")[0]%></span>
						</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
							<form action="" method="get">
								<div class="alert alert-info" role="alert"
									style="padding-top: 5px; padding-bottom: 5px;">
									<table
										style="background: transparent; box-shadow: none; border: 0px; color: inherit;">
										<tr>
											<td>Quarter -</td>
												<td><%@ include file="master/DateJsp.jsp"%>
												</td>
												<%
													List<EmployeeData> data = ReportingManagerDao.getResourceRequest(id, q, y);
												%>
										</tr>
									</table>
								</div>
								</form>
								<p>Employees reporting to you,</p>
								<div id="pm_as_pm">
									<form action="updateRequests.jsp">
										<table id="myTable" class="display table"
											style="border-radius: 2px;">
											<thead>
												<tr>
													<th>Name</th>
													<th>CEC_ID</th>
													<th>Project</th>
													<th>M1</th>
													<th>M2</th>
													<th>M3</th>
													<th>Approve</th>
													<th>Decline</th>
													<th>Comment</th>
												</tr>
											</thead>
											<tbody>
												<%
													for (int i = 0; i < data.size(); i++) {
												%>
												<tr>
													<td	valign="middle">&nbsp;<a onclick="getEmployeeData(<%=data.get(i).getCec_id()%>,<%=data.get(i).getName()%>,<%=data.get(i).getReportingManager()%>);"
														href="employeeDetail.jsp?id=<%out.print(data.get(i).getCec_id());%>">
															<%
																out.print(data.get(i).getName().split("\\(")[0]);
															%>
													</a><input type="hidden" name="empid[]"
														value="<%out.print(data.get(i).getCec_id());%>"></td>
														<td><%out.print(data.get(i).getCec_id());%></td>
													<td>&nbsp;<a
														href="projects.jsp?id=<%=data.get(i).getProject().getId()%>"><%=data.get(i).getProject().getName()%></a><input
														name="pfm_id[]" type="hidden"
														value="<%=data.get(i).getProject().getId()%>"></td>
													<td>&nbsp;<input type="hidden" name="M1[]"
														class="form-control" value="<%=data.get(i).getM1()%>"><%=data.get(i).getM1()%></td>
													<td>&nbsp;<input type="hidden" name="M2[]"
														class="form-control" value="<%=data.get(i).getM2()%>"><%=data.get(i).getM2()%></td>
													<td>&nbsp;<input type="hidden" name="M3[]"
														class="form-control" value="<%=data.get(i).getM3()%>"><%=data.get(i).getM3()%></td>
													<td>&nbsp;&nbsp;&nbsp;<input type="radio" name="decision<%=i%>" value="APPROVED"></td>
													<td>&nbsp;&nbsp;&nbsp;<input type="radio" name="decision<%=i%>" value="DECLINED"></td>
													<td><input type="text" name="comment[]" class="form-control" placeholder="Comments">
														<input type="hidden" name="type[]" value="<%=data.get(i).getType()%>">
														<input type="hidden" name="group_id[]" value="<%=data.get(i).getGroupId()%>"></td>
												</tr>
												<%
													}
												%>
											</tbody>
										</table>
										<input type="hidden" name="year" value="<%=y%>"> <input
											type="hidden" name="quarter" value="<%=q%>"> <input
											type="submit" class="btn btn-primary" value="Submit">
									</form>
								</div>
							</div>
							<div class="col-md-8"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<%@include file="master/footer.jsp"%>

	<!-- Table script -->
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/yandex.js"></script>
	<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#myTable').dataTable(
					
					 {
						 aaSorting: [],
						 "bSort" : false
					 }
			);
		});
	</script>

</body>
<%
	}
%>
</html>
