<%@page import="com.cisco.rmtool.ReportingManagerDao"%>
<%@page import="com.cisco.rmtool.ReportingManagerDao"%>
<%@page import="com.cisco.rmtool.EmployeeDao"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@ include file="master/datejava.jsp"%>
<%@page import="java.util.List"%>
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String groupId = request.getParameter("groupId");
		List<EmployeeData> data = ReportingManagerDao.getNotifyResReq(groupId, id);
%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/bootstrap-theme.min.css" rel="stylesheet">
<title>Resource Request</title>
</head>
<body>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-hidden="true">&times;</button>
		<h4 class="modal-title">Resource Request</h4>

	</div>
	<div class="modal-body">
		<form action="updateRequests.jsp">
			<table id="myTable" class="display table" style="border-radius: 2px;">
				<thead>
					<th>Name</th>
					<th>Project</th>
					<th>M1</th>
					<th>M2</th>
					<th>M3</th>
					<th>Approve</th>
					<th>Decline</th>
					<th>Comment</th>
				</thead>
				<tbody>
					<%
						for (int i = 0; i < data.size(); i++) {
								if (data.get(i).getStatus().equalsIgnoreCase(EmployeeData.PENDING)) {
					%>
					<tr>
						<td valign="middle">&nbsp;<a
							onclick="getEmployeeData(<%=data.get(i).getCec_id()%>,<%=data.get(i).getName()%>,<%=data.get(i).getReportingManager()%>);"
							href="employeeDetail.jsp?id=<%out.print(data.get(i).getCec_id());%>">
								<%
									out.print(data.get(i).getName().split("\\(")[0] + "  " + data.get(i).getStatus());
								%>
						</a><input type="hidden" name="empid[]"
							value="<%out.print(data.get(i).getCec_id());%>"></td>
						<td>&nbsp;<a
							href="projects.jsp?id=<%=data.get(i).getProject().getId()%>&name=<%=data.get(i).getProject().getName()%>"><%=data.get(i).getProject().getName()%></a><input
							name="pfm_id[]" type="hidden"
							value="<%=data.get(i).getProject().getId()%>"></td>
						<td>&nbsp;<input type="hidden" name="M1[]"
							class="form-control" value="<%=data.get(i).getM1()%>"><%=data.get(i).getM1()%></td>
						<td>&nbsp;<input type="hidden" name="M2[]"
							class="form-control" value="<%=data.get(i).getM2()%>"><%=data.get(i).getM2()%></td>
						<td>&nbsp;<input type="hidden" name="M3[]"
							class="form-control" value="<%=data.get(i).getM3()%>"><%=data.get(i).getM3()%></td>
						<td><input type="radio" name="decision<%=i%>"
							value="APPROVED"></td>
						<td><input type="radio" name="decision<%=i%>"
							value="DECLINED"></td>
						<td><input type="text" name="comment[]" class="form-control"
							placeholder="Comments"> <input type="hidden"
							name="type[]" value="<%=data.get(i).getType()%>"> <input
							type="hidden" name="group_id[]" value="<%=groupId%>"></td>
							
					</tr>
					<%
						}
							}
					%>
					<%
						for (int i = 0; i < data.size(); i++) {
								if (data.get(i).getStatus().equalsIgnoreCase(EmployeeData.APPROVED)
										|| data.get(i).getStatus().equalsIgnoreCase(EmployeeData.DECLINED)) {
					%>
					<tr>
						<td valign="middle">&nbsp;<%
							out.print(data.get(i).getName().split("\\(")[0]);
						%></td>
						<td>&nbsp;<%=data.get(i).getProject().getName()%></td>
						<td>&nbsp;<%=data.get(i).getM1()%></td>
						<td>&nbsp;<%=data.get(i).getM2()%></td>
						<td>&nbsp;<%=data.get(i).getM3()%></td>
						<td colspan="2"><%=data.get(i).getStatus()%></td>
						<td>Nil</td>
					</tr>
					<%
						}
							}
					%>
				</tbody>
			</table>
			<input type="hidden" name="year" value="<%=y%>"> <input
				type="hidden" name="quarter" value="<%=q%>"><input
				type="submit" class="btn btn-primary" value="Submit">
		</form>
	</div>
	<div class="modal-footer">
		<script type="text/javascript" src="../js/jquery.js"></script>
		<script src="../js/bootstrap.min.js"></script>
</body>
</html>
<%
	}
%>