<%@page import="com.cisco.rmtool.JsonProvider"%>
<%@page import="java.time.Year"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.List"%>
<%@page import="com.cisco.rmtool.ReportingManagerDao"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String[] m1 = request.getParameterValues("M1[]");
	String[] m2 = request.getParameterValues("M2[]");
	String[] m3 = request.getParameterValues("M3[]");
	String[] empid = request.getParameterValues("empid[]");
	String[] pfm_id = request.getParameterValues("pfm_id[]");
	String[] comment = request.getParameterValues("comment[]");
	String[] type = request.getParameterValues("type[]");
	String[] group_id = request.getParameterValues("group_id[]");
	String decision;
	List<EmployeeData> data = Collections.emptyList();
	if (empid.length > 0)
		data = new ArrayList();
	for (int i = 0; i < m1.length; i++) {
		decision = request.getParameter("decision" + i);
		if (decision != null && decision.equalsIgnoreCase("APPROVED")) {
			EmployeeData employeeData = new EmployeeData();
			employeeData.setCec_id(empid[i]);
			employeeData.setM1(m1[i]);
			employeeData.setM2(m2[i]);
			employeeData.setM3(m3[i]);
			employeeData.getProject().setId(pfm_id[i]);
			employeeData.setQuarter(request.getParameter("quarter"));
			employeeData.setYear(request.getParameter("year"));
			employeeData.setStatus("APPROVED");
			employeeData.setComment(comment[i]);
			employeeData.setType(type[i]);
			employeeData.setGroupId(group_id[i]);
			data.add(employeeData);
		} else if (decision != null && decision.equalsIgnoreCase("DECLINED")) {
			EmployeeData employeeData = new EmployeeData();
			employeeData.setCec_id(empid[i]);
			employeeData.setM1(m1[i]);
			employeeData.setM2(m2[i]);
			employeeData.setM3(m3[i]);
			employeeData.getProject().setId(pfm_id[i]);
			employeeData.setQuarter(request.getParameter("quarter"));
			employeeData.setYear(request.getParameter("year"));
			employeeData.setStatus("DECLINED");
			employeeData.setComment(comment[i]);
			employeeData.setType(type[i]);
			employeeData.setGroupId(group_id[i]);
			data.add(employeeData);
		}
		//	out.println("<p>" + empid[i] + " " + pfm_id[i] + " " + m1[i] + " " + m2[i] + " " + m3[i] + " " + " "
		//			+ comment[i] + " " + request.getParameter("choice" + i) + "</p>");
	}
	//out.println("<p> " + request.getParameter("year") + " " + request.getParameter("quarter") + "</p>");
	boolean r = ReportingManagerDao.updateRequest(data);
	if (r)
		response.sendRedirect("dashboard.jsp?alert=success");
	else
		response.sendRedirect("dashboard.jsp?alert=failed");
	//out.print(JsonProvider.getGson().toJson(data));
%>