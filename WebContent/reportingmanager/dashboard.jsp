<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="org.omg.CORBA.PUBLIC_MEMBER"%>
<%@page import="com.cisco.rmtool.ReportingManagerDao"%>
<%@page import="com.cisco.rmtool.EmployeeDao"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@ include file="master/datejava.jsp"%>
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		// Here i will use the ProjectManagerProjects class to get the data
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		int m1, m2, m3;
		int unnot = ReportingManagerDao.getNumberofUnreadNotifications(id);
		List<EmployeeData> data = ReportingManagerDao.getReportingEmployeeDetails(id, q, y);
%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Reporting Manager</title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../css/buttons.dataTables.min.css">
<style>
.badge-notify{
   background:red;
   position:relative;
   top: -20px;
   left: -35px;
}
#dt_align {
	vertical-align: middle;
}
</style>
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">

			<!-- Navbar left end, collapsable header Brand info-->
			<%@include file="master/brand.jsp"%>

			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="dashboard.jsp">Dashboard</a></li>
					<li><a href="Reports.jsp">Reports</a></li>
					<li><a href="resourcerequest.jsp">Resources Request</a></li>
					<li><a href="notifications.jsp">Notifications<% if(unnot != 0) {
						out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
					}%></a></li>
				</ul>
				<!-- Navbar right profile and logout -->
				<%@include file="master/navbar_right.jsp"%>
			</div>
		</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<!--/.sidebar-offcanvas-->
			<!--/row-->
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Welcome, <span id="rm_name"><%=name.split("\\(")[0]%></span>
						</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<form action="" method="get">
								<div class="alert alert-info" role="alert"
									style="padding-top: 5px; padding-bottom: 5px;">
									<table
										style="background: transparent; box-shadow: none; border: 0px; color: inherit;">
										<tr>
											<td>Quarter - </td>
											<td><%@ include file="master/DateJsp.jsp"%></td>
										</tr>
									</table>
								</div>
								</form>
								<p>Employees reporting to you,</p>
								<div id="pm_as_pm">
									<table id="myTable" class="display table"
										style="border-radius: 2px;">
										<thead>
											<tr>											
												<th class="table-sort">Employee Name</th>
												<th class="table-sort">Employee ID</th>
												<th class="table-sort">CEC ID</th>
												<th class="table-sort">Total Number of Projects</th>
												<th class="table-sort">M1</th>
												<th class="table-sort">M2</th>
												<th class="table-sort">M3</th>
											</tr>
										</thead>
										<tbody>
											<%
												for (int i = 0; i < data.size(); i++) {
													m1 = Integer.parseInt(data.get(i).getM1());
													m2 = Integer.parseInt(data.get(i).getM2());
													m3 = Integer.parseInt(data.get(i).getM3());
											%>
											<tr>
												<td id="dt_align">&nbsp;&nbsp;<a data-toggle="tooltip" data-placement="right"
													href="employeeDetail.jsp?cec_id=<%=data.get(i).getCec_id()%>&flag=<%out.print(data.get(i).getType());%>"><%=data.get(i).getName().split("\\(")[0]%></a></td>
												<td><%=data.get(i).getEmp_id()%></td>
												<td><%=data.get(i).getCec_id()%></td>
												<td id="dt_align"><a data-toggle="modal" href="employee.jsp?cec_id=<%out.print(data.get(i).getCec_id());%>&flag=<%out.print(data.get(i).getType()); %>" data-target="#myModal">
												<%=data.get(i).getTotalProjects()%></a>
												</td>
												<%
													if(m1 > 100)
													{
														out.print("<td id=\"dt_align\" style=\" color:red\">&nbsp;&nbsp;<u><b>" + data.get(i).getM1()+ "</b></u></td>" );
													} else if(m1 < 50)
													{
														out.print("<td id=\"dt_align\" style=\" color:2a6496\">&nbsp;&nbsp;<u><b>" + data.get(i).getM1()+ "</b></u></td>" );
													} else {
														out.print("<td id=\"dt_align\">&nbsp;&nbsp;<b>" + data.get(i).getM1()+ "</b></td>" );
													}
												%>
												<%
													if(m2 > 100)
													{
														out.print("<td id=\"dt_align\" style=\" color:red\">&nbsp;&nbsp;<u><b>" + data.get(i).getM2()+ "</b></u></td>" );
													} else if(m2 < 50)
													{
														out.print("<td id=\"dt_align\" style=\" color:2a6496\">&nbsp;&nbsp;<u><b>" + data.get(i).getM2()+ "</b></u></td>" );
													} else {
														out.print("<td id=\"dt_align\">&nbsp;&nbsp;<b>" + data.get(i).getM2()+ "</b></td>" );
													}
												%>
												<%
													if(m3 > 100)
													{
														out.print("<td id=\"dt_align\" style=\" color:red\">&nbsp;&nbsp;<u><b>" + data.get(i).getM3()+ "</b></u></td>" );
													} else if(m2 < 50)
													{
														out.print("<td id=\"dt_align\" style=\" color:2a6496\">&nbsp;&nbsp;<u><b>" + data.get(i).getM3()+ "</b></u></td>" );
													} else {
														out.print("<td id=\"dt_align\">&nbsp;&nbsp;<b>" + data.get(i).getM3()+ "</b></td>" );
													}
												%>
											</tr>
											<%
												}
											%>
										</tbody>
									</table>
								</div>
								<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										 <h4 class="modal-title">Employee Projects</h4>
									</div>
									<div class="modal-body"><div class="te"></div></div>
									<div class="modal-footer">
									</div>
								</div>
							</div>
						</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<%@include file="master/footer.jsp"%>
	<script type="text/javascript" src="../js/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="../js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="../js/jszip.min.js"></script>
	<script type="text/javascript" src="../js/pdfmake.min.js"></script>
	<script type="text/javascript" src="../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../js/buttons.html5.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/yandex.js"></script>
	<script>
		$(document).ready(function() {
			$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
<script>
	$(document).ready(function() {
	    $('#myTable').DataTable( {
	    	aaSorting: [],
	        dom: 'Bfrtip',
	        buttons: [
	            {
	                extend:    'copyHtml5',
	                text:      '<i class="fa fa-files-o"></i>',
	                titleAttr: 'Copy'
	            },
	            {
	                extend:    'excelHtml5',
	                text:      '<i class="fa fa-file-excel-o"></i>',
	                titleAttr: 'Excel'
	            },
	            {
	                extend:    'csvHtml5',
	                text:      '<i class="fa fa-file-text-o"></i>',
	                titleAttr: 'CSV'
	            },
	            {
	                extend:    'pdfHtml5',
	                text:      '<i class="fa fa-file-pdf-o"></i>',
	                titleAttr: 'PDF'
	            }
	        ]
	    } );
	} );
</script>
	<!-- Table script -->
</body>
<%
	}
%>
</html>
