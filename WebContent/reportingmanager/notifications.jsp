<%@page import="com.cisco.rmtool.ReportingManagerDao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.cisco.rmtool.ReportingManagerDao"%>
<%@page import="com.cisco.rmtool.Notification"%>
<%@page import="com.cisco.rmtool.NotificationData"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	out.print(session.getAttribute("session"));
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		int unnot = ReportingManagerDao.getNumberofUnreadNotifications(id);
		List<NotificationData> unread = Notification.getUnreadNotifications(id);
		List<NotificationData> read = Notification.getReadNotifications(id);
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Notifications - Reporting Manager</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" href="../css/sticky-footer-navbar.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../css/buttons.dataTables.min.css">
<style type="text/css">
#myModal .modal-dialog
{
  width: 70%;
}
</style>
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<%@include file="master/brand.jsp"%>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="dashboard.jsp">Dashboard</a></li>
					<li><a href="Reports.jsp">Reports</a></li>
					<li><a href="resourcerequest.jsp">Resources Request</a></li>
					<li class="active"><a href="notifications.jsp">Notifications <% if(unnot != 0) {
							out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
						}%></a></li>
				</ul>
				<%@include file="master/navbar_right.jsp"%>
			</div>
		</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Welcome, <span id="rm_name"><%=name.split("\\(")[0]%></span>
						</h4>
					</div>
					<div class="panel-body">
					<div class="row">
				<div class="col-md-12">
				<h4>Notifications</h4>
				  <ul class="nav nav-tabs">
				    <li class="active"><a data-toggle="tab" href="#notiunread">Unread<% if(unnot != 0) {
						out.print("(" + unnot + ")");
					}%></a></li>
				    <li><a data-toggle="tab" href="#notiread">Read</a></li>
				  </ul>
				  
				  <div class="tab-content">
			    <div id="notiunread" class="tab-pane fade in active">
			    	<br />
			    	<table id="Unread" class="display table"
									style="border-radius: 2px;">
									<thead>
										<tr>
											<th>Project Name</th>
											<th>Project Manager</th>
											<th>Total Resource Requested</th>
											<th>Date</th>
											<th>Time</th>
										</tr>
									</thead>
									<%
										for (int i = 0; i < unread.size(); i++) {
									%>
									<tr data-toggle="modal" href="remote/notifyResReq.jsp?groupId=<%out.print(unread.get(i).getGroupId());%>" data-target="#myModal">
										<td><a href="projects.jsp?pfm_id=<%out.println(unread.get(i).getProject().getId());%>">
											<%
												out.println(unread.get(i).getProject().getName());
											%>
											</a>
										</td>
										<td>
											<%
												out.println(unread.get(i).getProject().getProjectManager().split("\\(")[0]);
											%>
										</td>
										<td>
											<%
												out.println(unread.get(i).getTotResReq());
											%>
										</td>
										<td>
											<%
												out.println(unread.get(i).getDate().split(" ")[0]);
											%>
										</td>
										<td>
											<%
												out.println(unread.get(i).getTime().split("\\.")[0]);
											%>
										</td>
									</tr>
									<%
										}
									%>
								</table>
			    </div>
			    <div id="notiread" class="tab-pane fade">
			    <br />
			      <table id="Read" class="display table"
									style="border-radius: 2px;">
									<thead>
										<tr>
											<th>Project Name</th>
											<th>Project Manager</th>
											<th>Total Resource Requested</th>
											<th>Date</th>
											<th>Time</th>
										</tr>
									</thead>
									<%
										for (int i = 0; i < read.size(); i++) {
									%>
									<tr data-toggle="modal" href="remote/notifyResReq.jsp?groupId=<%out.print(read.get(i).getGroupId());%>" data-target="#myModal">
										<td><a href="projects.jsp?pfm_id=<%out.println(read.get(i).getProject().getId());%>">
											<%
												out.println(read.get(i).getProject().getName());
											%>
											</a>
										</td>
										<td>
											<%
												out.println(read.get(i).getProject().getProjectManager().split("\\(")[0]);
											%>
										</td>
										<td>
											<%
												out.println(read.get(i).getTotResReq());
											%>
										</td>
										<td>
											<%
												out.println(read.get(i).getDate().split(" ")[0]);
											%>
										</td>
										<td>
											<%
												out.println(read.get(i).getTime().split("\\.")[0]);
											%>
										</td>
									</tr>
									<%
										}
									%>
								</table>
				</div>
			    </div>
			    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										 <h4 class="modal-title">Employee Detail</h4>
									</div>
									<div class="modal-body"><div class="te"></div></div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
			  </div>
			</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Footer -->
	<%@include file="master/footer.jsp"%>
	<script type="text/javascript" src="../js/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="../js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="../js/jszip.min.js"></script>
	<script type="text/javascript" src="../js/pdfmake.min.js"></script>
	<script type="text/javascript" src="../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/yandex.js"></script>
	<script type="text/javascript" src="../js/highcharts.js"></script>
	<script>
		$(document).ready(function() {
			$('#Unread').dataTable( {aaSorting: []});
		});
	</script>
	<script>
		$(document).ready(function() {
			$('#Read').dataTable( {aaSorting: []});
		});
	</script>
</body>
</html>
<%
	}
%>