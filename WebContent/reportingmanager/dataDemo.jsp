<%@page import="com.cisco.rmtool.ProjectData"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@page import="java.util.List"%>
<%@page import="com.cisco.rmtool.ReportingManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<p>
		Number of Employees =
		<%=ReportingManager.getNumberOfEmployees("16", "Q4")%></p>

	<p>
		Number of all Employees =
		<%=ReportingManager.getCountOfAllEmployees("16")%></p>
	<p>
		List of all employees =
		<%=ReportingManager.JSgetAllEmployeesUnderRM("16")%></p>
	<p>
		List of all regions =
		<%=ReportingManager.JSgetRegion("16")%></p>
	<p>
		List of employees reporting -
		<%
		out.print(ReportingManager.JSgetReportingEmployee("16", "Q4"));
	%>
	</p>
	<p>
		List of Projects in which RM's employees work in -
		<%
		out.print(ReportingManager.JSgetProjectsOfAllEmployees("16", "Q4"));
	%>
	</p>
	<p>
		List of Projects in which RM's employees work in (IDs)-
		<%
		out.print(ReportingManager.getAllProjIDsofRMEmp("16", "Q4"));
	%>
	</p>
	<p>
	Total number of projects in which RM employees work in - 
	<%=ReportingManager.getTotalNoOfProjects("16", "Q4") %>
	</p>
	<p>
		Total no of employees in each project under RM-
		<%
		out.print(ReportingManager.JSgetTotalEmployeesInEachProjectUnderRM("16", "Q4"));
	%>
	</p>
	<p>
		Total no of employees in each project (irrespective of the RM) -
		<%
		out.print(ReportingManager.JSgetTotalEmployeesInEachProjectIrrespectiveOfRM("16", "Q4"));
	%>
	</p>
	<p>
		List of Regions that a RM has employees in -
		<%
		out.print(ReportingManager.JSgetAllRegionsOfEmployees("16", "Q4"));
	%>
	</p>
	<p>
		Number of Regions quarter =
		<%=ReportingManager.getNoOfRegionsOfAllEmployees("16", "Q4")%></p>
	<p>
		Number of Regions all =
		<%=ReportingManager.getNoOfRegionsOfAllEmployees("16")%></p>
	<p>
	<p>
		Total number of Employees under the RM who are from a particular
		region for quarter -
		<%
		out.print(ReportingManager.JSgetRegionAndEmployees("16", "Q4"));
	%>
	</p>
	<p>
		Total number of Employees under the RM who are from a particular
		region for for all time-
		<%
		out.print(ReportingManager.JSgetRegionAndEmployees("16"));
	%>
	</p>
	<p>
		List of Employees under the RM who are under allocated -
		<%
		out.print(ReportingManager.JSgetUnderAllocated("16", "Q4"));
	%>
	</p>
	<p>
		Number of Employees under the RM who are under allocated =
		<%=ReportingManager.getNoOfUnderAllocatedEmployees("16", "Q4")%></p>
	<p>
	<p>
		List of Employees under the RM who are over allocated -
		<%
		out.print(ReportingManager.JSgetOverAllocatedEmployees("16", "Q4"));
	%>
	</p>
	<p>
		Number of Employees under the RM who are Over allocated =
		<%=ReportingManager.getNoOfOverAllocatedEmployees("16", "Q4")%></p>
	<p>
	<p>
		Monthly allocation graph M1=
		<%=ReportingManager.JS_MA_getAllEmployeeNames("16", "Q4") %></p>
	<p>
		Monthly allocation graph M1=
		<%=ReportingManager.JS_MA_getAllEmployeeM1("16", "Q4")%></p>
	<p>
		Monthly allocation graph M2=
		<%=ReportingManager.JS_MA_getAllEmployeeM2("16", "Q4")%></p>
	<p>
		Monthly allocation graph M3=
		<%=ReportingManager.JS_MA_getAllEmployeeM3("16", "Q4")%></p>
	
</body>
</html>