<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="org.omg.CORBA.PUBLIC_MEMBER"%>
<%@page import="com.cisco.rmtool.ReportingManagerDao"%>
<%@page import="com.cisco.rmtool.EmployeeDao"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@ include file="master/datejava.jsp"%>
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		String cec_id = request.getParameter("cec_id").toString();
		String flag = request.getParameter("flag").toString();
		int unnot = ReportingManagerDao.getNumberofUnreadNotifications(id);
%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" href="../css/sticky-footer-navbar.css">
<title>Employee Project Details</title>
</head>
<body style="min-height: auto;">
	 <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
     <h4 class="modal-title">Employee Detail</h4>

</div>
<div class="modal-body">
	<div class="table-responsive">
        <table id="myTable" class="table table-bordered">
										<thead>
											<tr>
												<th>Project Name</th>
												<th>Project Manager</th>
												<th>M1</th>
												<th>M2</th>
												<th>M3</th>
											</tr>
										</thead>
										<tbody>
											<%List<ProjectData> emp_proj = EmployeeDao.getProjectsAlloc(cec_id, q, y, flag);
											for(int i = 0; i <emp_proj.size(); i++) { %>
												<tr>
													<td><a href="projects.jsp?pfm_id=<%=emp_proj.get(i).getId()%>"><%= emp_proj.get(i).getName()%></a></td>
													<td><%= emp_proj.get(i).getProjectManager().split("\\(")[0] %></td>
													<td><%= emp_proj.get(i).getM1() %></td>
													<td><%= emp_proj.get(i).getM2() %></td>
													<td><%= emp_proj.get(i).getM3() %></td>
												</tr>
											<% } %>
										</tbody>
									</table>
									</div>
</div>
<div class="modal-footer"></div>
 <script type="text/javascript" src="../js/jquery.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/yandex.js"></script>
</body>
</html>
<%
	}
%>