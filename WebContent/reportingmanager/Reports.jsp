<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="org.omg.CORBA.PUBLIC_MEMBER"%>
<%@page import="com.cisco.rmtool.ReportingManagerDao"%>
<%@page import="com.cisco.rmtool.EmployeeDao"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@ include file="master/datejava.jsp"%>
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		// Here i will use the ProjectManagerProjects class to get the data
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		String pfm_id = request.getParameter("pfm_id");
		if (pfm_id == null) {
			pfm_id = "All";
		}
		int m1, m2, m3;
		int unnot = ReportingManagerDao.getNumberofUnreadNotifications(id);
		List<EmployeeData> data = ReportingManagerDao.getAllEmployeeAllocation(id, q, y);
		List<EmployeeData> dataProject = ReportingManagerDao.getAllEmployeeAllocationInProject(id, pfm_id, q, y);
		List<ProjectData> project = ReportingManagerDao.getProjectsOfAllEmployees(id, q, y);
		List<ProjectData> redata = ReportingManagerDao.getRegionAndEmployeesInProject(id, pfm_id, q, y);
%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Reports - Reporting Manager</title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../css/buttons.dataTables.min.css">
<style>
.badge-notify{
   background:red;
   position:relative;
   top: -20px;
   left: -35px;
}
#dt_align {
	vertical-align: middle;
}
</style>
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<%@include file="master/brand.jsp"%>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="dashboard.jsp">Dashboard</a></li>
					<li class="active"><a href="Reports.jsp">Reports</a></li>
					<li><a href="resourcerequest.jsp">Resources Request</a></li>
					<li><a href="notifications.jsp">Notifications<% if(unnot != 0) {
						out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
					}%></a></li>
				</ul>
				<%@include file="master/navbar_right.jsp"%>
			</div>
		</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Welcome, <span id="rm_name"><%=name.split("\\(")[0]%></span>
						</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<form action="" method="get">
								<div class="alert alert-info" role="alert"
									style="padding-top: 5px; padding-bottom: 5px;">
									<table
										style="background: transparent; box-shadow: none; border: 0px; color: inherit;">
										<tr>
											<td>Quarter - </td>
											<td><%@ include file="master/DateJsp.jsp"%></td>
											<td>&nbsp;Project - </td>
													<td><select class="form-control selectpicker"
														data-live-search="true" name="pfm_id"
														style="min-width: 150px;'" onchange="this.form.submit()">
															<option value="All">All</option>
															<%
																for (int i = 0; i < project.size(); i++) {
																		if (pfm_id.equalsIgnoreCase(project.get(i).getId())) {
																			out.println("<option value=\"" + project.get(i).getId() + "\" selected>"
																					+ project.get(i).getName() + "</option>");
																		} else {
																			out.println("<option value=\"" + project.get(i).getId() + "\">" + project.get(i).getName()
																					+ "</option>");
																		}
																	}
															%>
													</select></td>
										</tr>
									</table>
								</div>
								</form>
								<div id="myCarousel" class="carousel slide" data-ride="carousel">
							  <!-- Indicators -->
							  <ol class="carousel-indicators">
							    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							    <li data-target="#myCarousel" data-slide-to="1"></li>
							    <li data-target="#myCarousel" data-slide-to="2"></li>
							    <li data-target="#myCarousel" data-slide-to="3"></li>
							  </ol>
							   <div class="carousel-inner" role="listbox">
							    <div class="item active"><div class="col-md-2"></div>
							    <div class="col-md-8">
									<div id="<%if(pfm_id.equalsIgnoreCase("All"))
							    	 {
							    	 	out.print("MonthlyAllocationofReportingEmployees");
							    	 }
							    	 else
							    	 {
							    		 out.print("MonthlyAllocationofReportingEmployeesInProject");
							    	 }%>" align="center" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
							    </div><div class="col-md-2"></div>
							    </div>
							    <div class="item">
							     <div id="<%if(pfm_id.equalsIgnoreCase("All"))
							    	 {
							    	 	out.print("NumberofEmployeesReportingtoRMvsTotalEmployees");
							    	 }
							    	 else
							    	 {
							    		 out.print("NumberofEmployeesReportingtoRMvsTotalEmployeesProj");
							    	 }%>" align="center" style="min-width: 400px; height: 400px; margin: 0 auto">
							    </div>
							    </div>
							    <div class="item">
							      <div id="<%if(pfm_id.equalsIgnoreCase("All"))
							    	 {
							    	 	out.print("RegionWiseEmployeesReportingtoRM");
							    	 }
							    	 else
							    	 {
							    		 out.print("RegionWiseEmployeesReportingtoRMInProject");
							    	 }%>" align="center" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
							    </div>
							  </div>
							  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
							    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							    <span class="sr-only">Previous</span>
							  </a>
							  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
							    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							    <span class="sr-only">Next</span>
							  </a>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<%@include file="master/footer.jsp"%>
	<script type="text/javascript" src="../js/jquery.js"></script>
		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/yandex.js"></script>
		<script src="../js/highcharts.js"></script>
		<script src="https://code.highcharts.com/modules/exporting.js"></script>
		<script src="https://code.highcharts.com/modules/offline-exporting.js"></script>
		<script>
			$(function() {
		        $('#MonthlyAllocationofReportingEmployees').highcharts({
		            chart: {
		        type: 'column'
		    },
		    title: {
		        text: 'Monthly Employee Allocation'
		    },
		    xAxis: {
		        categories: [<% for(int i = 0; i < data.size(); i++) {
		        				out.print("'" + data.get(i).getName().split("\\(")[0] + "'");
		        				if (i < data.size() - 1){
		        					out.print(",");
		        				}
		       				}%>]
		    },
		    credits: {
		        enabled: false
		    },
		    series: [{
		        name: 'Month 1',
		        data: [<% for(int i = 0; i < data.size(); i++) {
		        				out.print(Integer.parseInt(data.get(i).getM1()) - 100);
		        				if (i < data.size() - 1){
		        					out.print(",");
		        				}
		       				}%>]
		    }, {
		        name: 'Month 2',
		        data: [<% for(int i = 0; i < data.size(); i++) {
		        				out.print(Integer.parseInt(data.get(i).getM2()) - 100);
		        				if (i < data.size() - 1){
		        					out.print(",");
		        				}
		       				}%>]
		    }, {
		        name: 'Month 3',
		        data: [<% for(int i = 0; i < data.size(); i++) {
		        				out.print(Integer.parseInt(data.get(i).getM3()) - 100);
		        				if (i < data.size() - 1){
		        					out.print(",");
		        				}
		       				}%>]
		    }]
		        });
		    });
		</script>
		<script>
			$(function() {
		        $('#MonthlyAllocationofReportingEmployeesInProject').highcharts({
		            chart: {
		        type: 'column'
		    },
		    title: {
		        text: 'Monthly Employee Allocation'
		    },
		    xAxis: {
		        categories: [<% for(int i = 0; i < dataProject.size(); i++) {
		        				out.print("'" + dataProject.get(i).getName().split("\\(")[0] + "'");
		        				if (i < dataProject.size() - 1){
		        					out.print(",");
		        				}
		       				}%>]
		    },
		    credits: {
		        enabled: false
		    },
		    series: [{
		        name: 'Month 1',
		        data: [<% for(int i = 0; i < dataProject.size(); i++) {
		        				out.print(dataProject.get(i).getM1());
		        				if (i < dataProject.size() - 1){
		        					out.print(",");
		        				}
		       				}%>]
		    }, {
		        name: 'Month 2',
		        data: [<% for(int i = 0; i < dataProject.size(); i++) {
		        				out.print(dataProject.get(i).getM2());
		        				if (i < dataProject.size() - 1){
		        					out.print(",");
		        				}
		       				}%>]
		    }, {
		        name: 'Month 3',
		        data: [<% for(int i = 0; i < dataProject.size(); i++) {
		        				out.print(dataProject.get(i).getM3());
		        				if (i < dataProject.size() - 1){
		        					out.print(",");
		        				}
		       				}%>]
		    }]
		        });
		    });
		</script>
		<script>
			$(function () {
			    $('#NumberofEmployeesReportingtoRMvsTotalEmployees').highcharts({
			        chart: {
			            type: 'column'
			        },
			        title: {
			            text: 'Project wise Employee Contribution Ratio'
			        },
			        xAxis: {
			            categories: [<%for (int i = 0; i < project.size(); i++) {
														out.print("'" + project.get(i).getName() + "'");
														if (i < project.size() - 1) {
															out.print(",");
														}
													}%>
												]
			        },
			        yAxis: [{
			            min: 0,
			            title: {
			                text: 'Number of Employees'
			            }
			        }, {
			            title: {
			                text: 'Number of Employees'
			            },
			            opposite: true
			        }],
			        legend: {
			            shadow: false
			        },
			        tooltip: {
			            shared: true
			        },
			        plotOptions: {
			            column: {
			                grouping: false,
			                shadow: false,
			                borderWidth: 0
			            }
			        },
			        series: [{
			            name: 'Total Employees',
			            color: 'rgba(165,170,217,1)',
			            data: [<%for (int i = 0; i < project.size(); i++) {
												out.print(project.get(i).getTotalEmployees());
												if (i < project.size() - 1) {
													out.print(",");
												}
											}%>
										],
			           // pointPadding: 0.3,
			            //pointPlacement: -0.2
			        }, {
			            name: 'Employees Reporting to you',
			            color: 'rgba(126,86,134,.9)',
			            data: [<%for (int i = 0; i < project.size(); i++) {
												out.print(project.get(i).getNoOfEmployees());
												if (i < project.size() - 1) {
													out.print(",");
												}
											}%>
										],
			            pointPadding: 0.2,
			            //pointPlacement: -0.2
			        }]
			    });
			});
		</script>
		<script>
				$('#RegionWiseEmployeesReportingtoRM').highcharts({
			        chart: {
			            plotBackgroundColor: null,
			            plotBorderWidth: null,
			            plotShadow: false,
			            type: 'pie'
			        },
			        title: {
			            text: 'Regional Employee Distribution'
			        },
			        tooltip: {
			            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			        },
			        credits: {
			            enabled: false
			        },
			        plotOptions: {
			            pie: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                dataLabels: {
			                    enabled: true,
			                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
			                    style: {
			                        color: (Highcharts.theme && 
			
			Highcharts.theme.contrastTextColor) || 'black'
			                    }
			                }
			            }
			        }
			        <%=ReportingManagerDao.RegionsChart(id)%>
			    });
			
				</script>
				<script>
				$(function () {
				    $('#NumberofEmployeesReportingtoRMvsTotalEmployeesProj').highcharts({
				        chart: {
				            type: 'column'
				        },
				        title: {
				            text: 'Employee Contribution Ratio'
				        },
				        xAxis: {
				            categories: [<%for (int i = 0; i < project.size(); i++) {
															if (pfm_id.equalsIgnoreCase(project.get(i).getId())) {
																out.print("'" + project.get(i).getName() + "'");
															}
														}%>
													]
				        },
				        yAxis: [{
				            min: 0,
				            title: {
				                text: 'Number of Employees'
				            }
				        }, {
				            title: {
				                text: 'Number of Employees'
				            },
				            opposite: true
				        }],
				        legend: {
				            shadow: false
				        },
				        tooltip: {
				            shared: true
				        },
				        plotOptions: {
				            column: {
				                grouping: false,
				                shadow: false,
				                borderWidth: 0
				            }
				        },
				        series: [{
				            name: 'Total Employees',
				            color: 'rgba(165,170,217,1)',
				            data: [<%for (int i = 0; i < project.size(); i++) {
						            	if (pfm_id.equalsIgnoreCase(project.get(i).getId())) {
											out.print(project.get(i).getTotalEmployees());
										}
												}%>
											],
				           // pointPadding: 0.3,
				            //pointPlacement: -0.2
				        }, {
				            name: 'Employees Reporting to you',
				            color: 'rgba(126,86,134,.9)',
				            data: [<%for (int i = 0; i < project.size(); i++) {
													if (pfm_id.equalsIgnoreCase(project.get(i).getId())) {
														out.print(project.get(i).getNoOfEmployees());
													}
												}%>
											],
				            pointPadding: 0.2,
				            //pointPlacement: -0.2
				        }]
				    });
				});
				</script>
				<script>
				$('#RegionWiseEmployeesReportingtoRMInProject').highcharts({
			        chart: {
			            plotBackgroundColor: null,
			            plotBorderWidth: null,
			            plotShadow: false,
			            type: 'pie'
			        },
			        title: {
			            text: 'Regional Employee Distribution'
			        },
			        tooltip: {
			            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			        },
			        credits: {
			            enabled: false
			        },
			        plotOptions: {
			            pie: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                dataLabels: {
			                    enabled: true,
			                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
			                    style: {
			                        color: (Highcharts.theme && 
			
			Highcharts.theme.contrastTextColor) || 'black'
			                    }
			                }
			            }
			        },
			        series: [{ name: 'Percent of Employees', colorByPoint: true, data: [<%for(int i = 0; i <redata.size(); i++) {
			        	out.print("{name:'" + redata.get(i).getRegion() + "'," + "y:" + redata.get(i).getNoOfEmployees() + "}");
			        	if(i < redata.size() - 1){
			        		out.print(",");
			        	}
			        } %>]}]
			    });
			
				</script>
		<%
			}
		%>
</html>
