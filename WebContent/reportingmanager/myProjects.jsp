<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="org.omg.CORBA.PUBLIC_MEMBER"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="com.cisco.rmtool.ReportingManagerDao"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@ include file="master/datejava.jsp"%>
<%
	out.print(session.getAttribute("session"));
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		int unnot = ReportingManagerDao.getNumberofUnreadNotifications(id);
%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Projects</title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="../css/jquery.dataTables.min.css">
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">

			<!-- Navbar left end, collapsable header Brand info-->
			<%@include file="master/brand.jsp"%>

			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="dashboard.jsp">Dashboard</a></li>
					<li><a href="Reports.jsp">Reports</a></li>
					<li><a href="resourcerequest.jsp">Resource Request</a></li>
					<li><a href="notifications.jsp">Notifications<% if(unnot != 0) {
						out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
					}%></a></li>
				</ul>

				<!-- Navbar right profile and logout -->
				<%@include file="master/navbar_right.jsp"%>

			</div>
		</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<!--/.sidebar-offcanvas-->
			<!--/row-->
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Welcome, <span id="rm_name"><%=name.split("\\(")[0]%></span>
						</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<form action="" method="get">
								<div class="alert alert-info" role="alert"
									style="padding-top: 5px; padding-bottom: 5px;">
									<table
										style="background: transparent; box-shadow: none; border: 0px; color: inherit;">
										<tr>
											<td>Year -</td>
											
												<td><%@ include file="master/DateJsp.jsp"%>
												</td>
												<%
													List<ProjectData> data = ReportingManagerDao.getMyProjects(id, q, y);
												%>
										</tr>
									</table>
								</div>
								</form>
								<p>Projects you are working in,</p>
								<div id="pm_as_pm">
									<table id="myTable" class="display table"
										style="border-radius: 2px;">
										<thead>
											<tr>
												<th>#</th>
												<th class="table-sort">Project Name</th>
												<th class="table-sort">Project Manager</th>
												<th class="table-sort">M1</th>
												<th class="table-sort">M2</th>
												<th class="table-sort">M3</th>
											</tr>
										</thead>
										<tbody>
											<%
												for (int i = 0; i < data.size(); i++) {
											%>
											<tr>
												<td>&nbsp;&nbsp;<%=(i + 1)%></td>
												<td>&nbsp;&nbsp;<a
													href="emp_project.jsp?id=<%=data.get(i).getId()%>"><%=data.get(i).getName()%>
												</a></td>
												<td>&nbsp;&nbsp;<a
													href="employeeDetail.jsp?id=<%=data.get(i).getIdProjectManager()%>"><%=data.get(i).getProjectManager().split("\\(")[0]%></a>
													</td>
												<td>&nbsp;&nbsp;<%=data.get(i).getM1()%></td>
												<td>&nbsp;&nbsp;<%=data.get(i).getM2()%></td>
												<td>&nbsp;&nbsp;<%=data.get(i).getM3()%></td>
											</tr>
											<%
												}
											%>
										</tbody>
									</table>
								</div>

							</div>
							<div class="col-md-8"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<%@include file="master/footer.jsp"%>

	<!-- Table script -->
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/yandex.js"></script>
	<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#myTable').dataTable();
		});
	</script>
</body>
<%
	}
%>
</html>
