<%@page import="com.cisco.rmtool.Admin"%>
<%@page import="com.cisco.rmtool.Employee"%>
<%@page import="com.cisco.rmtool.ProjectManager"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@ include file="../master/datejava.jsp"%>
<%@page import="java.util.List"%>
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String groupId = request.getParameter("groupId");
		List<EmployeeData> data = Admin.getNotifyResReq(groupId);
%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<title>Resource Request</title>
</head>
<body>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Requested Resource</h4>
	</div>
	<% Boolean r = Admin.updateNotificationStatus(groupId); %>
	<div class="modal-body">
		<form action="updateRequests.jsp">
			<table id="myTable" class="display table" style="border-radius: 2px;">
				<thead>
					<tr>
						<th>Name</th>
						<th>Project</th>
						<th>M1</th>
						<th>M2</th>
						<th>M3</th>
						<th>Approve</th>
						<th>Decline</th>
						<th>Comment</th>
					</tr>
				</thead>
				<tbody>
					<%
					int count = 0;
					for (int i = 0; i < data.size(); i++) {
						if (data.get(i).getStatus().equalsIgnoreCase(EmployeeData.PENDING)) {
					%>
						<tr>
							<td	valign="middle">&nbsp;<a onclick="getEmployeeData(<%=data.get(i).getCec_id()%>,<%=data.get(i).getName()%>,<%=data.get(i).getReportingManager()%>);" href="employeeDetail.jsp?id=<%out.print(data.get(i).getCec_id());%>">
							<%out.print(data.get(i).getName().split("\\(")[0]);%></a><input type="hidden" name="empid[]" value="<%out.print(data.get(i).getCec_id());%>"></td>
							<td>&nbsp;<a href="ProjectDetails.jsp?pfm_id=<%=data.get(i).getProject().getId()%>"><%=data.get(i).getProject().getName()%></a><input name="pfm_id[]" type="hidden" value="<%=data.get(i).getProject().getId()%>"></td>
							<td>&nbsp;<input type="hidden" name="M1[]" class="form-control" value="<%=data.get(i).getM1()%>"><%=data.get(i).getM1()%></td>
							<td>&nbsp;<input type="hidden" name="M2[]" class="form-control" value="<%=data.get(i).getM2()%>"><%=data.get(i).getM2()%></td>
							<td>&nbsp;<input type="hidden" name="M3[]" class="form-control" value="<%=data.get(i).getM3()%>"><%=data.get(i).getM3()%></td>
							<td><input type="radio" name="decision<%=i%>" value="APPROVED"></td>
							<td><input type="radio" name="decision<%=i%>" value="DECLINED"></td>
							<td><input type="text" name="comment[]" class="form-control" placeholder="Comments"><input type="hidden" name="group_id[]" value="<%=groupId%>"></td>
						</tr>
					<%
							count++;
						}
					}
					%>
					<%
					for (int i = 0; i < data.size(); i++) {
						if (data.get(i).getStatus().equalsIgnoreCase(EmployeeData.APPROVED)) {
					%>
						<tr>
							<td valign="middle"><a onclick="getEmployeeData(<%=data.get(i).getCec_id()%>,<%=data.get(i).getName()%>,<%=data.get(i).getReportingManager()%>);" href="EmployeeDetails.jsp?cec_id=<%out.print(data.get(i).getCec_id());%>%26flag=t">
							<% out.print(data.get(i).getName().split("\\(")[0]); %></a></td>
							<td>&nbsp;<a href="ProjectDetails.jsp?pfm_id=<%=data.get(i).getProject().getId()%>"><%=data.get(i).getProject().getName()%></a></td>
							<td>&nbsp;<%=data.get(i).getM1()%></td>
							<td>&nbsp;<%=data.get(i).getM2()%></td>
							<td>&nbsp;<%=data.get(i).getM3()%></td>
							<td align="center"><%=data.get(i).getStatus()%></td>
							<td></td>
							<td>&nbsp;<%=data.get(i).getComment()%></td>
						</tr>
					<%
						}
					}
					%>
					<%
					for (int i = 0; i < data.size(); i++) {
						if (data.get(i).getStatus().equalsIgnoreCase(EmployeeData.DECLINED)) {
					%>
						<tr>
							<td valign="middle"><a onclick="getEmployeeData(<%=data.get(i).getCec_id()%>,<%=data.get(i).getName()%>,<%=data.get(i).getReportingManager()%>);"
							href="EmployeeDetails.jsp?cec_id=<%out.print(data.get(i).getCec_id());%>%26flag=t">
							<% out.print(data.get(i).getName().split("\\(")[0]);%></a></td>
							<td>&nbsp;<a href="ProjectDetails.jsp?pfm_id=<%=data.get(i).getProject().getId()%>"><%=data.get(i).getProject().getName()%></a></td>
							<td>&nbsp;<%=data.get(i).getM1()%></td>
							<td>&nbsp;<%=data.get(i).getM2()%></td>
							<td>&nbsp;<%=data.get(i).getM3()%></td>
							<td></td>
							<td align="center"><%=data.get(i).getStatus()%></td>
							<td>&nbsp;<%=data.get(i).getComment()%></td>
						</tr>
					<%
						}
					}
					%>
				</tbody>
			</table>
			<input type="hidden" name="year" value="<%=y%>">
			<input type="hidden" name="quarter" value="<%=q%>">
			<% if(count!=0) {
				 out.print("<input type=\"submit\" class=\"btn btn-primary\" value=\"Submit\">");
				}
			%>
		</form>
	</div>
	<div class="modal-footer">
	</div>
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
</body>
</html>
<%
	}
%>