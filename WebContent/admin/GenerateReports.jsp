<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="org.omg.CORBA.PUBLIC_MEMBER"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="com.cisco.rmtool.AdminDao"%>
<%@page import="com.cisco.rmtool.ProjectManagerDao"%>
<%@page import="com.cisco.rmtool.ReportingManagerDao"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@ include file="master/datejava.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else 
	{
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		String pfm_id = request.getParameter("pfm_id");
		if (pfm_id == null) {
			pfm_id = "All";
		}
		List<ProjectData> prjdata = ProjectManagerDao.getProjects(id, q, y);
		List<ProjectData> data = ProjectManagerDao.getTotFullEmp(id, q, y);
		List<ProjectData> data1 = ProjectManagerDao.getTotTempEmp(id, q, y);
		List<String> a = AdminDao.getProjectMonths(pfm_id);
		List<String> b = QandY.getProjectQuarters(a);
		List<ProjectData> rmemp = ProjectManagerDao.getRMsandEmployeesCountOverall(id, q, y);
		List<EmployeeData> data2 = ReportingManagerDao.getAllEmployeeAllocation(id, q, y);
		List<EmployeeData> dataProject = ReportingManagerDao.getAllEmployeeAllocationInProject(id, pfm_id, q, y);
		List<ProjectData> project = ReportingManagerDao.getProjectsOfAllEmployees(id, q, y);
		List<ProjectData> redata = ReportingManagerDao.getRegionAndEmployeesInProject(id, pfm_id, q, y);
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"	href="../css/jquery.dataTables.min.css">
<style>
.table > tbody > tr > td {
     vertical-align: middle;
}
</style>
<title>Generate Reports</title>
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">

		<!-- Navbar left end, collapsable header Brand info-->
		<%@include file="master/brand.jsp"%>

		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
					<li><a href="dashboard.jsp">Dashboard</a></li>
					<li><a href="EmployeeDetails.jsp">Employee Details</a></li>
					<li><a href="ProjectDetails.jsp">Project Details</a></li>
					<li><a href="addProject.jsp">Add Project</a></li>
					<li  class="active"><a href="GenerateReports.jsp">Generate Reports</a></li>
					<li><a href="notifications.jsp">Notifications</a></li>
			</ul>

			<!-- Navbar right profile and logout -->
			<%@include file="master/navbar_right.jsp"%>

		</div>
	</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<!--/.sidebar-offcanvas-->
			<!--/row-->
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Employee Details
						</h4>
					</div>
					<div class="panel-body">
					<div class="row">
							<div class="col-md-12">
								<form action="" method="get">
									<div class="alert alert-info" role="alert"
										style="padding-top: 5px; padding-bottom: 5px;">
										<table
											style="background: transparent; box-shadow: none; border: 0px; color: inherit;">
											<tr>
												<td>Quarter - </td>
												<td><%@ include file="master/DateJsp.jsp"%></td>
												<td>&nbsp;Project - </td>
													<td><select class="form-control"
														data-live-search="true" name="pfm_id"
														style="min-width: 150px;'" onchange="this.form.submit()">
															<option value="All">All</option>
															<%
																for (int i = 0; i < prjdata.size(); i++) {
																		if (pfm_id.equalsIgnoreCase(prjdata.get(i).getId())) {
																			out.println("<option value=\"" + prjdata.get(i).getId() + "\" selected>"
																					+ prjdata.get(i).getName() + "</option>");
																		} else {
																			out.println("<option value=\"" + prjdata.get(i).getId() + "\">" + prjdata.get(i).getName()
																					+ "</option>");
																		}
																	}
															%>
													</select></td>
											</tr>
										</table>
									</div>
								</form>
								<h4>Reports</h4>
								<div class="row">
									<div class="col-md-12">
										<div id="myCarousel" class="carousel slide" data-ride="carousel">
											<ol class="carousel-indicators">
												<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
												<li data-target="#myCarousel" data-slide-to="1"></li>
												<li data-target="#myCarousel" data-slide-to="2"></li>
											</ol>
											<div class="carousel-inner" role="listbox">
												<div class="item active">
													<center>
														<div id="<%if(pfm_id.equalsIgnoreCase("All"))
																			    	 {
																			    	 	out.print("container");
																			    	 }
																			    	 else
																			    	 {
																			    		 out.print("containerproj");
																			    	 }%>"
																					style="min-width: 400px; height: 400px; margin: 0 auto"></div>
													</center>
												</div>
												<div class="item">
													<center>
														<div id="<%if(pfm_id.equalsIgnoreCase("All"))
																			    	 {
																			    	 	out.print("progress");
																			    	 }
																			    	 else
																			    	 {
																			    		 out.print("progressproj");
																			    	 }%>"
																					style="min-width: 600px; height: 400px; margin: 0 auto"></div>
													</center>
												</div>
												<div class="item">
													<center>
														<div id="<%if(pfm_id.equalsIgnoreCase("All"))
																			    	 {
																			    	 	out.print("manager");
																			    	 }
																			    	 else
																			    	 {
																			    		 out.print("managerproject");
																			    	 }%>"
																					style="min-width: 400px; height: 400px; margin: 0 auto"></div>
													</center>
												</div>
											</div>
											<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
											<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
											</a>
											<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
											<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<form action="" method="get">
								<div class="alert alert-info" role="alert"
									style="padding-top: 5px; padding-bottom: 5px;">
									<table
										style="background: transparent; box-shadow: none; border: 0px; color: inherit;">
										<tr>
											<td>Quarter - </td>
											<td><%@ include file="master/DateJsp.jsp"%></td>
											<td>&nbsp;Project - </td>
													<td><select class="form-control selectpicker"
														data-live-search="true" name="pfm_id"
														style="min-width: 150px;'" onchange="this.form.submit()">
															<option value="All">All</option>
															<%
																for (int i = 0; i < project.size(); i++) {
																		if (pfm_id.equalsIgnoreCase(project.get(i).getId())) {
																			out.println("<option value=\"" + project.get(i).getId() + "\" selected>"
																					+ project.get(i).getName() + "</option>");
																		} else {
																			out.println("<option value=\"" + project.get(i).getId() + "\">" + project.get(i).getName()
																					+ "</option>");
																		}
																	}
															%>
													</select></td>
										</tr>
									</table>
								</div>
								</form>
								<div id="myCarousel" class="carousel slide" data-ride="carousel">
							  <!-- Indicators -->
							  <ol class="carousel-indicators">
							    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							    <li data-target="#myCarousel" data-slide-to="1"></li>
							    <li data-target="#myCarousel" data-slide-to="2"></li>
							    <li data-target="#myCarousel" data-slide-to="3"></li>
							  </ol>
							
							  <!-- Wrapper for slides -->
							   <div class="carousel-inner" role="listbox">
							    <div class="item active">
										<div id="<%if(pfm_id.equalsIgnoreCase("All"))
							    	 {
							    	 	out.print("MonthlyAllocationofReportingEmployees");
							    	 }
							    	 else
							    	 {
							    		 out.print("MonthlyAllocationofReportingEmployeesInProject");
							    	 }%>"
									style="min-width: 310px; height: 400px; margin: 0 auto"></div>
							    </div>
							
							    <div class="item"><center>
							     <div id="<%if(pfm_id.equalsIgnoreCase("All"))
							    	 {
							    	 	out.print("NumberofEmployeesReportingtoRMvsTotalEmployees");
							    	 }
							    	 else
							    	 {
							    		 out.print("NumberofEmployeesReportingtoRMvsTotalEmployeesProj");
							    	 }%>"
									style="min-width: 400px; height: 400px; margin: 0 auto"></div></center>
							    </div>
							
							    <div class="item"><center>
							      <div id="<%if(pfm_id.equalsIgnoreCase("All"))
							    	 {
							    	 	out.print("RegionWiseEmployeesReportingtoRM");
							    	 }
							    	 else
							    	 {
							    		 out.print("RegionWiseEmployeesReportingtoRMInProject");
							    	 }%>"
									style="min-width: 400px; height: 400px; margin: 0 auto"></div></center>
							    </div>
							  </div>
							  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
							    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							    <span class="sr-only">Previous</span>
							  </a>
							  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
							    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							    <span class="sr-only">Next</span>
							  </a>
							</div>
							</div>
						</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<%@include file="master/footer.jsp"%>
<script type="text/javascript" src="../js/jquery-1.12.0.min.js"></script>
	<script src="../js/highcharts.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/yandex.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/offline-exporting.js"></script>
	<script>
	$(function () {
	    $('#container').highcharts({
	    	colors: ['#696969','#7cb5ec'],
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Full Time and Temporary Employees'
	        },
	        credits: {
	            enabled: false
	        },
	        xAxis: {
	            categories: [<%for (int i = 0; i < prjdata.size(); i++) {
					out.print("'" + prjdata.get(i).getName() + "'");
					if (i < prjdata.size() - 1) {
						out.print(",");
					}
				}%>]
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Number of Employees'
	            },
	            stackLabels: {
	                enabled: true,
	                style: {
	                    fontWeight: 'bold',
	                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
	                }
	            }
	        },
	        tooltip: {
	            headerFormat: '<b>{point.x}</b><br/>',
	            pointFormat: 'Total Employees: {point.stackTotal}<br/>{series.name}: {point.y}'
	        },
	        plotOptions: {
	            column: {
	                stacking: 'normal',
	                dataLabels: {
	                    enabled: true,
	                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
	                    style: {
	                        textShadow: '0 0 3px black'
	                    }
	                }
	            }
	        },
	        series: [{
	            name: 'Temporaray Employees',
	            data: [<%for (int i = 0; i < data1.size(); i++) {
					out.print(data1.get(i).getTotalEmployees());
					if (i < data1.size() - 1) {
						out.print(",");
					}
				}%>]
	        },{
	            name: 'Fulltime Employees',
	            data: [<%for (int i = 0; i < data.size(); i++) {
					out.print(data.get(i).getTotalEmployees());
					if (i < data.size() - 1) {
						out.print(",");
					}
				}%>]
	        }]
	    });
	});
</script>
<script>
$(function () {
    $('#progress').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Project Progress'
        },
        xAxis: {
            categories: [<%for (int i = 0; i < prjdata.size(); i++) {
					out.print("'" + prjdata.get(i).getName() + "'");
					if (i < prjdata.size() - 1) {
						out.print(",");
					}
				}%>]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Progress Percent'
            }
        },
        legend: {
            reversed: true
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: 'Progress',
            data: [<%int progress;
				for (int i = 0; i < prjdata.size(); i++) {
					if (Integer.parseInt(prjdata.get(i).getProgress()) > 100) {
						progress = 100;
					} else {
						progress = Integer.parseInt(prjdata.get(i).getProgress());
					}
					out.print(progress);
					if (i < prjdata.size() - 1) {
						out.print(",");
					}
				}%>]
        }]
    });
});
</script>
<script>
$(function () {
    $('#progressproj').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Project Progress'
        },
        xAxis: {
            categories: [<%for (int i = 0; i < prjdata.size(); i++) {
					if (pfm_id.equalsIgnoreCase(prjdata.get(i).getId())) {
						out.print("'" + prjdata.get(i).getName() + "'");
					}
				}%>]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Progress Percent'
            }
        },
        legend: {
            reversed: true
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: 'Progress',
            data: [<%
				for (int i = 0; i < prjdata.size(); i++) {
					if (Integer.parseInt(prjdata.get(i).getProgress()) > 100) {
						progress = 100;
					} else {
						progress = Integer.parseInt(prjdata.get(i).getProgress());
					}
					if (pfm_id.equalsIgnoreCase(prjdata.get(i).getId())) {
						out.print(progress);
					}
				}%>]
        }]
    });
});
</script>
	<script type="text/javascript">
	        $('#manager').highcharts({
	            chart: {
	                plotBackgroundColor: null,
	                plotBorderWidth: null,
	                plotShadow: false,
	                type: 'pie'
	            },
	            title: {
	                text: 'Manager wise Employee Contribution'
	            },
	            tooltip: {
	                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	            },
	            plotOptions: {
	                pie: {
	                    allowPointSelect: true,
	                    cursor: 'pointer',
	                    dataLabels: {
	                        enabled: false
	                    },
	                    showInLegend: true
	                }
	            },
	            series: [{
	                name: 'Brands',
	                colorByPoint: true,
	                data: [<% for(int i = 0; i < rmemp.size(); i++)
			                {
			                	out.println("{name:" + "'" + rmemp.get(i).getRMName() + "'" + ",y:" + rmemp.get(i).getNoOfEmployees() + "}");
			                	if (i < rmemp.size() - 1){
			        				out.println(",");
			        			}
			                }
	                %>]
	            }]
	        });
	</script>
	<script type="text/javascript">
	$('#managerproject').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Manager wise Employee Contribution'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        }
        <%=ProjectManagerDao.getRMsandEmployeesCountChart(pfm_id, q, y, "APPROVED")%>
    });
	</script>
	<script>
$(function () {
    $('#containerproj').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'Fulltime and Temporary Employees'
        },
        subtitle: {
            text: ''
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        series: [{
            name: 'Total',
            data: [
                ['Temporary Employees', <%for (int i = 0; i < data1.size(); i++) {
											if (pfm_id.equalsIgnoreCase(data1.get(i).getId())) {
												out.print(data1.get(i).getTotalEmployees());
											}
										}%>],
                ['Fulltime Employees', <%for (int i = 0; i < data.size(); i++) {
											if (pfm_id.equalsIgnoreCase(data.get(i).getId())) {
												out.print(data.get(i).getTotalEmployees());
											}
										}%>]
            ]
        }]
    });
});
</script>
<script>
			$(function() {
		        $('#MonthlyAllocationofReportingEmployees').highcharts({
		            chart: {
		        type: 'column'
		    },
		    title: {
		        text: 'Monthly Employee Allocation'
		    },
		    xAxis: {
		        categories: [<% for(int i = 0; i < data2.size(); i++) {
		        				out.print("'" + data2.get(i).getName().split("\\(")[0] + "'");
		        				if (i < data2.size() - 1){
		        					out.print(",");
		        				}
		       				}%>]
		    },
		    credits: {
		        enabled: false
		    },
		    series: [{
		        name: 'Month 1',
		        data: [<% for(int i = 0; i < data2.size(); i++) {
		        				out.print(Integer.parseInt(data2.get(i).getM1()) - 100);
		        				if (i < data2.size() - 1){
		        					out.print(",");
		        				}
		       				}%>]
		    }, {
		        name: 'Month 2',
		        data: [<% for(int i = 0; i < data2.size(); i++) {
		        				out.print(Integer.parseInt(data2.get(i).getM2()) - 100);
		        				if (i < data2.size() - 1){
		        					out.print(",");
		        				}
		       				}%>]
		    }, {
		        name: 'Month 3',
		        data: [<% for(int i = 0; i < data2.size(); i++) {
		        				out.print(Integer.parseInt(data2.get(i).getM3()) - 100);
		        				if (i < data2.size() - 1){
		        					out.print(",");
		        				}
		       				}%>]
		    }]
		        });
		    });
		</script>
		<script>
			$(function() {
		        $('#MonthlyAllocationofReportingEmployeesInProject').highcharts({
		            chart: {
		        type: 'column'
		    },
		    title: {
		        text: 'Monthly Employee Allocation'
		    },
		    xAxis: {
		        categories: [<% for(int i = 0; i < dataProject.size(); i++) {
		        				out.print("'" + dataProject.get(i).getName().split("\\(")[0] + "'");
		        				if (i < dataProject.size() - 1){
		        					out.print(",");
		        				}
		       				}%>]
		    },
		    credits: {
		        enabled: false
		    },
		    series: [{
		        name: 'Month 1',
		        data: [<% for(int i = 0; i < dataProject.size(); i++) {
		        				out.print(dataProject.get(i).getM1());
		        				if (i < dataProject.size() - 1){
		        					out.print(",");
		        				}
		       				}%>]
		    }, {
		        name: 'Month 2',
		        data: [<% for(int i = 0; i < dataProject.size(); i++) {
		        				out.print(dataProject.get(i).getM2());
		        				if (i < dataProject.size() - 1){
		        					out.print(",");
		        				}
		       				}%>]
		    }, {
		        name: 'Month 3',
		        data: [<% for(int i = 0; i < dataProject.size(); i++) {
		        				out.print(dataProject.get(i).getM3());
		        				if (i < dataProject.size() - 1){
		        					out.print(",");
		        				}
		       				}%>]
		    }]
		        });
		    });
		</script>
		<script>
			$(function () {
			    $('#NumberofEmployeesReportingtoRMvsTotalEmployees').highcharts({
			        chart: {
			            type: 'column'
			        },
			        title: {
			            text: 'Project wise Employee Contribution Ratio'
			        },
			        xAxis: {
			            categories: [<%for (int i = 0; i < project.size(); i++) {
														out.print("'" + project.get(i).getName() + "'");
														if (i < project.size() - 1) {
															out.print(",");
														}
													}%>
												]
			        },
			        yAxis: [{
			            min: 0,
			            title: {
			                text: 'Number of Employees'
			            }
			        }, {
			            title: {
			                text: 'Number of Employees'
			            },
			            opposite: true
			        }],
			        legend: {
			            shadow: false
			        },
			        tooltip: {
			            shared: true
			        },
			        plotOptions: {
			            column: {
			                grouping: false,
			                shadow: false,
			                borderWidth: 0
			            }
			        },
			        series: [{
			            name: 'Total Employees',
			            color: 'rgba(165,170,217,1)',
			            data: [<%for (int i = 0; i < project.size(); i++) {
												out.print(project.get(i).getTotalEmployees());
												if (i < project.size() - 1) {
													out.print(",");
												}
											}%>
										],
			           // pointPadding: 0.3,
			            //pointPlacement: -0.2
			        }, {
			            name: 'Employees Reporting to you',
			            color: 'rgba(126,86,134,.9)',
			            data: [<%for (int i = 0; i < project.size(); i++) {
												out.print(project.get(i).getNoOfEmployees());
												if (i < project.size() - 1) {
													out.print(",");
												}
											}%>
										],
			            pointPadding: 0.2,
			            //pointPlacement: -0.2
			        }]
			    });
			});
		</script>
		<script>
				$('#RegionWiseEmployeesReportingtoRM').highcharts({
			        chart: {
			            plotBackgroundColor: null,
			            plotBorderWidth: null,
			            plotShadow: false,
			            type: 'pie'
			        },
			        title: {
			            text: 'Regional Employee Distribution'
			        },
			        tooltip: {
			            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			        },
			        credits: {
			            enabled: false
			        },
			        plotOptions: {
			            pie: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                dataLabels: {
			                    enabled: true,
			                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
			                    style: {
			                        color: (Highcharts.theme && 
			
			Highcharts.theme.contrastTextColor) || 'black'
			                    }
			                }
			            }
			        }
			        <%=ReportingManagerDao.RegionsChart(id)%>
			    });
			
				</script>
				<script>
				$(function () {
				    $('#NumberofEmployeesReportingtoRMvsTotalEmployeesProj').highcharts({
				        chart: {
				            type: 'column'
				        },
				        title: {
				            text: 'Employee Contribution Ratio'
				        },
				        xAxis: {
				            categories: [<%for (int i = 0; i < project.size(); i++) {
															if (pfm_id.equalsIgnoreCase(project.get(i).getId())) {
																out.print("'" + project.get(i).getName() + "'");
															}
														}%>
													]
				        },
				        yAxis: [{
				            min: 0,
				            title: {
				                text: 'Number of Employees'
				            }
				        }, {
				            title: {
				                text: 'Number of Employees'
				            },
				            opposite: true
				        }],
				        legend: {
				            shadow: false
				        },
				        tooltip: {
				            shared: true
				        },
				        plotOptions: {
				            column: {
				                grouping: false,
				                shadow: false,
				                borderWidth: 0
				            }
				        },
				        series: [{
				            name: 'Total Employees',
				            color: 'rgba(165,170,217,1)',
				            data: [<%for (int i = 0; i < project.size(); i++) {
						            	if (pfm_id.equalsIgnoreCase(project.get(i).getId())) {
											out.print(project.get(i).getTotalEmployees());
										}
												}%>
											],
				           // pointPadding: 0.3,
				            //pointPlacement: -0.2
				        }, {
				            name: 'Employees Reporting to you',
				            color: 'rgba(126,86,134,.9)',
				            data: [<%for (int i = 0; i < project.size(); i++) {
													if (pfm_id.equalsIgnoreCase(project.get(i).getId())) {
														out.print(project.get(i).getNoOfEmployees());
													}
												}%>
											],
				            pointPadding: 0.2,
				            //pointPlacement: -0.2
				        }]
				    });
				});
				</script>
				<script>
				$('#RegionWiseEmployeesReportingtoRMInProject').highcharts({
			        chart: {
			            plotBackgroundColor: null,
			            plotBorderWidth: null,
			            plotShadow: false,
			            type: 'pie'
			        },
			        title: {
			            text: 'Regional Employee Distribution'
			        },
			        tooltip: {
			            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			        },
			        credits: {
			            enabled: false
			        },
			        plotOptions: {
			            pie: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                dataLabels: {
			                    enabled: true,
			                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
			                    style: {
			                        color: (Highcharts.theme && 
			
			Highcharts.theme.contrastTextColor) || 'black'
			                    }
			                }
			            }
			        },
			        series: [{ name: 'Percent of Employees', colorByPoint: true, data: [<%for(int i = 0; i <redata.size(); i++) {
			        	out.print("{name:'" + redata.get(i).getRegion() + "'," + "y:" + redata.get(i).getNoOfEmployees() + "}");
			        	if(i < redata.size() - 1){
			        		out.print(",");
			        	}
			        } %>]}]
			    });
			
				</script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.4/js/bootstrap-select.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.4/js/i18n/defaults-*.min.js"></script>
</body>
</html>
<%
	}
%>