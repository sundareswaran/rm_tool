<%@page import="com.cisco.rmtool.AdminDao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.cisco.rmtool.AdminDao"%>
<%@page import="com.cisco.rmtool.Notification"%>
<%@page import="com.cisco.rmtool.NotificationData"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	out.print(session.getAttribute("session"));
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		// Here i will use the ProjectManagerProjects class to get the data

		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		List<NotificationData> unread = Notification.getAdminUnreadNotifications();
		List<NotificationData> read = Notification.getAdminReadNotifications();
		int unnot = AdminDao.getNumberofUnreadNotifications();
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Notifications - Admin</title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="../css/jquery.dataTables.min.css">
<style>
#myModal .modal-dialog
{
  width: 70%;
}
</style>
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">

		<!-- Navbar left end, collapsable header Brand info-->
		<%@include file="master/brand.jsp"%>

		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="dashboard.jsp">Dashboard</a></li>
					<li><a href="EmployeeDetails.jsp">Employee Details</a></li>
					<li><a href="ProjectDetails.jsp">Project Details</a></li>
					<li><a href="addProject.jsp">Add Project</a></li>
					<li><a href="Reports.jsp">Reports</a></li>
					<li class="active"><a href="notifications.jsp">Notifications<% if(unnot != 0) {
						out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
					}%></a></li>
			</ul>

			<!-- Navbar right profile and logout -->
			<%@include file="master/navbar_right.jsp"%>

		</div>
	</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Welcome, <span id="admin_name"><%out.print(name.split("\\(")[0]);%></span>
						</h4>
					</div>
					<div class="panel-body">
					<div class="row">
				<div class="col-md-12">
				<h4>Notifications</h4>
				  <ul class="nav nav-tabs">
				    <li class="active"><a data-toggle="tab" href="#notiunread">Unread<% if(unnot != 0) {
						out.print("(" + unnot + ")");
					}%></a></li>
				    <li><a data-toggle="tab" href="#notiread">Read</a></li>
				  </ul>
				  
				  <div class="tab-content">
			    <div id="notiunread" class="tab-pane fade in active">
			    	<br />
			    	<table id="Unread" class="display table"
									style="border-radius: 2px;">
									<thead>
										<tr>
											<th>Project Name</th>
											<th>Project Manager</th>
											<th>Total Resource Requested</th>
											<th>Date</th>
											<th>Time</th>
										</tr>
									</thead>
									<%
										for (int i = 0; i < unread.size(); i++) {
									%>
									<tr data-toggle="modal" href="remote/notifyResReq.jsp?groupId=<%out.print(unread.get(i).getGroupId());%>" data-target="#myModal">
										<td>
											<%
												out.println(unread.get(i).getProject().getPrjName());
											%>
										</td>
										<td>
											<%
												out.println(unread.get(i).getProject().getProjectManager());
											%>
										</td>
										<td>
											<%
												out.println(unread.get(i).getCount());
											%>
										</td>
										<td>
											<%
												out.println(unread.get(i).getDate().split(" ")[0]);
											%>
										</td>
										<td>
											<%
												out.println(unread.get(i).getTime().split("\\.")[0]);
											%>
										</td>
									</tr>
									<%
										}
									%>
								</table>
			    </div>
			    <div id="notiread" class="tab-pane fade">
			    <br />
			     <table id="Read" class="display table"
									style="border-radius: 2px;">
									<thead>
										<tr>
											<th>Project Name</th>
											<th>Project Manager</th>
											<th>Total Resource Requested</th>
											<th>Date</th>
											<th>Time</th>
										</tr>
									</thead>
									<%
										for (int i = 0; i < read.size(); i++) {
									%>
									<tr data-toggle="modal" href="remote/notifyResReq.jsp?groupId=<%out.print(read.get(i).getGroupId());%>" data-target="#myModal">
										<td>
											<%
												out.println(read.get(i).getProject().getPrjName());
											%>
										</td>
										<td>
											<%
												out.println(read.get(i).getProject().getProjectManager());
											%>
										</td>
										<td>
											<%
												out.println(read.get(i).getCount());
											%>
										</td>
										<td>
											<%
												out.println(read.get(i).getDate().split(" ")[0]);
											%>
										</td>
										<td>
											<%
												out.println(read.get(i).getTime().split("\\.")[0]);
											%>
										</td>
									</tr>
									<%
										}
									%>
								</table>
				</div>
			    </div>
			    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									    <div class="modal-dialog">
									        <div class="modal-content">
									            <div class="modal-header">
									                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									                 <h4 class="modal-title">Resource Request</h4>
									            </div>
									            <div class="modal-body"><div class="te"></div></div>
									            <div class="modal-footer">
									                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									            </div>
									        </div>
									    </div>
									</div>
			  </div>
			</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Footer -->
	<%@include file="master/footer.jsp"%>
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/yandex.js"></script>
	<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#Unread').dataTable();
		});
	</script>
	<script>
		$(document).ready(function() {
			$('#Read').dataTable();
		});
	</script>
</body>
</html>
<%
	}
%>