<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="org.omg.CORBA.PUBLIC_MEMBER"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="com.cisco.rmtool.AdminDao"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@ include file="master/datejava.jsp"%>
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		String pfm_id = request.getParameter("pfm_id");
		List<EmployeeData> emp = AdminDao.getAllEmployees();
		List<EmployeeData> emps = AdminDao.getEmployeesNotAdminNotRM();
		List<ProjectData> data = AdminDao.getAllProjects(q, y);
		int unnot = AdminDao.getNumberofUnreadNotifications();
%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../css/buttons.dataTables.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.4/css/bootstrap-select.min.css">
<title>Project Manager</title>
<script>
function alerts() {
	var alert = location.search.split('alert=')[1] ? location.search.split('alert=')[1] : 'false';
	if(alert === "success") {
		document.getElementById("div_alert").innerHTML = '<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> <br/>New temporary resource added!</div>';
	} else if(alert === "failed") {
		document.getElementById("div_alert").innerHTML = '<div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Failure!</strong> <br/>New resource was not added!</div>';
	}
}
</script>
</head>
<body style="min-height: auto;"  onload="alerts();">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">

			<!-- Navbar left end, collapsable header Brand info-->
			<%@include file="master/brand.jsp"%>

			<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
					<li><a href="dashboard.jsp">Dashboard</a></li>
					<li><a href="EmployeeDetails.jsp">Employee Details</a></li>
					<li><a href="ProjectDetails.jsp">Project Details</a></li>
					<li  class="active"><a href="addProject.jsp">Add Project</a></li>
					<li><a href="Reports.jsp">Reports</a></li>
					<li><a href="notifications.jsp">Notifications<% if(unnot != 0) {
						out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
					}%></a></li>
				</ul>
				
				<!-- Navbar right profile and logout -->
				<%@include file="master/navbar_right.jsp"%>

			</div>
		</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<!--/.sidebar-offcanvas-->
			<!--/row-->
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Welcome, <span id="admin_name"><%out.print(name.split("\\(")[0]);%></span>
						</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
							<form class="form-horizontal" method="post" action="../AddProject">
				
				<div class="form-group row">
	                <label for="inputKey" class="col-md-2 control-label">PFM ID</label>
	                <div class="col-md-2">
	                    <input type="number" class="form-control" id="pfm_id" name="pfm_id" required>
	                </div>
	                <label for="inputValue" class="col-md-2 control-label">Project Name</label>
	                <div class="col-md-2">
	                    <input type="text" class="form-control" id="project_name" name="project_name" required>
	                </div>
	            </div>
	            <div class="form-group row">
	                <label for="inputKey" class="col-md-2 control-label">Project Description</label>
	                <div class="col-md-2">
	                    <input type="text" class="form-control" id="project_description" name="project_description">
	                </div>
	                <label for="inputValue" class="col-md-2 control-label">Solution Manager</label>
	                <div class="col-md-2">
	                   <select class="form-control selectpicker" data-live-search="true" name="solution_manager">
					<option value="">&nbsp;</option>
					   <% 
					  	 for(int i = 0; i<emp.size(); i++)
					  	 {
					  		 out.println("<option value=\"" + emp.get(i).getCec_id() + "\">" +  emp.get(i).getName().split("\\(")[0] + "</option>");
					  	 }
					  %>
				</select>
	                </div>
	            </div>
	            <div class="form-group row">
	                <label for="inputKey" class="col-md-2 control-label">Delivery Manager</label>
	                <div class="col-md-2">
	                    <select class="form-control selectpicker" data-live-search="true" name="delivery_manager">
					<option value="">&nbsp;</option>
					  <% 
					  	 for(int i = 0; i<emp.size(); i++)
					  	 {
					  		 out.println("<option value=\"" + emp.get(i).getCec_id() + "\">" +  emp.get(i).getName().split("\\(")[0] + "</option>");
					  	 }
					  %>
				</select>
	                </div>
	                <label for="inputValue" class="col-md-2 control-label">Project Manager</label>
	                <div class="col-md-2">
	                    <select class="form-control selectpicker" data-live-search="true" name="project_manager" required>
					<option value="">&nbsp;</option>
					  <% 
					  	 for(int i = 0; i<emps.size(); i++)
					  	 {
					  		 out.println("<option value=\"" + emps.get(i).getCec_id() + "\">" +  emps.get(i).getName().split("\\(")[0] + "</option>");
					  	 }
					  %>
				</select>
	                </div>
	            </div>
	            <div class="form-group row">
	                <label for="inputKey" class="col-md-2 control-label">Budget</label>
	                <div class="col-md-2">
	                 	<div class="input-group">
					<span class="input-group-addon">$</span>
					<input type="number" class="form-control" id="budget" name="budget" required>
				</div>
	                </div>
	                <label for="inputValue" class="col-md-2 control-label">Target Release Quarter</label>
	                <div class="col-md-2">
	                    <input type="text" class="form-control" id="target_release_quarter" name="target_release_quarter" required>
	                </div>
	            </div>
	             <div class="form-group row">
	                <label for="inputKey" class="col-md-2 control-label">Start Date</label>
	                <div class="col-md-2">
	                 	<div class="input-group">
					<input type="date" class="form-control" id="start_date" name="start_date" required>
				</div>
	                </div>
	                <label for="inputValue" class="col-md-2 control-label">End Date</label>
	                <div class="col-md-2">
	                    <input type="date" class="form-control" id="end_date" name="end_date" required>
	                </div>
	            </div>
	             <div class="form-group row">
	                <label for="inputKey" class="col-md-2 control-label">Status</label>
	                <div class="col-md-2">
	                 	<div class="input-group">
						<input type="text" class="form-control" id="status" name="status" required>
				</div>
	                </div>
	                <label for="inputValue" class="col-md-2 control-label">Funding Source</label>
	                <div class="col-md-2">
						<input type="text" class="form-control" id="funding_source" name="funding_source" required>
	                </div>
	            </div>
				<div class="form-group row">
				<div class="col-md-8" align="right">
										<input type="submit"  class="btn btn-primary" value="Submit">
										</div>
				</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	<!-- Footer -->
	<%@include file="master/footer.jsp"%>

	<!-- Table script -->
	<script type="text/javascript" src="../js/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="../js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="../js/jszip.min.js"></script>
	<script type="text/javascript" src="../js/pdfmake.min.js"></script>
	<script type="text/javascript" src="../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../js/buttons.html5.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/yandex.js"></script>
	<script>
	$(document).ready(function() {
	    $('#myTable').DataTable( {
	        dom: 'Bfrtip',
	        buttons: [
	            {
	                extend:    'copyHtml5',
	                text:      '<i class="fa fa-files-o"></i>',
	                titleAttr: 'Copy'
	            },
	            {
	                extend:    'excelHtml5',
	                text:      '<i class="fa fa-file-excel-o"></i>',
	                titleAttr: 'Excel'
	            },
	            {
	                extend:    'csvHtml5',
	                text:      '<i class="fa fa-file-text-o"></i>',
	                titleAttr: 'CSV'
	            },
	            {
	                extend:    'pdfHtml5',
	                text:      '<i class="fa fa-file-pdf-o"></i>',
	                titleAttr: 'PDF'
	            }
	        ]
	    } );
	} );
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.4/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.4/js/i18n/defaults-*.min.js"></script>
</body>
</html>
<%
	}
%>