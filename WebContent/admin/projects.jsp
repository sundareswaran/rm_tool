
<%@page import="com.cisco.rmtool.ReportingManagerDao"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {

		Calendar c = Calendar.getInstance();
		int month = c.get(Calendar.MONTH);
		int y = c.get(Calendar.YEAR);
		int quarter = (int) Math.ceil((((month + 1) / 3)));
		String q = "Q" + quarter;
		if (request.getParameter("quarter") != null) {
			q = request.getParameter("quarter");
		}
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		List<EmployeeData> data = ReportingManagerDao.getProjectResources(id, q, "" + y);

		ProjectData pdata = ReportingManagerDao.getProjectDetails(id);
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Reporting Manager</title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link href="../css/tables.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="../css/tablesort.css">
<link rel="stylesheet" type="text/css"
	href="../css/styles_tablesort.css">
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">

		<!-- Navbar left end, collapsable header Brand info-->
		<%@include file="master/brand.jsp"%>

		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
					<li><a href="dashboard.jsp">Home</a></li>
					<li><a href="Reports.jsp">Reports</a></li>
					<li><a href="myProjects.jsp">My Projects</a></li>
					<li><a href="resourcerequest.jsp">Resources Request</a></li>
					<li><a href="#">Notifications</a></li>
				</ul>

			<!-- Navbar right profile and logout -->
			<%@include file="master/navbar_right.jsp"%>

		</div>
	</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<!--/.sidebar-offcanvas-->
			<!--/row-->
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							<span id="pm_user_name"><%=name%></span>
						</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-8">
								<div class="alert alert-info" role="alert"
									style="padding-top: 5px; padding-bottom: 5px;">
									<table
										style="background: transparent; box-shadow: none; border: 0px; color: inherit;">
										<tr>
											<td style="width: 235px; vertical-align: middle;"><label
												for="quarter">Current Quarter - <%=c.get(Calendar.YEAR)%>
													- Quarter
											</label></td>
											<td><form action="" method="get">
													<select id="quarter" name="quarter" class="form-control"
														style="width: 70px; vertical-align: middle;"
														onchange="this.form.submit()">
														<%
															for (int i = 1; i <= 4; i++) {
																	if (q.equalsIgnoreCase("Q" + i))
																		out.print("<option selected value=\"Q" + i + "\">Q" + i + "</option>");
																	else
																		out.print("<option value=\"Q" + i + "\">Q" + i + "</option>");
																}
														%>
													</select> <input type="hidden" name="id" value="<%=id%>"> <input
														type="hidden" name="name" value="<%=name%>">
												</form></td>
										</tr>
									</table>
								</div>
								<p>Resources in project</p>
								<div id="pm_as_pm">
									<table class="table-sort table-sort-search">
										<thead>
											<tr>
												<th>#</th>
												<th class="table-sort">Name</th>
												<th class="table-sort">M1</th>
												<th class="table-sort">M2</th>
												<th class="table-sort">M3</th>
												<th class="table-sort">Reporting Manager</th>
												<th class="table-sort">Status</th>
											</tr>
										</thead>
										<tbody>
											<%
												for (int i = 0; i < data.size(); i++) {
											%>
											<tr>
												<td>
													<%
														out.print((i + 1) + "");
													%>
												</td>
												<td><a onclick="getEmployeeData(<%=data.get(i).getCec_id()%>,<%=data.get(i).getName()%>,<%=data.get(i).getReportingManager()%>);"
													href="employeeDetail.jsp?id=<%=data.get(i).getCec_id()%>"> <%out.print(data.get(i).getName());%>
												</a></td>
												<td>
													<%
														out.print(data.get(i).getM1());
													%>
												</td>
												<td>
													<%
														out.print(data.get(i).getM2());
													%>
												</td>
												<td>
													<%
														out.print(data.get(i).getM3());
													%>
												</td>
												<td>
													<%
														out.print(data.get(i).getReportingManager());
													%>
												</td>
												<td>
													<%
														out.print(data.get(i).getStatus());
													%>
												</td>
											</tr>
											<%
												}
											%>
										</tbody>
									</table>
								</div>

							</div>
							<div class="col-md-4">
								<div class="list-group">
									<a class="list-group-item active"><strong><%=request.getParameter("name")%></strong></a>
									<a class="list-group-item ">Start Date: <strong><%=pdata.getStart().split(" ")[0]%></strong></a>
									<a class="list-group-item ">Target Date: <strong><%=pdata.getEnd().split(" ")[0]%></strong></a>
									<a class="list-group-item ">Budget: <strong><%=pdata.getBudget()%></strong></a>
								</div>
								<div class="list-group">
								<div id="container"
									style="min-width: 200px; height: 200px; margin: 0 auto"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<%@include file="master/footer.jsp"%>

	<!-- Table script -->
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script src="../js/yandex.js"></script>
	<script type="text/javascript" src="../js/tablesort.js"></script>
	<script src="../js/highcharts.js"></script>
	<script type="text/javascript">
		//This script loads all the necessary data from the tablesort.js file
		$(function() {
			$('table.table-sort').tablesort();
			hljs.initHighlightingOnLoad(); // Syntax Hilighting
		});
	</script>
	<script>
	$('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Region Wise'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && 

Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        }
        <%=ReportingManagerDao.PWRegionsChart(id, q, "" + y)%>
    });
	
	</script>
</body>
<%
	}
%>>
</html>
