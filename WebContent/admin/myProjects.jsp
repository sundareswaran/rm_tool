<%@page import="com.cisco.rmtool.ReportingManagerDao"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.cisco.rmtool.ProjectManagerDao"%>
<%@page import="org.omg.CORBA.PUBLIC_MEMBER"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%
	out.print(session.getAttribute("session"));
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		// Here i will use the ProjectManagerProjects class to get the data

		String id = session.getAttribute("id").toString();

		Calendar c = Calendar.getInstance();
		int month = c.get(Calendar.MONTH);
		int y = c.get(Calendar.YEAR);
		int quarter = ((int) ((month + 1) / 3));
		String q = "Q" + quarter;
		if (request.getParameter("quarter") != null) {
			q = request.getParameter("quarter");
		}
		List<ProjectData> data = ReportingManagerDao.getMyProjects(id, q, "" + y );
%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Projects</title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link href="../css/tables.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="../css/tablesort.css">
<link rel="stylesheet" type="text/css"
	href="../css/styles_tablesort.css">
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">

			<!-- Navbar left end, collapsable header Brand info-->
			<%@include file="master/brand.jsp"%>

			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="dashboard.jsp">Home</a></li>
					<li><a href="Reports.jsp">Reports</a></li>
					<li class="active"><a href="myProjects.jsp">My Projects</a></li>
					<li><a href="resourcerequest.jsp">Resources Request</a></li>
					<li><a href="#">Notifications</a></li>
				</ul>

				<!-- Navbar right profile and logout -->
				<%@include file="master/navbar_right.jsp"%>

			</div>
		</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<!--/.sidebar-offcanvas-->
			<!--/row-->
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Welcome, <span id="pm_user_name"><%=session.getAttribute("name")%></span>
						</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="alert alert-info" role="alert"
									style="padding-top: 5px; padding-bottom: 5px;">
									<table style="background: transparent;box-shadow: none; border:0px; color: inherit;"><tr><td style="width:235px; vertical-align: middle;"><label for="quarter">Current Quarter - <span id="cur_quarter"><%=c.get(Calendar.YEAR)%>
										- Quarter</span></label></td><td>
									<form action="" method="get">
										<select id="quarter"  class="form-control" style="width:75px" name="quarter"
											onchange="this.form.submit()">
											<%
												for (int i = 1; i <= 4; i++) {
														if (q.equalsIgnoreCase("Q" + i))
															out.print("<option selected value=\"Q" + i + "\">Q" + i + "</option>");
														else
															out.print("<option value=\"Q" + i + "\">Q" + i + "</option>");
													}
											%>
										</select></td></tr></table>
									</form>
								</div>
								<p>Projects you are working in,</p>
								<div id="pm_as_pm">
									<table class="table-sort table-sort-search">
										<thead>
											<tr>
												<th>#</th>
												<th class="table-sort">Project Name</th>
												<th class="table-sort">Project Manager</th>
												<th class="table-sort">Delivery Manager</th>
												<th class="table-sort">Solution Manager</th>
												<th class="table-sort">Budget</th>
												<th class="table-sort">Start Date</th>
												<th class="table-sort">Target Release</th>
											</tr>
										</thead>
										<tbody>
											<%
												for (int i = 0; i < data.size(); i++) {
											%>
											<tr>
												<td><%=(i + 1)%></td>
												<td><a
													href="projects.jsp?id=<%=data.get(i).getId()%>&name=<%out.print(data.get(i).getName());%>"><%=data.get(i).getName()%>
												</a></td>
												<td><%=data.get(i).getProjectManager()%></a></td>
												<td><%=data.get(i).getDeliveryManager()%></td>
												<td><%=data.get(i).getSolutionManager()%></td>
												<td><%=data.get(i).getBudget()%></td>
												<td><%=data.get(i).getStart()%></td>
												<td><%=data.get(i).getEnd()%></td>
											</tr>
											<%
												}
											%>
										</tbody>
									</table>
								</div>

							</div>
							<div class="col-md-8"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<%@include file="master/footer.jsp"%>

	<!-- Table script -->
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script src="../js/yandex.js"></script>
	<script type="text/javascript" src="../js/tablesort.js"></script>
	<script type="text/javascript">
		//This script loads all the necessary data from the tablesort.js file
		$(function() {
			$('table.table-sort').tablesort();
			hljs.initHighlightingOnLoad(); // Syntax Hilighting
		});
	</script>

</body>
<%
	}
%>
</html>
