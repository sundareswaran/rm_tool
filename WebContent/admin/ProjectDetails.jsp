<%@page import="java.util.Calendar"%>
<%@page import="com.cisco.rmtool.AdminDao"%>
<%@page import="com.cisco.rmtool.EmployeeDao"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@page import="java.util.List"%>
<%@ include file="master/datejava.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else 
	{
		List<ProjectData> adata = AdminDao.getAllProjects();
		String pfm_id = "";
		if (request.getParameter("pfm_id") == null) {
			pfm_id = adata.get(0).getId();
		}
		else
		{
			pfm_id = request.getParameter("pfm_id");
		}
		String status = "";
		if (request.getParameter("status") == null) {
			status = "APPROVED";
		}
		else
		{
			status = request.getParameter("status");
		}
		int unnot = AdminDao.getNumberofUnreadNotifications();
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"	href="../css/jquery.dataTables.min.css">
<style>
.table > tbody > tr > td {
     vertical-align: middle;
}
</style>
<title>Project Details</title>
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">

		<!-- Navbar left end, collapsable header Brand info-->
		<%@include file="master/brand.jsp"%>

		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
					<li><a href="dashboard.jsp">Dashboard</a></li>
					<li><a href="EmployeeDetails.jsp">Employee Details</a></li>
					<li  class="active"><a href="ProjectDetails.jsp">Project Details</a></li>
					<li><a href="addProject.jsp">Add Project</a></li>
					<li><a href="Reports.jsp">Reports</a></li>
					<li><a href="notifications.jsp">Notifications<% if(unnot != 0) {
						out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
					}%></a></li>
			</ul>
			<%@include file="master/navbar_right.jsp"%>

		</div>
	</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Project Details
						</h4>
					</div>
					<div class="panel-body">
					<div class="row">
							<div class="col-md-12">
							<form action="" method="get">
								<div class="alert alert-info" role="alert" style="padding-top: 5px; padding-bottom: 5px;">
									<table
										style="background: transparent; box-shadow: none; border: 0px; color: inherit;">
										<tr>
												<td>Project Name - </td>
												<td>
												<select id="project" class="form-control" style="width: auto" name="pfm_id"
														onchange="this.form.submit()">
														<% 
															for (int i = 0; i < adata.size(); i++) { 
															if(adata.get(i).getId().equalsIgnoreCase(pfm_id)) {
																out.print("<option selected value=" + adata.get(i).getId() + ">" + adata.get(i).getName() + "</option>");
															}
															else
															{
																out.print("<option value=" +  adata.get(i).getId() + ">" +  adata.get(i).getName() +  "</option>");																
															}
															}
														%>
												</select>
												</td>
											<td>&nbsp;Quarter - </td>
											<td><%@ include file="master/DateJsp.jsp"%></td>
									</tr>
									</table>
								</div>
								</form>
						<div class="row">
							<div class="col-md-12">
									<% ProjectData data = AdminDao.getProjectDetails(pfm_id); %>
									<table class="table table-striped">
										<tr>
											<th colspan="2">Project Information</th>
										</tr>
									   <tr>
									      <td class="col-md-3">Project Name</td>
									      <td><%out.print(data.getPrjName());%></td>
									   </tr>
									   <tr>
									      <td class="col-md-3">PFM ID</td>
									      <td><%out.print(data.getId());%></td>
									   </tr>
									   <tr>
									      <td class="col-md-3">Description</td>
									      <td><%out.print(data.getDescription());%></td>
									   </tr>
									   <tr>
									      <td class="col-md-3">Solution Manager</td>
									      <td><%out.print(data.getSolutionManager());%></td>
									   </tr>
									   <tr>
									      <td class="col-md-3">Delivery Manager</td>
									      <td><%out.print(data.getDeliveryManager());%></td>
									   </tr>
									   <tr>
									      <td class="col-md-3">Budget</td>
									      <td><%out.print("$" + data.getBudget());%></td>
									   </tr>
									   <tr>
									      <td class="col-md-3">Start Date</td>
									      <td><%out.print(data.getStart().split(" ")[0]);%></td>
									   </tr>
									   <tr>
									      <td class="col-md-3">End Date</td>
									      <td><%out.print(data.getEnd().split(" ")[0]);%></td>
									   </tr>
									</table>
									<% List<EmployeeData> empdata = AdminDao.getProjectEmployeeDetails(pfm_id, q, y, status); %>
									<form action="" method="get">
											<table>
											<tr>
											<td>Allocation Status - </td>
											<td>
											<select id="status" class="form-control" style="width: auto"
												name="status" onchange="this.form.submit()">
												<%
													if (status.equalsIgnoreCase("APPROVED"))
															out.print(
																	"<option selected>APPROVED</option><option>PENDING</option><option>DECLINED</option>");
														else if (request.getParameter("status").equalsIgnoreCase("PENDING"))
															out.print(
																	"<option>APPROVED</option><option selected>PENDING</option><option>DECLINED</option>");
														else
															out.print(
																	"<option>APPROVED</option><option>PENDING</option><option selected>DECLINED</option>");
												%>

											</select>
											</td>
											</tr>
											</table>
											
									</form>
									<br />
									<table id="myTable" class="display table"
									   style="border-radius: 2px;">
									   <thead>
									      <tr>
									         <th class="table-sort">Employee ID</th>
									         <th class="table-sort">Employee Name</th>
									         <th class="table-sort">Reporting Manager</th>
									         <th class="table-sort">M1</th>
									         <th class="table-sort">M2</th>
									         <th class="table-sort">M3</th>
									      </tr>
									   </thead>
									   <tbody>
									      <%
									         for (int i = 0; i < empdata.size(); i++) {
									         %>
									      <tr>
									         <td>
									            <%
									               out.print(empdata.get(i).getEmp_id());
									               %>
									         </td>
									         <td>
									            <%out.print(empdata.get(i).getCec_id());%>
									         </td>
									          <td><a href="EmployeeDetails.jsp?cec_id=<%out.print(empdata.get(i).getCec_id());%>%26<%out.print(empdata.get(i).getType());%>"
									            onclick="getEmployeeData(<%=empdata.get(i).getCec_id()%>,<%=empdata.get(i).getName()%>,<%=empdata.get(i).getReportingManager()%>);">
									            <%out.print(empdata.get(i).getName().split("\\(")[0]);%>
									            </a>
									         </td>
									         <td>
									            <%
									               out.print(empdata.get(i).getM1());
									               %>
									         </td>
									         <td>
									            <%
									               out.print(empdata.get(i).getM2());
									               %>
									         </td>
									         <td>
									            <%
									               out.print(empdata.get(i).getM3());
									               %>
									         </td>
									      </tr>
									      <%
									         }
									         %>
									   </tbody>
									</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>

	<!-- Footer -->
	<%@include file="master/footer.jsp"%>

	<!-- Table script -->
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/yandex.js"></script>
	<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#myTable').dataTable();
		});
	</script>
	<script>
		$(document).ready(function() {
			$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
</body>
</html>
<%
	}
%>