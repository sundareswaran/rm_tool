<%@page import="java.util.Calendar"%>
<%@page import="com.cisco.rmtool.EmployeeDao"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@page import="com.cisco.rmtool.ProjectManagerDao"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {

		Calendar c = Calendar.getInstance();
		int month = c.get(Calendar.MONTH);
		int y = c.get(Calendar.YEAR);
		int quarter = (int) Math.ceil((((month + 1) / 3)));
		String q = "Q" + quarter;
		if (request.getParameter("quarter") != null) {
			q = request.getParameter("quarter");
		}
		
		String id = request.getParameter("id");
		
		List<EmployeeData> data = ProjectManagerDao.getProjectResources(id, q, "" + y);%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link href="../css/tables.css" rel="stylesheet">


<link rel="stylesheet" type="text/css" href="../css/tablesort.css">
<link rel="stylesheet" type="text/css"
	href="../css/styles_tablesort.css">
<title>Employee Detail</title>
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">

		<!-- Navbar left end, collapsable header Brand info-->
		<%@include file="master/brand.jsp"%>

		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="dashboard.jsp">Dashboard</a></li>
				<li><a href="myProjects.jsp">My Projects</a></li>
				<li><a href="resourceAllocation.jsp">Resources Allocation</a></li>
				<li><a href="#">Notifications</a></li>
			</ul>

			<!-- Navbar right profile and logout -->
			<%@include file="master/navbar_right.jsp"%>

		</div>
	</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<!--/.sidebar-offcanvas-->
			<!--/row-->
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Employee Details -
							<%=EmployeeDao.getName(id)%>
						</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<table>
									<tr>
										<td>Name</td>
										<td><%=EmployeeDao.getName(id)%></td>
									</tr>
									<tr>
										<td>Address</td>
										<td><%=EmployeeDao.getAddress(id)%></td>
									</tr>
									<tr>
										<td>Region</td>
										<td><%=EmployeeDao.getEmployeeRegion(id)%></td>
									</tr>
									<tr>
										<td>Mail ID</td>
										<td><%=EmployeeDao.getMail(id)%></td>
									</tr>
									<tr>
										<td>Contact Number</td>
										<td><%=EmployeeDao.getPhoneNo(id)%></td>
									</tr>
									<tr>
										<td>Skills</td>
										<td><%=EmployeeDao.getSkills(id)%></td>
									</tr>
									<tr>
										<td>Reporting Manager</td>
										<td><%=EmployeeDao.getReportingManager(id)%></td>
									</tr>
								</table>
								<br /> <br /> <br />
								<form action="" method="get">
									<select id="quarter" name="quarter" class="form-control"
										onchange="this.form.submit()">
										<%
											for (int i = 1; i <= 4; i++) {
													if (q.equalsIgnoreCase("Q" + i))
														out.print("<option selected value=\"Q" + i + "\">Q" + i + "</option>");
													else
														out.print("<option value=\"Q" + i + "\">Q" + i + "</option>");
												}
										%>
									</select>
									<input type="hidden" name="id" value="<%=id%>">
								</form>
								<table>
									<tr>
										<td>Projects where <%=EmployeeDao.getName(id)%> is a
											Employee
										</td>
										<td>
											<%
												List<ProjectData> projects = EmployeeDao.getProjects(id, q, "" + y);
													for (int i = 0; i < projects.size(); i++) {
														out.println(projects.get(i).getName());
													}
											%>
										</td>
									</tr>
									<tr>
										<td>Project Manager(s)</td>
										<td>
											<%
												List<EmployeeData> employees = EmployeeDao.getProjectManagers(id, q, "" + y);
													for (int i = 0; i < employees.size(); i++)
														out.println(employees.get(i).getName());
											%>
										</td>
									</tr>
									<tr>
										<td>Projects where <%=EmployeeDao.getName(id)%> is a PM
										</td>
										<td>
											<%
												projects = EmployeeDao.getManagingProjects(id, q);
													if(projects.size() == 0)
													{
														out.println("Not a PM in this Quarter");
													}
													else
													{
														for (int i = 0; i < projects.size(); i++) {
															out.println(projects.get(i).getName());
														}
													}
											%>
										</td>
									</tr>
								</table>
								<table>
								<tr>
								<td>
								
								</td>
								</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<%@include file="master/footer.jsp"%>

	<!-- Table script -->
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script src="../js/yandex.js"></script>
	<script type="text/javascript" src="../js/tablesort.js"></script>
	<script type="text/javascript">
		//This script loads all the necessary data from the tablesort.js file
		$(function() {
			$('table.table-sort').tablesort();
			hljs.initHighlightingOnLoad(); // Syntax Hilighting
		});
	</script>
</body>
</html>
<%
	}
%>