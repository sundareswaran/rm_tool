<%@page import="com.cisco.rmtool.AdminDao"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.cisco.rmtool.AdminDao"%>
<%@page import="com.cisco.rmtool.EmployeeDao"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@page import="java.util.List"%>
<%@ include file="master/datejava.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	out.print(session.getAttribute("session"));
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		int unnot = AdminDao.getNumberofUnreadNotifications();
%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Administrator</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" href="../css/sticky-footer-navbar.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../css/buttons.dataTables.min.css">
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">

			<!-- Navbar left end, collapsable header Brand info-->
			<%@include file="master/brand.jsp"%>

			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li  class="active"><a href="dashboard.jsp">Dashboard</a></li>
					<li><a href="EmployeeDetails.jsp">Employee Details</a></li>
					<li><a href="ProjectDetails.jsp">Project Details</a></li>
					<li><a href="addProject.jsp">Add Project</a></li>
					<li><a href="Reports.jsp">Reports</a></li>
					<li><a href="notifications.jsp">Notifications<% if(unnot != 0) {
						out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
					}%></a></li>
				</ul>

				<!-- Navbar right profile and logout -->
				<%@include file="master/navbar_right.jsp"%>

			</div>
		</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<!--/.sidebar-offcanvas-->
			<!--/row-->
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Welcome, <span id="admin_name"><%out.print(name.split("\\(")[0]);%></span>
						</h4>
					</div>
					<div class="panel-body">
						<form action="" method="post">
							<div class="alert alert-info" role="alert" style="padding-top: 5px; padding-bottom: 5px;">
								<table style="background: transparent; box-shadow: none; border: 0px; color: inherit;">
									<tr>
										<td>Quarter - </td>
										<td><%@ include file="master/DateJsp.jsp"%></td>
									</tr>
								</table>
							</div>
						</form>
						<div class="row">
							<div class="col-md-12">
								<%List<ProjectData> data = AdminDao.getAllProjects(q, y);%>
								<h3>Project Details</h3>
								<div id="pm_as_pm">
									<table id="myTable" class="display table"
										style="border-radius: 5px;">
										<thead>
											<tr>
												<th class="table-sort">Project Name</th>
												<th class="table-sort">Project Manager</th>
												<th class="table-sort">Delivery Manager</th>
												<th class="table-sort">Solution Manager</th>
												<th class="table-sort">Status</th>
												<th class="table-sort" style="min-width: 200px;">Progress</th>
											</tr>
										</thead>
										<tbody>
											<%
												for (int i = 0; i < data.size(); i++) {
											%>
											<tr>
												<td>
														<a href="ProjectDetails.jsp?pfm_id=<%out.print(data.get(i).getId());%>&year=<%=year%>&status=APPROVED">
														<%
															out.print(data.get(i).getName());
														%>
														</a>
												</td>
												<td>
														<a href="EmployeeDetails.jsp?id=<%out.print(data.get(i).getIdProjectManager());%>">
														<%
															out.print(data.get(i).getProjectManager().split("\\(")[0]);
														%>
														</a>
												</td>
												<td><%=data.get(i).getDeliveryManager()%></td>
												<td><%=data.get(i).getSolutionManager()%></td>
												<td><%=data.get(i).getStatus()%></td>
												<td><div class="progress" style="margin-bottom: 5px;">
														<div class="progress-bar" role="progressbar"
															aria-valuenow="<%out.print(data.get(i).getProgress());%>"
															aria-valuemin="0" aria-valuemax="100"
															style="width:<%out.print(data.get(i).getProgress());%>%;"></div>
															<span style="visibility: hidden;"><%out.print(data.get(i).getProgress());%></span>
													</div>
													</td>
											</tr>
											<%
												}
											%>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<%@include file="master/footer.jsp"%>

	<!-- Table script -->
	<script type="text/javascript" src="../js/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="../js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="../js/jszip.min.js"></script>
	<script type="text/javascript" src="../js/pdfmake.min.js"></script>
	<script type="text/javascript" src="../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/yandex.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
	    $('#myTable').DataTable( {
	        dom: 'Bfrtip',
	        buttons: [
	            {
	                extend:    'copyHtml5',
	                text:      '<i class="fa fa-files-o"></i>',
	                titleAttr: 'Copy'
	            },
	            {
	                extend:    'excelHtml5',
	                text:      '<i class="fa fa-file-excel-o"></i>',
	                titleAttr: 'Excel'
	            },
	            {
	                extend:    'csvHtml5',
	                text:      '<i class="fa fa-file-text-o"></i>',
	                titleAttr: 'CSV'
	            },
	            {
	                extend:    'pdfHtml5',
	                text:      '<i class="fa fa-file-pdf-o"></i>',
	                titleAttr: 'PDF'
	            }
	        ]
	    } );
	} );
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('[data-toggle="tooltip"]').tooltip();
		});
	</script>

</body>
<%
	}
%>
</html>
