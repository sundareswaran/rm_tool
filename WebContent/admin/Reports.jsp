<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="org.omg.CORBA.PUBLIC_MEMBER"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="com.cisco.rmtool.AdminDao"%>
<%@page import="com.cisco.rmtool.ProjectManagerDao"%>
<%@page import="com.cisco.rmtool.ReportingManagerDao"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@ include file="master/datejava.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else 
	{
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		String pfm_id = request.getParameter("pfm_id");
		if (pfm_id == null) {
			pfm_id = "All";
		}
		List<ProjectData> totEmp = AdminDao.getTotalEmployees();
		List<ProjectData> empRegion = AdminDao.getAllEmpRegion();
		List<ProjectData> empRM = AdminDao.getRMsandEmployeesCount();
		List<ProjectData> project = AdminDao.ProjectWiseDetails(q,y);
		int unnot = AdminDao.getNumberofUnreadNotifications();
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"	href="../css/jquery.dataTables.min.css">
<style>
.table > tbody > tr > td {
     vertical-align: middle;
}
</style>
<title>Generate Reports</title>
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">

		<!-- Navbar left end, collapsable header Brand info-->
		<%@include file="master/brand.jsp"%>

		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
					<li><a href="dashboard.jsp">Dashboard</a></li>
					<li><a href="EmployeeDetails.jsp">Employee Details</a></li>
					<li><a href="ProjectDetails.jsp">Project Details</a></li>
					<li><a href="addProject.jsp">Add Project</a></li>
					<li class="active"><a href="Reports.jsp">Reports</a></li>
					<li><a href="notifications.jsp">Notifications<% if(unnot != 0) {
						out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
					}%></a></li>
			</ul>

			<!-- Navbar right profile and logout -->
			<%@include file="master/navbar_right.jsp"%>

		</div>
	</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Reports
						</h4>
					</div>
					<div class="panel-body">
					<div class="row">
							<div class="col-md-12">
							<ul class="nav nav-tabs">
				   
				    <li class="active"><a data-toggle="tab" href="#Project">Project</a></li>
				    <li><a data-toggle="tab" href="#Employee">Employee</a></li>
				  </ul>
				  
				  <div class="tab-content">
			    <div id="Project" class="tab-pane fade in active">
			    <br />
			      <form action="" method="get">
									<div class="alert alert-info" role="alert"
										style="padding-top: 5px; padding-bottom: 5px;">
										<table
											style="background: transparent; box-shadow: none; border: 0px; color: inherit;">
											<tr>
												<td>Quarter - </td>
												<td><%@ include file="master/DateJsp.jsp"%></td>
												<td>&nbsp;Project - </td>
													<td><select class="form-control"
														data-live-search="true" name="pfm_id"
														style="min-width: 150px;'" onchange="this.form.submit()">
															<option value="All">All</option>
															<%
																for (int i = 0; i < project.size(); i++) {
																		if (pfm_id.equalsIgnoreCase(project.get(i).getId())) {
																			out.println("<option value=\"" + project.get(i).getId() + "\" selected>"
																					+ project.get(i).getName() + "</option>");
																		} else {
																			out.println("<option value=\"" + project.get(i).getId() + "\">" + project.get(i).getName()
																					+ "</option>");
																		}
																	}
															%>
													</select></td>
											</tr>
										</table>
									</div>
								</form>
								<div id="myCarousel" class="carousel slide" data-ride="carousel">
											<ol class="carousel-indicators">
												<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
												<li data-target="#myCarousel" data-slide-to="1"></li>
												<li data-target="#myCarousel" data-slide-to="2"></li>
											</ol>
											<div class="carousel-inner" role="listbox">
												<div class="item active"><div class="col-md-2"></div>
												<div class="col-md-8">
														<div id="<%if(pfm_id.equalsIgnoreCase("All"))
																			    	 {
																			    	 	out.print("ProjectsEmployeeCount");
																			    	 }
																			    	 else
																			    	 {
																			    		 out.print("ProjectsEmployeeCountIndividual");
																			    	 }%>" align="center"
																					style="min-width: 400px; height: 400px; margin: 0 auto"></div>
												</div><div class="col-md-2"></div>
												</div>
												<div class="item">
														<div id="<%if(pfm_id.equalsIgnoreCase("All"))
																			    	 {
																			    	 	out.print("progress");
																			    	 }
																			    	 else
																			    	 {
																			    		 out.print("progressproj");
																			    	 }%>" align="center"
																					style="min-width: 600px; height: 400px; margin: 0 auto"></div>
												</div>
											</div>
											<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
											<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
											</a>
											<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
											<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
											</a>
										</div>
								</div>
				<div id="Employee" class="tab-pane fade">
			    	<br />
								
								<br />
								<div id="myCarousel1" class="carousel slide" data-ride="carousel">
											<ol class="carousel-indicators">
												<li data-target="#myCarousel1" data-slide-to="0" class="active"></li>
												<li data-target="#myCarousel1" data-slide-to="1"></li>
												<li data-target="#myCarousel1" data-slide-to="2"></li>
											</ol>
											<div class="carousel-inner" role="listbox">
												<div class="item active">
												<center>
													<div id="EmployeeDistribution" style="height: 400px"></div>
												</center>
												</div>
												<div class="item">
												<center>
												<div id="RegionDistribution" style="height: 400px"></div>
												</center>
												</div>
												<div class="item">
												<center>
												<div id="RMwiseDistribution" style="height: 400px"></div>
												</center>
												</div>
											</div>
											<a class="left carousel-control" href="#myCarousel1" role="button" data-slide="prev">
											<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
											</a>
											<a class="right carousel-control" href="#myCarousel1" role="button" data-slide="next">
											<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
											</a>
										</div>
			    </div>
			    </div>
							</div>
						</div>
			</div>
		</div>
	</div>
	</div>
	</div>

	<!-- Footer -->
	<%@include file="master/footer.jsp"%>
<script type="text/javascript" src="../js/jquery-1.12.0.min.js"></script>
	<script src="../js/highcharts.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/yandex.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/offline-exporting.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.4/js/bootstrap-select.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.4/js/i18n/defaults-*.min.js"></script>
	<script>
	$(function () {
	    $('#EmployeeDistribution').highcharts({
	        chart: {
	            type: 'pie',
	            options3d: {
	                enabled: true,
	                alpha: 45
	            }
	        },
	        title: {
	            text: 'Employee Distribution'
	        },
	        plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
	        series: [{
	            type: 'pie',
	            name: 'Total',
	            data: [ ['Full Time Employees', <% for(int i = 0; i<totEmp.size(); i++){
										            	out.print(totEmp.get(i).getNoOfEmployees());
										            }%>],
						['Temporary Employees', <% for(int i = 0; i<totEmp.size(); i++){
										            	out.print(totEmp.get(i).getTotalEmployees());
										            }%>]
	            ]
	        }]
	    });
	});
	</script>
	<script>
	$(function () {
	    $('#RegionDistribution').highcharts({
	        chart: {
	            type: 'pie',
	            options3d: {
	                enabled: true,
	                alpha: 45
	            }
	        },
	        title: {
	            text: 'Region Distribution'
	        },
	        plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
	        series: [{
	            type: 'pie',
	            name: 'Total',
	            data: [ <% for(int i = 0; i<empRegion.size(); i++){
				            	out.print("['" + empRegion.get(i).getRegion() + "'," + empRegion.get(i).getTotalEmployees() + "]");
				            	if(i < empRegion.size() - 1){
				            		out.print(",");
				            	}
				            }%>
	            ]
	        }]
	    });
	});
	</script>
	<script>
	$(function () {
	    $('#RMwiseDistribution').highcharts({
	        chart: {
	            type: 'pie',
	            options3d: {
	                enabled: true,
	                alpha: 45
	            }
	        },
	        title: {
	            text: 'Reporting Manager wise Distribution'
	        },
	        plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
	        series: [{
	            type: 'pie',
	            name: 'Total',
	            data: [ <% for(int i = 0; i<empRM.size(); i++){
				            	out.print("['" + empRM.get(i).getRMName().split("\\(")[0] + "'," + empRM.get(i).getNoOfEmployees() + "]");
				            	if(i < empRM.size() - 1){
				            		out.print(",");
				            	}
				            }%>
	            ]
	        }]
	    });
	});
	</script>
	<script>
	$(function () {
	    $('#ProjectsEmployeeCount').highcharts({
	    	colors: ['#696969','#7cb5ec'],
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Full Time and Temporary Employees'
	        },
	        credits: {
	            enabled: false
	        },
	        xAxis: {
	            categories: [<%for (int i = 0; i < project.size(); i++) {
					out.print("'" + project.get(i).getName() + "'");
					if (i < project.size() - 1) {
						out.print(",");
					}
				}%>]
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Number of Employees'
	            },
	            stackLabels: {
	                enabled: true,
	                style: {
	                    fontWeight: 'bold',
	                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
	                }
	            }
	        },
	        tooltip: {
	            headerFormat: '<b>{point.x}</b><br/>',
	            pointFormat: 'Total Employees: {point.stackTotal}<br/>{series.name}: {point.y}'
	        },
	        plotOptions: {
	            column: {
	                stacking: 'normal',
	                dataLabels: {
	                    enabled: true,
	                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
	                    style: {
	                        textShadow: '0 0 3px black'
	                    }
	                }
	            }
	        },
	        series: [{
	            name: 'Temporaray Employees',
	            data: [<%for (int i = 0; i < project.size(); i++) {
					out.print(project.get(i).getNoOfEmployees());
					if (i < project.size() - 1) {
						out.print(",");
					}
				}%>]
	        },{
	            name: 'Fulltime Employees',
	            data: [<%for (int i = 0; i < project.size(); i++) {
					out.print(project.get(i).getTotalEmployees());
					if (i < project.size() - 1) {
						out.print(",");
					}
				}%>]
	        }]
	    });
	});
</script>
<script>
$(function () {
    $('#ProjectsEmployeeCountIndividual').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'Fulltime and Temporary Employees'
        },
        subtitle: {
            text: ''
        },
        credits: {
            enabled: false
        },
        plotOptions: {
        	pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Total',
            data: [
                ['Temporary Employees', <%for (int i = 0; i < project.size(); i++) {
											if (pfm_id.equalsIgnoreCase(project.get(i).getId())) {
												out.print(project.get(i).getNoOfEmployees());
											}
										}%>],
                ['Fulltime Employees', <%for (int i = 0; i < project.size(); i++) {
											if (pfm_id.equalsIgnoreCase(project.get(i).getId())) {
												out.print(project.get(i).getTotalEmployees());
											}
										}%>]
            ]
        }]
    });
});
</script>
<script>
$(function () {
    $('#progress').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Project Progress'
        },
        xAxis: {
            categories: [<%for (int i = 0; i < project.size(); i++) {
					out.print("'" + project.get(i).getName() + "'");
					if (i < project.size() - 1) {
						out.print(",");
					}
				}%>]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Progress Percent'
            }
        },
        legend: {
            reversed: true
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: 'Progress',
            data: [<%int progress;
				for (int i = 0; i < project.size(); i++) {
					if (Integer.parseInt(project.get(i).getProgress()) > 100) {
						progress = 100;
					} else {
						progress = Integer.parseInt(project.get(i).getProgress());
					}
					out.print(progress);
					if (i < project.size() - 1) {
						out.print(",");
					}
				}%>]
        }]
    });
});
</script>
<script>
$(function () {
    $('#progressproj').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Project Progress'
        },
        xAxis: {
            categories: [<%for (int i = 0; i < project.size(); i++) {
					if (pfm_id.equalsIgnoreCase(project.get(i).getId())) {
						out.print("'" + project.get(i).getName() + "'");
					}
				}%>]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Progress Percent'
            }
        },
        legend: {
            reversed: true
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: 'Progress',
            data: [<%
				for (int i = 0; i < project.size(); i++) {
					if (Integer.parseInt(project.get(i).getProgress()) > 100) {
						progress = 100;
					} else {
						progress = Integer.parseInt(project.get(i).getProgress());
					}
					if (pfm_id.equalsIgnoreCase(project.get(i).getId())) {
						out.print(progress);
					}
				}%>]
        }]
    });
});
</script>
</body>
</html>
<%
	}
%>