function submitData(form, list) {
	var f = form;
	selectAllOptions(list);
	f.submit();
}
function adding(index, v, y) {
	s1 = document.getElementById("selected");
	s2 = document.getElementById("available");
	var newOption = document.createElement("option");
	newOption.value = v;
	newOption.innerHTML = y;
	s1.options.add(newOption);
	s2.remove(index);
}
function removing(index, v, y) {
	s2 = document.getElementById("available");
	s1 = document.getElementById("selected");
	var newOption = document.createElement("option");
	newOption.value = v;
	newOption.innerHTML = y;
	s2.options.add(newOption);
	var opt = s1.remove(index);
}
function selectAllOptions(selStr) {
	var selObj = selStr;
	for (var i = 0; i < selObj.options.length; i++) {
		selObj.options[i].selected = true;
	}
}

function removeSkill(index) {
	skills = document.getElementById("selectedSkill");
	skills.remove(index);
}

function addSkill() {
	text = document.getElementById("skillText");
	skills = document.getElementById("selectedSkill");
	var skill = text.value;
	var newOption = document.createElement("option");
	newOption.value = skill;
	newOption.innerHTML = skill;
	skills.options.add(newOption);
	text.value = "";
}