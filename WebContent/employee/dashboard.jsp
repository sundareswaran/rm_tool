<%@page import="com.cisco.rmtool.QandY"%>
<%@page import="com.cisco.rmtool.ProjectData"%>
<%@page import="com.cisco.rmtool.EmployeeDao"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.List"%>
<%@ include file="master/datejava.jsp"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		List<ProjectData> data = EmployeeDao.getProjectsAlloc(id, q, y, "p");
		int unnot = EmployeeDao.getNumberofUnreadNotifications(id);

%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Employee</title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/tablesort.css">
<link rel="stylesheet" type="text/css"
	href="../css/styles_tablesort.css">
<link href="http://www.marcorpsa.com/ee/css/custom.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="http://www.marcorpsa.com/ee/plugins/bootstrap-select/css/bootstrap-select.css">
<style type="text/css">
	.dropdown-menu {
	    -webkit-touch-callout: none;
	    -webkit-user-select: none;
	    -khtml-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	}
	 
	.dropdown-menu a {
	   overflow: hidden;
	   outline: none;
	}
	 
	.additem input
	{
	   border:0;
	   margin:-3px;
	   padding:3px;
	   outline: none;
	   color: #000;
	   width: 99%;
	}
	 
	.additem:hover input
	{
	   background-color: #f5f5f5;
	}
	 
	.additem .check-mark
	{
	   opacity: 0;
	   z-index: -1000;
	}
	 
	.addnewicon {
	   position: relative;
	   padding: 4px;
	   margin: -8px;
	   padding-right: 50px;
	   margin-right: -50px;
	   color: #aaa;
	}
	 
	.addnewicon:hover {
	   color: #222;
	}
</style>
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">

			<!-- Navbar left end, collapsable header Brand info-->
			<%@include file="master/brand.jsp"%>

			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="dashboard.jsp">Dashboard</a></li>
					<li><a href="myProjects.jsp">My Projects</a></li>
					<li><a href="updateSkills.jsp">Update Skills</a></li>
					<li><a href="notifications.jsp">Notifications<% if(unnot != 0) {
							out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
						}%></a></li>				
				</ul>

				<!-- Navbar right profile and logout -->
				<%@include file="master/navbar_right.jsp"%>

			</div>
		</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">

			<!--/.sidebar-offcanvas-->
			<!--/row-->
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Welcome, <span id="employee_name"><%out.print(name.split("\\(")[0]);%></span>
						</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
							<table>
										<tr>
											<td>Name</td>
											<td><%=EmployeeDao.getName(id).split("\\(")[0]%></td>
										</tr>
										<tr>
											<td>Address</td>
											<td><%=EmployeeDao.getAddress(id)%></td>
										</tr>
										<tr>
											<td>Region</td>
											<td><%=EmployeeDao.getEmployeeRegion(id)%></td>
										</tr>
										<tr>
											<td>Mail ID</td>
											<td><%=EmployeeDao.getMail(id)%></td>
										</tr>
										<tr>
											<td>Contact Number</td>
											<td><%=EmployeeDao.getPhoneNo(id)%></td>
										</tr>
										<tr>
											<td>Skills</td>
											<td><%=EmployeeDao.getSkills(id)%></td>
										</tr>
										<tr>
											<td>Reporting Manager</td>
											<td><%=EmployeeDao.getReportingManager(id)%></td>
										</tr>
									</table>
									<select class="selectpicker" id="select2">
							          <option value="java">Java</option>
							          <option value="c">C</option>
							          <option value="cpp">C++</option>
							        </select>    
							</div>
							<div class="col-md-6">
							<form>
								<table style="background: transparent; box-shadow: none; border: 0px; color: inherit;">
									<tr>
										<td width="75px">Quarter - </td>
										<td><%@ include file="master/DateJsp.jsp"%></td>
									</tr>
								</table>
							</form>
							<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<%@include file="master/footer.jsp"%>
<script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/highcharts.js" type="text/javascript"></script>
<script>
	$(function() {
		$('#container')
				.highcharts(
						{
							chart : {
								type : 'column'
							},
							title : {
								text : 'Monthly Allocation'
							},
							xAxis : {
								categories : [
													<%
														for(int i = 0; i < data.size(); i++) {
															out.print("'" + data.get(i).getName() + "'");
															if(i < data.size() - 1) {
																out.print(",");
															}
														}
													%>
											 ]
							},

							credits : {
								enabled : false
							},

							series : [
									{
										name : 'M1',
										data : [
													<%
														for(int i = 0; i < data.size(); i++) {
															out.print(data.get(i).getM1());
															if(i < data.size() - 1) {
																out.print(",");
															}
														}
													%>
												]
									},
									{
										name : 'M2',
										data : [
													<%
														for(int i = 0; i < data.size(); i++) {
															out.print(data.get(i).getM2());
															if(i < data.size() - 1) {
																out.print(",");
															}
														}
													%>
												]
									},
									{
										name : 'M3',
										data : [
													<%
														for(int i = 0; i < data.size(); i++) {
															out.print(data.get(i).getM3());
															if(i < data.size() - 1) {
																out.print(",");
															}
														}
													%>
												]
									} ]
						});
	});
</script>
<script>
$(function() {
  var content = "<input type=text onKeyDown='event.stopPropagation();' onKeyPress='addSelectInpKeyPress(this,event)' onClick='event.stopPropagation()' placeholder='Add item'> <span class='glyphicon glyphicon-plus addnewicon' onClick='addSelectItem(this,event,1);'></span>";
 
  var divider = $('<option/>')
          .addClass('divider')
          .data('divider', true);
          
 
  var addoption = $('<option/>')
          .addClass('additem')
          .data('content', content)
      
  $('.selectpicker')
          .append(divider)
          .append(addoption)
          .selectpicker();
 
});
 
function addSelectItem(t,ev)
{
   ev.stopPropagation();
 
   var txt=$(t).prev().val().replace(/[|]/g,"");
   if ($.trim(txt)=='') return;
   var p=$(t).closest('.bootstrap-select').prev();
   var o=$('option', p).eq(-2);
   o.before( $("<option>", { "selected": true, "text": txt}) );
   p.selectpicker('refresh');
}
 
function addSelectInpKeyPress(t,ev)
{
   ev.stopPropagation();
 
   // do not allow pipe character
   if (ev.which==124) ev.preventDefault();
 
   // enter character adds the option
   if (ev.which==13)
   {
      ev.preventDefault();
      addSelectItem($(t).next(),ev);
   }
}
</script>
</body>
</html>
<%
	}
%>