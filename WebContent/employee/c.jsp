<%@page import="com.cisco.rmtool.ProjectData"%>
<%@page import="com.cisco.rmtool.Employee"%>
<%@page import="com.cisco.rmtool.ReportingManager"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.cisco.rmtool.EmployeeData"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.cisco.rmtool.ProjectManager"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	Calendar c = Calendar.getInstance();
	int month = c.get(Calendar.MONTH);
	int quarter = ((int) ((month + 1) / 3));
	String q = "Q" + quarter;
	if (request.getParameter("quarter") != null) {
		q = request.getParameter("quarter");
	}
	q = "Q1";
%>

<html>
<head>
<title>Demo page for data</title>
</head>
<body>
	<p>
		Region for employee =
		<%=Employee.getEmployeeRegion("1")%></p>
	<p>
		Name of Employee =
		<%=Employee.getName("1")%></p>
	<p>
		Address of employee =
		<%=Employee.getAddress("1")%></p>
	<p>
		Reporting manager =
		<%=Employee.getReportingManager("1")%></p>
	<p>
		Mail id =
		<%=Employee.getMail("1")%></p>
	<p>
		Phone number =
		<%=Employee.getPhoneNo("1")%></p>
	<p>
		Project managers of quarter =
		<%
		List<EmployeeData> employees = Employee.getProjectManagers("1", q);
		for (int i = 0; i < employees.size(); i++)
			out.println(employees.get(i).getName());
	%>
	</p>
	<p>
		Projects in which employee works in =
		<%
		List<ProjectData> projects = Employee.getProjects("1", q);
		for (int i = 0; i < projects.size(); i++) {
			out.println(projects.get(i).getName());
		}
	%>
	</p>
	<p>
		Project ids in which employee works in =
		<%
		projects = Employee.getProjectIDs("1", q);
		for (int i = 0; i < projects.size(); i++) {
			out.println(projects.get(i).getId());
		}
	%>
	</p>
	<p>
		projects and number of employees working in it =
		<%
		projects = Employee.getProjectEmployeeCount("1", q);
		for (int i = 0; i < projects.size(); i++) {
			out.println(projects.get(i).getId() + " - " + projects.get(i).getName() + " - "
					+ projects.get(i).getNoOfEmployees() + "  ");
		}
	%>
	</p>

	<p>
		Get the list of projects in which an employee is a PM in =
		<%
		projects = Employee.getManagingProjects("1", q);
		for (int i = 0; i < projects.size(); i++) {
			out.println(projects.get(i).getName());
		}
	%>
	</p>
	<p>
		List of skills of employee =
		<%=Employee.getSkills("1")%>
	</p>
	<p>
		Get project allocation =
		<%
		employees = Employee.getProjectAllocation("1", q, "1");
		for (int i = 0; i < employees.size(); i++) {
			out.println(employees.get(i).getProject().getId() + " - " + employees.get(i).getProject().getName()
					+ " - Allocation " + employees.get(i).getM1() + " " + employees.get(i).getM2() + " "
					+ employees.get(i).getM3() + " ");
		}
	%>
	</p>

</body>
</html>