<%@page import="com.cisco.rmtool.RememberMe"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	RememberMe.clearCookie(request, response);
	if (session.getAttribute("session") == "TRUE") {
		session.setAttribute("session", "FALSE");
		session.invalidate();
		response.sendRedirect("../index.jsp");
	} else {
		response.sendRedirect("../index.jsp");
	}
%>