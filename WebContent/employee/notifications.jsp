<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.cisco.rmtool.EmployeeDao"%>
<%@page import="com.cisco.rmtool.Notification"%>
<%@page import="com.cisco.rmtool.NotificationData"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	if (session.getAttribute("session") == null) {
		response.sendRedirect("../index.jsp");
	} else {
		String id = session.getAttribute("id").toString();
		String name = session.getAttribute("name").toString();
		int unnot = EmployeeDao.getNumberofUnreadNotifications(id);
		List<NotificationData> unread = Notification.getEmployeeUnreadNotfications(id);
		List<NotificationData> read = Notification.getEmployeeReadNotfications(id);
%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Notifications - Admin</title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../css/sticky-footer-navbar.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="../css/jquery.dataTables.min.css">
<style>
#myModal .modal-dialog
{
  width: 70%;
}
</style>
</head>
<body style="min-height: auto;">
	<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">
		<%@include file="master/brand.jsp"%>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
					<li><a href="dashboard.jsp">Dashboard</a></li>
					<li><a href="myProjects.jsp">My Projects</a></li>
					<li><a href="updateSkills.jsp">Update Skills</a></li>
					<li class="active"><a href="notifications.jsp">Notifications<% if(unnot != 0) {
							out.print("&nbsp;<span class=\"label label-danger\">" + unnot + "</span>");
						}%></a></li>
			</ul>
			<%@include file="master/navbar_right.jsp"%>
		</div>
	</div>
	</nav>
	<div class="container-fluid" style="margin-top: 75px;">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary" style="min-height: 410px;">
					<div class="panel-heading">
						<h4 class="panel-title">
							Welcome, <span id="employee_name"><%out.print(name.split("\\(")[0]);%></span>
						</h4>
					</div>
					<div class="panel-body">
					<div class="row">
				<div class="col-md-12">
				<h4>Notifications</h4>
				  <ul class="nav nav-tabs">
				    <li class="active"><a data-toggle="tab" href="#notiunread">Unread</a></li>
				    <li><a data-toggle="tab" href="#notiread">Read</a></li>
				  </ul>
				  <div class="tab-content">
			    <div id="notiunread" class="tab-pane fade in active">
			    	<br />
			    	<table id="Unread" class="display table"
									style="border-radius: 2px;">
									<thead>
										<tr>
											<th>From</th>
											<th>Project</th>
											<th>Project Manager</th>
											<th>M1</th>
											<th>M2</th>
											<th>M3</th>
										</tr>
									</thead>
									<%
										for (int i = 0; i < unread.size(); i++) {
									%>
									<tr>
										<td>
											<%
												out.println(unread.get(i).getSenderName());
											%>
										</td>
										<td>
											<%
												out.println(unread.get(i).getProject().getPrjName());
											%>
										</td>
										<td>
											<%
												out.println(unread.get(i).getProject().getProjectManager());
											%>
										</td>
										<td>
											<%
												out.println(unread.get(i).getProject().getM1());
											%>
										</td>
										<td>
											<%
												out.println(unread.get(i).getProject().getM2());
											%>
										</td>
										<td>
											<%
												out.println(unread.get(i).getProject().getM3());
											%>
										</td>
									</tr>
									<%
										}
									%>
								</table>
			    </div>
			    <div id="notiread" class="tab-pane fade">
			    <br />
			     <table id="Read" class="display table"
									style="border-radius: 2px;">
									<thead>
										<tr>
											<th>From</th>
											<th>Project</th>
											<th>Project Manager</th>
											<th>M1</th>
											<th>M2</th>
											<th>M3</th>
										</tr>
									</thead>
									<%
										for (int i = 0; i < read.size(); i++) {
									%>
									<tr>
										<td>
											<%
												out.println(read.get(i).getSenderName());
											%>
										</td>
										<td>
											<%
												out.println(read.get(i).getProject().getPrjName());
											%>
										</td>
										<td>
											<%
												out.println(read.get(i).getProject().getProjectManager());
											%>
										</td>
										<td>
											<%
												out.println(read.get(i).getProject().getM1());
											%>
										</td>
										<td>
											<%
												out.println(read.get(i).getProject().getM2());
											%>
										</td>
										<td>
											<%
												out.println(read.get(i).getProject().getM3());
											%>
										</td>
									</tr>
									<%
										}
									%>
								</table>
				</div>
			    </div>
			  </div>
			</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Footer -->
	<%@include file="master/footer.jsp"%>
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/yandex.js"></script>
	<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#Unread').dataTable();
		});
	</script>
	<script>
		$(document).ready(function() {
			$('#Read').dataTable();
		});
	</script>
	<% Boolean r = EmployeeDao.updateNotificationStatus(id); %>
</body>
</html>
<%
	}
%>