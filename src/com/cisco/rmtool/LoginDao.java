package com.cisco.rmtool;

import java.sql.*;

public class LoginDao {

	public static EmployeeData validate(LoginBean bean) {

		EmployeeData data = new EmployeeData();
		data.setCec_id("0");
		data.setName("");
		try {
			Connection con = ConnectionProvider.getCon();

			// Todo change query so that emp_id is first and emp_name is second
			String sql = "with emp(cec) as (select cec_id from login where cec_id=? and password=?) select cec_id,display_name from employee,emp where cec_id=cec and admin1='YES'";
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, bean.getEmail());
			ps.setString(2, bean.getPassword());

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				data.setCec_id(rs.getString(1));
				data.setName(rs.getString(2));
				data.setDesignation("Admin");
			} else {
				sql = "with rep(cec) as (select cec_id from login where cec_id=? and password=?), rep1(cec) as (select reporting_manager from employee,rep where reporting_manager=cec) select unique(cec_id),display_name from employee,rep1 where cec_id=cec";
				ps = con.prepareStatement(sql);

				ps.setString(1, bean.getEmail());
				ps.setString(2, bean.getPassword());

				rs = ps.executeQuery();

				if (rs.next()) {
					data.setCec_id(rs.getString(1));
					data.setName(rs.getString(2));
					data.setDesignation("reporting manager");
				} else {
					sql = "with emp(cec) as (select cec_id from login where cec_id=? and password=?), prj(cec) as (select project_manager from emp,project1 where project_manager=cec) select unique(cec_id),display_name from employee,prj where cec_id=cec";
					ps = con.prepareStatement(sql);

					ps.setString(1, bean.getEmail());
					ps.setString(2, bean.getPassword());

					rs = ps.executeQuery();

					if (rs.next()) {
						data.setCec_id(rs.getString(1));
						data.setName(rs.getString(2));
						data.setDesignation("project manager");
					} else {
						sql = "with emp(cec) as (select cec_id from login where cec_id=? and password=?) select cec_id,display_name from employee,emp where cec=cec_id";
						ps = con.prepareStatement(sql);

						ps.setString(1, bean.getEmail());
						ps.setString(2, bean.getPassword());

						rs = ps.executeQuery();

						if (rs.next()) {
							data.setCec_id(rs.getString(1));
							data.setName(rs.getString(2));
							data.setDesignation("employee");
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;

	}
}