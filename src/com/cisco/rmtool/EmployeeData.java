package com.cisco.rmtool;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

public class EmployeeData {
	public static String APPROVED = "APPROVED";
	public static String DECLINED = "DECLINED";
	public static String PENDING = "PENDING";
	String name;
	String cec_id;
	String emp_id;
	String skills;
	String m1, m2, m3;
	String quarter;
	String reportingManager;
	String status;
	String designation;
	String region;
	String year;
	String comment;
	String TotalProjects;
	String group_id;
	ProjectData project;
	String date;
	String time;
	String QFY;
	String type;
	String location;
	String mail;
	String contact;
	List<ProjectData> projects;

	public static String PERMANENT = "PERMANENT";
	public static String TEMPORARY = "TEMPORARY";

	public String getTime() {
		return time;
	}

	public void setProjects(List<ProjectData> projects) {
		if (this.projects != null)
			this.projects.addAll(projects);
		else
			this.projects = projects;
	}

	public List<ProjectData> getProjects() {
		if (project != null)
			return projects;
		else {
			projects = new ArrayList<>();
			return projects;
		}
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public String getQFY() {
		return QFY;
	}

	public void setQFY(String qFY) {
		QFY = qFY;
	}

	public String getDate() {
		return date;
	}

	public String getGroup_id() {
		return group_id;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public void setCec_id(String cec_id) {
		this.cec_id = cec_id;
	}

	public void setEmp_id(String emp_id) {
		this.emp_id = emp_id;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	public String getSkills() {
		if (skills == null || skills.equalsIgnoreCase(""))
			return "-";
		return skills;
	}

	public String getComment() {
		if (comment == null || comment.equalsIgnoreCase(""))
			return "-";
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getYear() {
		if (year == null || year.equalsIgnoreCase(""))
			return "-";
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getRegion() {
		if (region == null || region.equalsIgnoreCase("") || region.equalsIgnoreCase("-"))
			return "-";
		if (region != null) {
			return region.split(":")[2] + ", " + region.split(":")[1] + " (" + region.split(":")[0] + ")";
		}
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public ProjectData getProject() {
		if (this.project == null)
			project = new ProjectData();
		return project;
	}

	public void setProject(ProjectData project) {
		this.project = project;
	}

	public String getDesignation() {
		if (designation == null || designation.equalsIgnoreCase(""))
			return "-";
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getStatus() {
		if (status == null || status.equalsIgnoreCase(""))
			return "-";
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReportingManager() {
		if (reportingManager == null || reportingManager.equalsIgnoreCase(""))
			return "Not Assigned";
		return reportingManager;
	}

	public void setReportingManager(String reportingManager) {
		this.reportingManager = reportingManager;
	}

	public String getM1() {
		if (m1 == null || m1.equalsIgnoreCase(""))
			return "0";
		return m1;
	}

	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	public String getQuarter() {
		if (quarter == null || quarter.equalsIgnoreCase(""))
			return "-";
		return quarter;
	}

	public String getM2() {
		if (m2 == null || m2.equalsIgnoreCase(""))
			return "0";
		return m2;
	}

	public String getM3() {
		if (m3 == null || m3.equalsIgnoreCase(""))
			return "0";
		return m3;
	}

	public void setM1(String m1) {
		this.m1 = m1;
	}

	public void setM2(String m2) {
		this.m2 = m2;
	}

	public void setM3(String m3) {
		this.m3 = m3;
	}

	public String getName() {
		if (name == null || name.equalsIgnoreCase(""))
			return "-";
		return name;
	}

	public String getCec_id() {
		if (cec_id == null || cec_id.equalsIgnoreCase(""))
			return "-";
		return cec_id;
	}

	public String getEmp_id() {
		if (emp_id == null || emp_id.equalsIgnoreCase(""))
			return "-";
		return emp_id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setTotalProjects(String TotalProjects) {
		this.TotalProjects = TotalProjects;
	}

	public String getTotalProjects() {
		if (TotalProjects == null || TotalProjects.equalsIgnoreCase(""))
			return "-";
		return TotalProjects;
	}

	public void setGroupId(String group_id) {
		this.group_id = group_id;
	}

	public String getGroupId() {
		return group_id;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getMail() {
		if (mail == null || mail.equalsIgnoreCase(""))
			return "-";
		return mail;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLocation() {
		if (location == null || location.equalsIgnoreCase(""))
			return "-";
		return location;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getContact() {
		if (contact == null || contact.equalsIgnoreCase(""))
			return "-";
		return contact;
	}

	public static String getEmployeeJSON(List<EmployeeData> data) {
		Gson gson = new Gson();
		return gson.toJson(data);
	}

	public static String getEmployeeJSON(EmployeeData data) {
		Gson gson = new Gson();
		return gson.toJson(data);
	}

}
