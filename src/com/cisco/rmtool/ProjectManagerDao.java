package com.cisco.rmtool;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProjectManagerDao {

	public static List<ProjectData> getProjects(String cec_id, String quarter, String year) {
		List<ProjectData> data = Collections.emptyList();
		try {
			Connection con = ConnectionProvider.getCon();
			String sql = "with pfms(pfm) as (select pfm_id from project1 where project_manager=?), projects(pfm) as (select unique(project_elapse.pfm_id) from project_elapse,pfms where pfms.pfm=project_elapse.pfm_id and months in (?,?,?)), NUMER(NUM_DT,PFM) AS (SELECT (trunc(sysdate)-to_date(to_char(start_date, 'DD-MM-YYYY'))),PFM_ID FROM PROJECT1,projects WHERE PFM_ID=PFM), DENOM(DEN_DT,PFM) AS (SELECT (to_date(to_char(end_date, 'DD-MM-YYYY'))-to_date(to_char(start_date,'DD-MM-YYYY'))), PFM_ID FROM PROJECT1,projects WHERE PFM_ID=PFM), RESULTS(ANS,PFM) AS (SELECT NUM_DT/DEN_DT,NUMER.PFM FROM NUMER,DENOM WHERE NUMER.PFM=DENOM.PFM), no_emps(pfm,cec) as (select pfm_id,count(unique(cec_id)) from resource_allocation,projects where pfm_id=pfm and status='APPROVED' and quarter=? and year1=? group by pfm_id), no_temp_emps(pfm,cec) as (select pfm_id,count(unique(temp_cec_id)) from temp_resource_allocation,projects where pfm_id=pfm and status='APPROVED' and quarter=? and year1=? group by pfm_id) select pfm_id,project_name,status,NVL(no_emps.cec,0)+NVL(no_temp_emps.cec,0) as no_emps,floor(ans*100) from project1 left outer join no_emps on pfm_id=no_emps.pfm left outer join no_temp_emps on pfm_id=no_temp_emps.pfm,results where pfm_id=results.pfm";
			PreparedStatement ps = con.prepareStatement(sql);
			QandY qY = new QandY(quarter + "FY" + (Integer.parseInt(year) % 100));
			ps.setString(1, cec_id);
			ps.setString(2, qY.getM1() + "-" + qY.getY1());
			ps.setString(3, qY.getM2() + "-" + qY.getY2());
			ps.setString(4, qY.getM3() + "-" + qY.getY3());
			ps.setString(5, quarter);
			ps.setString(6, year);
			ps.setString(7, quarter);
			ps.setString(8, year);
			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setId(rs.getString(1));
				project.setName(rs.getString(2));
				project.setStatus(rs.getString(3));
				project.setTotalEmployees(rs.getString(4));
				project.setProgress(rs.getString(5));
				data.add(project);
			}
			ConnectionProvider.close(con, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * data = new ArrayList<>(); for (int i = 0; i < 5; i++) { ProjectData
		 * project = new ProjectData(); project.setId("" + i); project.setName(
		 * "Project name" + (i + 1)); data.add(project); }
		 */
		return data;
	}

	public static List<EmployeeData> getProjectResources(String proj_id, String quarter, String year) { // Used
																										// in
																										// projectmanager/employeeDetail.jsp
		List<EmployeeData> data = Collections.emptyList();
		try {
			Connection con = ConnectionProvider.getCon();
			String sql = "with emps(cec,m1,m2,m3,status) as (select cec_id,m1,m2,m3,status from resource_allocation where pfm_id=? and year1=? and quarter=?) select emp_id,cec_id,display_name,m1,m2,m3,emps.status from employee,emps where cec=cec_id";
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, proj_id);
			ps.setString(2, year);
			ps.setString(3, quarter);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employeeData = new EmployeeData();
				employeeData.setEmp_id(rs.getString(1));
				employeeData.setCec_id(rs.getString(2));
				employeeData.setName(rs.getString(3));
				employeeData.setM1(rs.getString(3));
				employeeData.setM2(rs.getString(4));
				employeeData.setM3(rs.getString(5));
				employeeData.setStatus(rs.getString(6));
				data.add(employeeData);
			}
			ConnectionProvider.close(con, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * data = new ArrayList<>(); for (int i = 0; i < 5; i++) { EmployeeData
		 * employeeData = new EmployeeData(); employeeData.setId("" + (i + 1));
		 * employeeData.setName("employee " + (i + 1)); data.add(employeeData);
		 * }
		 */
		return data;
	}

	public static List<EmployeeData> getProjectEmpDetails(String proj_id, String quarter, String year, String status) { // Used
		// in
		// projectmanager/projects.jsp
		List<EmployeeData> data = Collections.emptyList();
		try {
			Connection con = ConnectionProvider.getCon();
			// Todo change query so that project_id is first and project name is
			// second
			String sql = "with emps(cec,m1,m2,m3) as (select unique(cec_id),sum(m1),sum(m2),sum(m3) from resource_allocation where pfm_id=? and year1=? and quarter=? and status=? group by cec_id), rm_cec(cec,rmcec) as (select cec_id,reporting_manager from employee,emps where cec_id=cec), rm_nm(cec,rmcec,rmnm) as (select cec,cec_id,display_name from employee,rm_cec where cec_id=rmcec order by cec) select emp_id,cec_id,display_name,m1,m2,m3,rm_nm.cec,rm_nm.rmnm from employee,emps,rm_nm where emps.cec=cec_id and rm_nm.cec=cec_id";
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, proj_id);
			ps.setString(2, year);
			ps.setString(3, quarter);
			ps.setString(4, status);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employeeData = new EmployeeData();
				employeeData.setEmp_id(rs.getString(1));
				employeeData.setCec_id(rs.getString(2));
				employeeData.setName(rs.getString(3));
				employeeData.setM1(rs.getString(4));
				employeeData.setM2(rs.getString(5));
				employeeData.setM3(rs.getString(6));
				employeeData.setReportingManager(rs.getString(8));
				employeeData.setType("p");
				data.add(employeeData);
			}
			ConnectionProvider.close(con, ps, rs);
			data.addAll(getTempResource(proj_id, year, quarter, status));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<EmployeeData> getTempResource(String proj_id, String year, String quarter, String status) {
		List<EmployeeData> data = Collections.emptyList();
		try {
			Connection con = ConnectionProvider.getCon();
			// Todo change query so that project_id is first and project name is
			// second
			String sql = "with emps(cec,m1,m2,m3) as (select unique(temp_cec_id),sum(m1),sum(m2),sum(m3) from temp_resource_allocation where pfm_id=? and year1=? and quarter=? and status=? group by temp_cec_id), rm_cec(cec,rmcec) as (select temp_cec_id,NVL(reporting_manager,'') from temp_employee,emps where temp_cec_id=cec) select temp_emp_id,temp_cec_id,display_name,m1,m2,m3,rm_cec.cec,rm_cec.rmcec from temp_employee,emps,rm_cec where emps.cec=temp_cec_id and rm_cec.cec=temp_cec_id";
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, proj_id);
			ps.setString(2, year);
			ps.setString(3, quarter);
			ps.setString(4, status);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employeeData = new EmployeeData();
				employeeData.setEmp_id(rs.getString(1));
				employeeData.setCec_id(rs.getString(2));
				employeeData.setName(rs.getString(3));
				employeeData.setM1(rs.getString(4));
				employeeData.setM2(rs.getString(5));
				employeeData.setM3(rs.getString(6));
				employeeData.setType("t");
				if (rs.getString(8) != null) {
					employeeData.setReportingManager(EmployeeDao.getName(rs.getString(8)));
				} else {
					employeeData.setReportingManager("Not Assigned");
				}
				data.add(employeeData);
			}
			ConnectionProvider.close(con, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static boolean enterAllocation(List<EmployeeData> data, String pfm_id, String cec_id) {
		// Used in projectmanager/notifyUpdate.jsp
		boolean status = true;
		String hashString = getHash(cec_id, pfm_id);
		for (int i = 0; i < data.size(); i++) {
			try {
				if (!(data.get(i).getM1().equalsIgnoreCase("0") && data.get(i).getM2().equalsIgnoreCase("0")
						&& data.get(i).getM3().equalsIgnoreCase("0"))) {
					Connection con = ConnectionProvider.getCon();
					if (!data.get(i).getCec_id().equalsIgnoreCase("")) {
						String sql = "insert into RESOURCE_ALLOCATION(cec_id,pfm_id,status,quarter,m1,m2,m3,year1,last_updated,comments,group_id) values (?,?,'PENDING',?,?,?,?,?,current_timestamp,'comments',?)";
						PreparedStatement ps = con.prepareStatement(sql);

						ps.setString(1, data.get(i).getCec_id());
						ps.setInt(2, Integer.parseInt(pfm_id));
						ps.setString(3, data.get(i).getQuarter());
						ps.setInt(4, Integer.parseInt(data.get(i).getM1()));
						ps.setInt(5, Integer.parseInt(data.get(i).getM2()));
						ps.setInt(6, Integer.parseInt(data.get(i).getM3()));
						ps.setString(7, data.get(i).getYear());
						ps.setString(8, hashString);
						ps.executeQuery();
						ConnectionProvider.close(ps);
					} else {
						String sql = "insert into RESOURCE_ALLOCATION (CEC_ID, PFM_ID, QUARTER, M1, M2, M3, STATUS)values(?,?,?,?,?,?,?)";
						PreparedStatement ps = con.prepareStatement(sql);

						ps.setString(1, data.get(i).getCec_id());
						ps.setInt(2, Integer.parseInt(pfm_id));
						ps.setString(3, data.get(i).getQuarter());
						ps.setInt(4, Integer.parseInt(data.get(i).getM1()));
						ps.setInt(5, Integer.parseInt(data.get(i).getM2()));
						ps.setInt(6, Integer.parseInt(data.get(i).getM3()));
						ps.setString(7, "PENDING");
						ConnectionProvider.close(ps);
					}
					ConnectionProvider.close(con);
				}
			} catch (Exception e) {
				e.printStackTrace();
				status = false;
			}
		}
		if (!(data.size() > 0))
			status = false;
		return status;
	}

	public static List<EmployeeData> cloneRequest(String from, String quarter, String year) { // Used
																								// in
																								// projectmanager/CloneAuthenticate.jsp
		List<EmployeeData> data = Collections.emptyList();

		try {
			Connection con = ConnectionProvider.getCon();
			String sql = "with cecs(cec) as (select cec_id from resource_allocation where pfm_id=? and quarter=? and year1=? and status='APPROVED') select cec_id,display_name,emp_id from employee,cecs where cec_id=cec";
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, from);
			ps.setString(2, quarter);
			ps.setString(3, year);
			data = new ArrayList<>();
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				EmployeeData employeeData = new EmployeeData();
				employeeData.setCec_id(rs.getString(1));
				employeeData.setName(rs.getString(2));
				employeeData.setEmp_id(rs.getString(3));
				data.add(employeeData);
			}
			ConnectionProvider.close(con, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;
	}

	public static List<String> getAllSkills() {
		List<String> list = Collections.emptyList();
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "Select distinct skill_name from skill";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst())
				list = new ArrayList<>();
			while (rs.next()) {
				String skill = rs.getString("skill_name");
				list.add(skill);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public static String getSkillsJson() { // Used in
											// projectmanager/SkillSelection.jsp
		return JsonProvider.getGson().toJson(getAllSkills());
	}

	public static List<EmployeeData> getEmployeeForSkill(String sk, String year, String quarter) {
		String[] skills = sk.split(",");
		List<EmployeeData> data = Collections.emptyList();
		String sql = "with sk(skid,snme) as (select skill_id,skill_name from skill where skill_name in (";
		String s = "";
		StringBuilder sbStr = new StringBuilder("");
		for (int i = 0; i < skills.length; i++) {
			if (i > 0)
				sbStr.append(",");
			sbStr.append("?");
		}
		s = sbStr.toString();
		sql = sql + s
				+ ")), cecs(cec,skid) as (select unique(cec_id),skill_id from emp_skill,sk where skill_id=skid), works(cec,m1,m2,m3) as (select cec_id,sum(m1),sum(m2),sum(m3) from resource_allocation,cecs where status='APPROVED' and cec_id=cec and year1=? and quarter=? group by cec_id), rm_cec(cec,rmcec) as (select cec_id,reporting_manager from employee,cecs where cec_id=cec), rm_nme(cec,rmcec,rmnme) as (select cec,cec_id,display_name from employee,rm_cec where cec_id=rmcec) select unique(cec_id),display_name,rmnme, emp_id,NVL(m1,0),NVL(m2,0),NVL(m3,0) from employee,rm_nme,cecs left outer join works on cecs.cec=works.cec where employee.cec_id=cecs.cec and employee.cec_id=rm_nme.cec";
		try {
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			int i;
			for (i = 0; i < skills.length; i++) {
				ps.setString((i + 1), skills[i]);
			}
			ps.setString(i + 1, year);
			ps.setString(i + 2, quarter);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employeeData = new EmployeeData();
				employeeData.setCec_id(rs.getString(1));
				employeeData.setName(rs.getString(2));
				employeeData.setReportingManager(rs.getString(3));
				employeeData.setEmp_id(rs.getString(4));
				employeeData.setM1(rs.getString(5));
				employeeData.setM2(rs.getString(6));
				employeeData.setM3(rs.getString(7));
				data.add(employeeData);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static ProjectData getProjectDetails(String id) {
		ProjectData data = new ProjectData();
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "select project_name, budget, target_release_quarter,start_date,end_date, delivary_manager, solution_manager, project_desc, pfm_id from project1 WHERE pfm_ID = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				data.setPrjName(rs.getString(1));
				data.setBudget(rs.getString(2));
				data.setTargetReleaseQuarter(rs.getString(3));
				data.setStart(rs.getString(4));
				data.setEnd(rs.getString(5));
				data.setDeliveryManager(rs.getString(6));
				data.setSolutionManager(rs.getString(7));
				data.setDeliveryManager(rs.getString(8));
				data.setId(rs.getString(9));
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<EmployeeData> getAllRegion(String pfm_id, String quarter, String year) {
		List<EmployeeData> data = Collections.emptyList();
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "WITH EMPS(CEC) AS (SELECT CEC_ID FROM RESOURCE_ALLOCATION WHERE PFM_ID=? AND QUARTER=? AND YEAR1=?), REG(CEC,REGION) AS (SELECT CEC_ID,REGION FROM EMPLOYEE_DETAILS,EMPS WHERE CEC_ID=CEC) SELECT UNIQUE(REGION) FROM REG";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, pfm_id);
			ps.setString(2, quarter);
			ps.setString(3, year);

			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				data = new ArrayList<>();
				while (rs.next()) {
					EmployeeData employee = new EmployeeData();
					employee.setRegion(rs.getString(1));
					data.add(employee);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<ProjectData> getRMsandEmployeesCount(String PFM_ID, String quarter, String year, String status) {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "with pfms(cec) as (select unique(cec_id) from resource_allocation where pfm_id=? and status=? and year1=? and quarter=?), temp_pfms(cec) as (select unique(temp_cec_id) from temp_resource_allocation where pfm_id=? and status=? and year1=? and quarter=?), rm(cec,counts) as (select reporting_manager, count(cec_id) from employee,pfms where cec_id=cec group by reporting_manager), temp_rm(cec,counts) as (select reporting_manager, count(temp_cec_id) from temp_employee,temp_pfms where temp_cec_id=cec group by reporting_manager) select cec_id,display_name,(NVL(rm.counts,0)+NVL(temp_rm.counts,0)) as count from rm full outer join temp_rm on temp_rm.cec=rm.cec,employee where cec_id in (rm.cec,temp_rm.cec)";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, PFM_ID);
			ps.setString(2, status);
			ps.setString(3, year);
			ps.setString(4, quarter);
			ps.setString(5, PFM_ID);
			ps.setString(6, status);
			ps.setString(7, year);
			ps.setString(8, quarter);

			ResultSet rs = ps.executeQuery();

			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setRMId(rs.getString(1));
				if (rs.getString(2) == null) {
					project.setRMName("Not assigned to RM");
				} else {
					project.setRMName(rs.getString(2));
				}
				project.setNoOfEmployees(rs.getString(3));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static List<ProjectData> getMyProjects(String cec_id, String quarter, String year) { // Used
																								// in
																								// projectmanager/myProjects.jsp
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "with prjts(pfm) as (select pfm_id from resource_allocation where cec_id=? and quarter=? and year1=? and status='APPROVED') select pfm_id,project_name,project_manager, solution_manager, delivary_manager,budget,start_date,end_date from project1,prjts where pfm_id=pfm";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, cec_id);
			ps.setString(2, quarter);
			ps.setInt(3, Integer.parseInt(year));
			ResultSet rs = ps.executeQuery();

			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setId(rs.getString(1));
				project.setName(rs.getString(2));
				project.setProjectManager(rs.getString(3));
				project.setSolutionManager(rs.getString(4));
				project.setDeliveryManager(rs.getString(5));
				project.setBudget(rs.getString(6));
				project.setStart(rs.getString(7).split(" ")[0]);
				project.setEnd(rs.getString(8).split(" ")[0]);
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static String getProjectName(String PFM_ID) { // Used in
															// projectmanager/CloneAuthenticate.jsp
		String sql = "select PROJECT_NAME from PROJECT1 where PFM_ID=?";
		String ProjectName = null;
		Connection conn = ConnectionProvider.getCon();

		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, PFM_ID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				ProjectName = rs.getString(1);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ProjectName;
	}

	public static int getNumberofUnreadNotifications(String CEC_ID) {
		String sql = "with projs(fr,pf,gp,tot) as (select from1,pfm_id,group_id,count(group_id) from notify where to1=? and seen_status='unseen' group by group_id,from1,pfm_id order by max(sent) desc), proj_nm(pf,pnm) as (select unique(pfm_id),project_name from project1,projs where pfm_id=pf), nme(cec,nm) as (select unique(cec_id),display_name from employee,projs where cec_id=fr), ans(nm,pnm,gp,tot) as (select unique(nm),pnm,gp,tot from projs,proj_nm,nme where projs.fr=nme.cec and projs.pf=proj_nm.pf) select count(*) from ans";
		int numberofnotification = 0;
		Connection conn = ConnectionProvider.getCon();

		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_ID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				numberofnotification = rs.getInt(1);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return numberofnotification;
	}

	public static List<EmployeeData> getReportingManagers() {
		List<EmployeeData> data = Collections.emptyList();
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "with cecs(cec) as (select unique(reporting_manager) from employee) select cec_id,display_name from employee,cecs where cec=cec_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				data = new ArrayList<>();
				while (rs.next()) {
					EmployeeData employee = new EmployeeData();
					employee.setCec_id(rs.getString(1));
					employee.setReportingManager(rs.getString(2));
					data.add(employee);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<EmployeeData> getTempResources() {
		List<EmployeeData> data = Collections.emptyList();
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "select temp_emp_id,temp_cec_id,display_name,reporting_manager,added_by from temp_employee where status='ACTIVE' order by temp_emp_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				data = new ArrayList<>();
				while (rs.next()) {
					EmployeeData employee = new EmployeeData();
					employee.setEmp_id(rs.getString(1));
					employee.setCec_id(rs.getString(2));
					employee.setName(rs.getString(3));
					employee.setReportingManager(rs.getString(4));
					employee.setComment(rs.getString(5));
					data.add(employee);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static boolean insertTemporaryResource(String... strings) {
		boolean result = false;
		try {
			String sql = "insert into TEMP_EMPLOYEE (temp_emp_id,temp_cec_id,first_name,last_name,display_name,mail,phone_no,reporting_manager,emp_type, region,location1,company,designation,added_by,last_updated,status) values (temp_emp_id.NEXTVAL,temp_cec_id.NEXTVAL,?,?,?,?,?,?,?,?,?,?,?,?,current_timestamp,'ACTIVE')";
			Connection conn = ConnectionProvider.getCon();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, strings[0]); // First name
			ps.setString(2, strings[1]); // Last name
			ps.setString(3, strings[0] + " " + strings[1]); // Display name
			ps.setString(4, strings[2]); // email
			ps.setString(5, strings[4]); // Phone
			ps.setString(6, strings[3]); // Reporting manager
			ps.setString(7, strings[8]); // employee type
			ps.setString(8, strings[5]); // Region
			ps.setString(9, strings[6]); // Location
			ps.setString(10, strings[7]); // Company
			ps.setString(11, strings[9]); // Designation
			ps.setString(12, strings[10]); // PM CEC_ID
			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				result = true;
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		}
		return result;
	}

	public static boolean enterTempAllocation(List<EmployeeData> data, String pfm_id, String cec_id) {
		// Used in projectmanager/notifyUpdate.jsp
		boolean status = true;
		String hashString = getHash(cec_id, pfm_id);
		for (int i = 0; i < data.size(); i++) {
			try {
				if (!(data.get(i).getM1().equalsIgnoreCase("0") && data.get(i).getM2().equalsIgnoreCase("0")
						&& data.get(i).getM3().equalsIgnoreCase("0"))) {
					Connection con = ConnectionProvider.getCon();
					// Todo change query so that project_id is first and project
					// name is second
					if (!data.get(i).getCec_id().equalsIgnoreCase("")) {
						String sql = "insert into temp_resource_allocation values (?,?,'PENDING',?,?,?,?,?,current_timestamp,'Comments',?)";
						PreparedStatement ps = con.prepareStatement(sql);

						ps.setString(1, data.get(i).getCec_id());
						ps.setInt(2, Integer.parseInt(pfm_id));
						ps.setString(3, data.get(i).getQuarter());
						ps.setInt(4, Integer.parseInt(data.get(i).getM1()));
						ps.setInt(5, Integer.parseInt(data.get(i).getM2()));
						ps.setInt(6, Integer.parseInt(data.get(i).getM3()));
						ps.setString(7, data.get(i).getYear());
						ps.setString(8, hashString);
						ps.executeQuery();
						ConnectionProvider.close(ps);
					} else {
						String sql = "insert into RESOURCE_ALLOCATION (CEC_ID, PFM_ID, QUARTER, M1, M2, M3, STATUS)values(?,?,?,?,?,?,?)";
						PreparedStatement ps = con.prepareStatement(sql);

						ps.setString(1, data.get(i).getCec_id());
						ps.setInt(2, Integer.parseInt(pfm_id));
						ps.setString(3, data.get(i).getQuarter());
						ps.setInt(4, Integer.parseInt(data.get(i).getM1()));
						ps.setInt(5, Integer.parseInt(data.get(i).getM2()));
						ps.setInt(6, Integer.parseInt(data.get(i).getM3()));
						ps.setString(7, "PENDING");
						ConnectionProvider.close(ps);
					}
					ConnectionProvider.close(con);
				}
			} catch (Exception e) {
				e.printStackTrace();
				status = false;
			}
		}
		if (!(data.size() > 0))
			status = false;
		return status;
	}

	public static List<EmployeeData> getTempEmployeeForIds(List<String> ids) { // Used
		// in
		// projectmanager/AuthenticateResource.jsp
		List<EmployeeData> data = Collections.emptyList();
		String sql = "select temp_emp_id,temp_cec_id,display_name,reporting_manager from temp_employee where temp_cec_id in (";
		String s = "";
		StringBuilder sbStr = new StringBuilder("");
		for (int i = 0; i < ids.size(); i++) {
			if (i > 0)
				sbStr.append(",");
			sbStr.append("?");
		}
		s = sbStr.toString();
		sql = sql + s + ")";
		try {
			Connection con = ConnectionProvider.getCon();
			// Todo change query so that project_id is first and project name is
			// second
			PreparedStatement ps = con.prepareStatement(sql);
			for (int i = 0; i < ids.size(); i++) {
				ps.setString((i + 1), ids.get(i));
			}
			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employeeData = new EmployeeData();
				employeeData.setEmp_id(rs.getString(1));
				employeeData.setCec_id(rs.getString(2));
				employeeData.setName(rs.getString(3));
				if (rs.getString(4) != null) {
					employeeData.setReportingManager(EmployeeDao.getName(rs.getString(4)));
				} else {
					employeeData.setReportingManager("Not Assigned");
				}
				data.add(employeeData);
			}
			ConnectionProvider.close(con, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<EmployeeData> getNotifyResReq(String cec_id, String rm_cec_id, String gid, String seen_status) {
		List<EmployeeData> data = Collections.emptyList();
		try {
			String sql = "with projs(noti,pfm,grp,sta,snt,dte,tme) as (select not_id,pfm_id,group_id,status,sent,(to_date(to_char(sent, 'DD-MM-YYYY'))),(EXTRACT(HOUR FROM sent) || ':' ||EXTRACT(MINUTE FROM sent)|| ':' ||EXTRACT(SECOND FROM sent)) from notify where to1=? and from1=? and group_id=? and seen_status=?), fins(cec,pfm,pnme,m1,m2,m3,quarter,year1,stat,dt,tm) as (select cec_id,project1.pfm_id,project_name,m1,m2,m3,quarter,year1,resource_allocation.status,dte,tme from resource_allocation,projs,project1 where resource_allocation.pfm_id=pfm and resource_allocation.status=sta and resource_allocation.last_updated=snt and group_id=grp and project1.pfm_id=pfm) select cec,display_name,pfm,pnme,m1,m2,m3,quarter,year1,stat,dt,tm from employee,fins where cec_id=cec order by tm desc, dt desc";
			Connection conn = ConnectionProvider.getCon();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, cec_id);
			ps.setString(2, rm_cec_id);
			ps.setString(3, gid);
			ps.setString(4, seen_status);
			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			if (rs.isBeforeFirst()) {
				data = new ArrayList<>();
				while (rs.next()) {
					EmployeeData employee = new EmployeeData();
					employee.setCec_id(rs.getString(3));
					employee.setName(rs.getString(4));
					employee.getProject().setId(rs.getString(1));
					employee.getProject().setPrjName(rs.getString(2));
					employee.setM1(rs.getString(5));
					employee.setM2(rs.getString(6));
					employee.setM3(rs.getString(7));
					employee.setQuarter(rs.getString(8));
					employee.setYear(rs.getString(9));
					employee.setStatus(rs.getString(10));
					employee.setDate(rs.getString(11));
					employee.setTime(rs.getString(12));
					QandY qY = new QandY(employee.getQuarter(), employee.getYear());
					employee.setQFY(qY.getQFY());
					employee.setType("p");
					data.add(employee);
				}
			}

			String sql1 = "with projs(noti,pfm,grp,sta,snt,dte,tme) as (select not_id,pfm_id,group_id,status,sent,(to_date(to_char(sent, 'DD-MM-YYYY'))),(EXTRACT(HOUR FROM sent) || ':' ||EXTRACT(MINUTE FROM sent)|| ':' ||EXTRACT(SECOND FROM sent)) from notify where to1=? and from1=? and group_id=? and seen_status=?), fins(cec,pfm,pnme,m1,m2,m3,quarter,year1,stat,dt,tm) as (select temp_cec_id,project1.pfm_id,project_name,m1,m2,m3,quarter,year1,temp_resource_allocation.status,dte,tme from temp_resource_allocation,projs,project1 where temp_resource_allocation.pfm_id=pfm and temp_resource_allocation.status=sta and temp_resource_allocation.last_updated=snt and group_id=grp and project1.pfm_id=pfm) select cec,display_name,pfm,pnme,m1,m2,m3,quarter,year1,stat,dt,tm from temp_employee,fins where temp_cec_id=cec order by dt desc, tm desc";
			PreparedStatement ps1 = conn.prepareStatement(sql1);
			ps1.setString(1, cec_id);
			ps1.setString(2, rm_cec_id);
			ps1.setString(3, gid);
			ps1.setString(4, seen_status);
			ResultSet rs1 = ps1.executeQuery();
			if (rs1.isBeforeFirst()) {
				data = new ArrayList<>();
				while (rs1.next()) {
					EmployeeData employee = new EmployeeData();
					employee.setCec_id(rs1.getString(3));
					employee.setName(rs1.getString(4));
					employee.getProject().setId(rs1.getString(1));
					employee.getProject().setPrjName(rs1.getString(2));
					employee.setM1(rs1.getString(5));
					employee.setM2(rs1.getString(6));
					employee.setM3(rs1.getString(7));
					employee.setQuarter(rs1.getString(8));
					employee.setYear(rs1.getString(9));
					employee.setStatus(rs1.getString(10));
					employee.setDate(rs1.getString(11));
					employee.setTime(rs1.getString(12));
					QandY qY = new QandY(employee.getQuarter(), employee.getYear());
					employee.setQFY(qY.getQFY());
					employee.setType("t");
					data.add(employee);
				}
			}
			ConnectionProvider.close(ps1, rs1);
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;

	}

	public static List<ProjectData> getNotifyResReqQandY(String cec_id, String rm_cec_id, String gid,
			String seen_status) {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "with projs(noti,pfm,grp,sta,snt) as (select not_id,pfm_id,group_id,status,sent from notify where to1=? and from1=? and group_id=? and seen_status=?) select unique(project1.pfm_id),project_name,quarter,year1 from resource_allocation,projs,project1 where resource_allocation.pfm_id=pfm and resource_allocation.status=sta and resource_allocation.last_updated=snt and group_id=grp and project1.pfm_id=pfm";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, cec_id);
			ps.setString(2, rm_cec_id);
			ps.setString(3, gid);
			ps.setString(4, seen_status);
			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			if (rs.isBeforeFirst()) {
				data = new ArrayList<>();
				while (rs.next()) {
					ProjectData project = new ProjectData();
					project.setId(rs.getString(1));
					project.setPrjName(rs.getString(2));
					project.setQuarter(rs.getString(3));
					project.setYear(rs.getString(4));
					QandY qY = new QandY(project.getQuarter(), project.getYear());
					project.setQFY(qY.getQFY());
					data.add(project);
				}
			}

			String sql1 = "with projs(noti,pfm,grp,sta,snt) as (select not_id,pfm_id,group_id,status,sent from notify where to1=? and from1=? and group_id=? and seen_status=?) select unique(project1.pfm_id),project_name,quarter,year1 from temp_resource_allocation,projs,project1 where temp_resource_allocation.pfm_id=pfm and temp_resource_allocation.status=sta and temp_resource_allocation.last_updated=snt and group_id=grp and project1.pfm_id=pfm";
			PreparedStatement ps1 = conn.prepareStatement(sql1);
			ps1.setString(1, cec_id);
			ps1.setString(2, rm_cec_id);
			ps1.setString(3, gid);
			ps1.setString(4, seen_status);
			ResultSet rs1 = ps1.executeQuery();
			if (rs.isBeforeFirst()) {
				data = new ArrayList<>();
				while (rs1.next()) {
					ProjectData project = new ProjectData();
					project.setId(rs1.getString(1));
					project.setPrjName(rs1.getString(2));
					project.setQuarter(rs1.getString(3));
					project.setYear(rs1.getString(4));
					QandY qY = new QandY(project.getQuarter(), project.getYear());
					project.setQFY(qY.getQFY());
					data.add(project);
				}
			}
			ConnectionProvider.close(ps1, rs1);
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;

	}

	public static boolean updateNotificationStatus(String to, String from, String group_id) {
		boolean result = false;
		try {
			Connection con = ConnectionProvider.getCon();
			String sql = "update notify set seen_status='seen' where to1=? and from1=? and group_id=? and seen_status='unseen'";
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, to);
			ps.setString(2, from);
			ps.setString(3, group_id);
			con.setAutoCommit(false);

			int rows = ps.executeUpdate();
			if (rows == 1) {
				con.commit();
				result = true;
			}
			ConnectionProvider.close(con, ps);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;

	}

	public static List<ProjectData> getTotFullEmp(String id, String quarter, String year) {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "with pfms(pfm,pnme) as (select unique(pfm_id),project_name from project1 where project_manager=? order by pfm_id) select pfm,pnme,count(unique(cec_id)) from resource_allocation,pfms where pfm_id=pfm and quarter=? and year1=? and status='APPROVED' group by pfm,pnme order by pfm";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ps.setString(2, quarter);
			ps.setString(3, year);
			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			if (rs.isBeforeFirst()) {
				data = new ArrayList<>();
				while (rs.next()) {
					ProjectData project = new ProjectData();
					project.setId(rs.getString(1));
					project.setName(rs.getString(2));
					project.setTotalEmployees(rs.getString(3));
					data.add(project);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;

	}

	public static List<ProjectData> getTotTempEmp(String id, String quarter, String year) {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "with pfms(pfm,pnme) as (select unique(pfm_id),project_name from project1 where project_manager=? order by pfm_id) select pfm,pnme,count(unique(temp_cec_id)) from temp_resource_allocation,pfms where pfm_id=pfm and quarter=? and year1=? and status='APPROVED' group by pfm,pnme order by pfm";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ps.setString(2, quarter);
			ps.setString(3, year);
			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			if (rs.isBeforeFirst()) {
				data = new ArrayList<>();
				while (rs.next()) {
					ProjectData project = new ProjectData();
					project.setId(rs.getString(1));
					project.setName(rs.getString(2));
					project.setTotalEmployees(rs.getString(3));
					data.add(project);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;

	}

	public static List<ProjectData> getRMsandEmployeesCountOverall(String id, String quarter, String year) {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "with pfms(pfm) as (select pfm_id from project1 where project_manager=?), perm_cecs(pcec) as (select unique(cec_id) from resource_allocation,pfms where status='APPROVED' and quarter=? and year1=? and pfm_id=pfm), temp_cecs(tcec) as (select unique(temp_cec_id) from temp_resource_allocation,pfms where status='APPROVED' and quarter=? and year1=? and pfm_id=pfm), perm_fins(preps,pcnt) as (select reporting_manager,count(pcec) from perm_cecs,employee where cec_id=pcec group by reporting_manager), temp_fins(treps,tcnt) as (select NVL(reporting_manager,'NOT ASSIGNED'),count(tcec) from temp_cecs,temp_employee where temp_cec_id=tcec group by reporting_manager) select NVL(cec_id,'NOT ASSIGNED'),NVL(display_name,'NOT ASSIGNED'),(NVL(pcnt,0)+NVL(tcnt,0)) as numbers from perm_fins full outer join temp_fins on preps=treps left outer join employee on cec_id in (treps,preps)";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ps.setString(2, quarter);
			ps.setString(3, year);
			ps.setString(4, quarter);
			ps.setString(5, year);

			ResultSet rs = ps.executeQuery();

			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setRMId(rs.getString(1));
				if (rs.getString(2) == null) {
					project.setRMName("Not assigned to RM");
				} else {
					project.setRMName(rs.getString(2));
				}
				project.setNoOfEmployees(rs.getString(3));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static List<EmployeeData> getEmployeeForIds(List<String> ids) { // Used
		// in
		// projectmanager/AuthenticateResource.jsp
		List<EmployeeData> data = Collections.emptyList();
		String sql = "Select  employee.emp_id, employee.cec_id, employee.display_name from employee where cec_id in (";
		String s = "";
		StringBuilder sbStr = new StringBuilder("");
		for (int i = 0; i < ids.size(); i++) {
			if (i > 0)
				sbStr.append(",");
			sbStr.append("?");
		}
		s = sbStr.toString();
		sql = sql + s + ")";
		try {
			Connection con = ConnectionProvider.getCon();
			// Todo change query so that project_id is first and project name is
			// second
			PreparedStatement ps = con.prepareStatement(sql);
			for (int i = 0; i < ids.size(); i++) {
				ps.setString((i + 1), ids.get(i));
			}
			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employeeData = new EmployeeData();
				employeeData.setEmp_id(rs.getString(1));
				employeeData.setCec_id(rs.getString(2));
				employeeData.setName(rs.getString(3));
				data.add(employeeData);
			}
			ConnectionProvider.close(con, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static String getHash(String cec_id, String pfm_id) {
		String string = cec_id + pfm_id + System.currentTimeMillis();
		byte[] thedigest = null;
		try {
			byte[] bytesOfMessage = string.getBytes("UTF-8");

			MessageDigest md = MessageDigest.getInstance("MD5");
			thedigest = md.digest(bytesOfMessage);
		} catch (Exception e) {
			e.printStackTrace();
		}
		BigInteger bigInt = new BigInteger(1, thedigest);
		String hashtext = bigInt.toString(16);
		return hashtext;
	}

	/*---------     As requested by UI developer   ----------*/

	public static String getRMsandEmployeesCountChart(String PFM_ID, String quarter, String year, String status) // Used
	// in
	// reportingmanager/Reports.jsp
	{
		List<ProjectData> rmdata = getRMsandEmployeesCount(PFM_ID, quarter, year, status);
		StringBuilder series = new StringBuilder("");
		series.append(",series: [{ name: 'Percent of Employees', colorByPoint: true, data: [");
		for (int i = 0; i < rmdata.size(); i++) {
			series.append("{name:'" + rmdata.get(i).getRMName().split("\\(")[0] + "'," + "y:"
					+ rmdata.get(i).getNoOfEmployees() + "}");
			if (i < rmdata.size() - 1)
				series.append(",");
		}
		series.append("]}]");

		return series.toString();
	}

	public static String getTotTempEmpChart(String id, String quarter, String year) { // Used
		// in
		// reportingmanager/Reports.jsp
		List<ProjectData> projects = ProjectManagerDao.getTotTempEmp(id, quarter, year);
		StringBuilder builder = new StringBuilder("");
		for (int i = 0; i < projects.size(); i++) {
			builder.append(projects.get(i).getTotalEmployees());
			if (i < projects.size() - 1)
				builder.append(",");
		}
		return builder.toString();
	}

	public static String getTotFullEmpChart(String id, String quarter, String year) { // Used
																						// in
																						// reportingmanager/Reports.jsp
		List<ProjectData> projects = ProjectManagerDao.getTotFullEmp(id, quarter, year);
		StringBuilder builder = new StringBuilder("");
		for (int i = 0; i < projects.size(); i++) {
			builder.append(projects.get(i).getTotalEmployees());
			if (i < projects.size() - 1)
				builder.append(",");
		}
		return builder.toString();
	}

}
