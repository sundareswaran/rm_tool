package com.cisco.rmtool;

public class NotificationData {
	String senderId;
	String recipientId;
	String senderName;
	String receiverName;
	ProjectData project;
	String message;
	String title;
	String dateAndTime;
	String totresreq;
	String date;
	String time;
	String gid;
	String count;
	EmployeeData employee;

	public void setProject(ProjectData project) {
		this.project = project;
	}
	
	public void setEmployee(EmployeeData employee) {
		this.employee = employee;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public void setDateAndTime(String dateAndTime) {
		this.dateAndTime = dateAndTime;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setRecipientId(String recipientId) {
		this.recipientId = recipientId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public String getSenderName() {
		return senderName;
	}

	public String getDateAndTime() {
		return dateAndTime;
	}

	public String getMessage() {
		return message;
	}

	public String getRecipientId() {
		return recipientId;
	}

	public String getSenderId() {
		return senderId;
	}

	public String getTitle() {
		return title;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	public String getDate() {
		return date;
	}
	
	public void setTime(String time) {
		this.time = time;
	}
	public String getTime() {
		return time;
	}
	
	public void setGroupId(String gid) {
		this.gid = gid;
	}
	public String getGroupId() {
		return gid;
	}
	
	public void setTotResReq(String totresreq) {
		this.totresreq = totresreq;
	}
	public String getTotResReq() {
		return totresreq;
	}
	
	public void setCount(String count) {
		this.count = count;
	}
	public String getCount() {
		return count;
	}

	public ProjectData getProject() {
		if(project == null)
			project = new ProjectData();
		return project;
	}
	
	public EmployeeData getEmployee() {
		if(employee == null)
			employee = new EmployeeData();
		return employee;
	}
}
