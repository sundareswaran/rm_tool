package com.cisco.rmtool;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class AddTempResource
 */
@WebServlet("/AddTempResource")
public class AddTempResource extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddTempResource() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("session") == null) {
			response.sendRedirect("../index.jsp");
		} else {
			String id = session.getAttribute("id").toString();
			String[] ids = request.getParameterValues("id[]");
			String[] m1 = request.getParameterValues("M1[]");
			String[] m2 = request.getParameterValues("M2[]");
			String[] m3 = request.getParameterValues("M3[]");
			QandY qY = new QandY(request.getParameter("quarter"));
			String quarter = qY.getQuarter();
			String year = qY.getYear();
			String pfm_id = request.getParameter("pfm_id");
			java.util.List<EmployeeData> data = new ArrayList<EmployeeData>();
			for (int i = 0; i < ids.length; i++) {
				EmployeeData emp = new EmployeeData();
				emp.setCec_id(ids[i]);
				emp.setM1(m1[i]);
				emp.setM2(m2[i]);
				emp.setM3(m3[i]);
				emp.setQuarter(quarter);
				emp.setYear(year);
				data.add(emp);
			}

			session.setAttribute("dataForUpload", data);
			// out.print(JsonProvider.getGson().toJson(data));
			// response.sendRedirect("c.jsp");
			boolean upload = ProjectManagerDao.enterTempAllocation(data, pfm_id, id);
			if (upload)
				response.sendRedirect("projectmanager/dashboard.jsp?alert=success");
			else
				response.sendRedirect("projectmanager/dashboard.jsp?alert=failed");

		}
	}
}
