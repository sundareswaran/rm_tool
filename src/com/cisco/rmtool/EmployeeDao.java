package com.cisco.rmtool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EmployeeDao {

	public static List<ProjectData> getMyProjects(String cec_id, String quarter, String year) {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "with prjts(pfm) as (select pfm_id from resource_allocation where cec_id=? and quarter=? and year1=? and status='APPROVED') select pfm_id,project_name,project_manager, solution_manager, delivary_manager,budget,start_date,end_date from project1,prjts where pfm_id=pfm";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, cec_id);
			ps.setString(2, quarter);
			ps.setInt(3, Integer.parseInt(year));
			ResultSet rs = ps.executeQuery();

			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setId(rs.getString(1));
				project.setName(rs.getString(2));
				project.setProjectManager(rs.getString(3));
				project.setSolutionManager(rs.getString(4));
				project.setDeliveryManager(rs.getString(5));
				project.setBudget(rs.getString(6));
				project.setStart(rs.getString(7).split(" ")[0]);
				project.setEnd(rs.getString(8).split(" ")[0]);
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static String getEmployeeRegion(String CEC_id) {
		String region = "";
		try {
			String sql = "SELECT REGION FROM EMPLOYEE_DETAILS WHERE CEC_ID=?";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_id);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				region = rs.getString(1);
			}
			String[] r = region.split(":");
			region = r[2] + ", " + r[1] + " (" + r[0] + ")";
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return region;
	}

	public static String getReportingManager(String CEC_id) {
		String reportingManager = "";
		try {
			String sql = "SELECT DISPLAY_NAME FROM EMPLOYEE WHERE CEC_ID= (SELECT REPORTING_MANAGER FROM EMPLOYEE WHERE CEC_ID=?)";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_id);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				reportingManager = rs.getString(1);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reportingManager;
	}

	public static String getReportingManagerId(String CEC_id) { // Used in
																// projectmanager/employee.jsp
																// |
																// projectmanager/employeeDetail.jsp
																// |
																// reportingmanager/emplyeeDetail.jsp
		String reportingManager = "";
		try {
			String sql = "SELECT CEC_ID FROM EMPLOYEE WHERE CEC_ID= (SELECT REPORTING_MANAGER FROM EMPLOYEE WHERE CEC_ID=?)";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_id);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				reportingManager = rs.getString(1);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reportingManager;
	}

	public static String getAddress(String CEC_id) { // Used in
														// projectmanager/employee.jsp
														// |
														// projectmanager/employeeDetail.jsp
														// |
														// reportingmanager/emplyeeDetail.jsp
		String result = "";
		try {
			String sql = "SELECT LOCATION1 FROM EMPLOYEE_DETAILS WHERE CEC_ID=?";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_id);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				result = rs.getString(1);
			}
			if (result == null)
				result = " - ";
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String getMail(String CEC_id) { // Used in
													// projectmanager/employee.jsp
													// |
													// projectmanager/employeeDetail.jsp
													// |
													// reportingmanager/emplyeeDetail.jsp
		String result = "";
		try {
			String sql = "SELECT MAIL FROM EMPLOYEE WHERE CEC_ID=?";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_id);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				result = rs.getString(1);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String getPhoneNo(String CEC_id) { // Used in
														// projectmanager/employee.jsp
														// |
														// projectmanager/employeeDetail.jsp
														// |
														// reportingmanager/emplyeeDetail.jsp
		String result = "";
		try {
			String sql = "SELECT PHONE_NO FROM EMPLOYEE_DETAILS WHERE CEC_ID=?";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_id);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				if (rs.getString(1) != null)
					result = rs.getString(1);
				else
					result = "Not Available";
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String getName(String CEC_id) {
		String result = "";
		try {
			String sql = "SELECT DISPLAY_NAME FROM EMPLOYEE WHERE CEC_ID=?";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_id);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				result = rs.getString(1);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static List<EmployeeData> getProjectManagers(String CEC_id, String quarter, String year) { // Not
																										// used
		List<EmployeeData> data = Collections.emptyList();
		try {
			String sql = "WITH EMP_PROJ(PRJ) AS (SELECT UNIQUE(PFM_ID) FROM RESOURCE_ALLOCATION WHERE "
					+ "CEC_ID=? AND STATUS='APPROVED' AND QUARTER=? AND YEAR1=?), PRJ_MNG(CEC) AS (SELECT CEC_ID FROM "
					+ "EMPLOYEE_PROJECT,EMP_PROJ WHERE PFM_ID=PRJ AND DESIGNATION=lower('PROJECT MANAGER')) "
					+ "SELECT UNIQUE(EMP_NAME) AS PROJECT_MANAGERS FROM EMPLOYEE,PRJ_MNG WHERE CEC=CEC_ID";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_id);
			ps.setString(2, quarter);
			ps.setString(3, year);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employee = new EmployeeData();
				employee.setName(rs.getString(1));
				data.add(employee);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<ProjectData> getProjects(String CEC_id, String quarter, String year) {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "with pfms(pfm) as (select pfm_id from resource_allocation where cec_id=? and status='APPROVED' and year1=? and quarter=?) select pfm_id,project_name from project1,pfms where pfm_id=pfm";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_id);
			ps.setString(2, year);
			ps.setString(3, quarter);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setId(rs.getString(1));
				project.setName(rs.getString(2));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<ProjectData> getProjectIDs(String CEC_id, String quarter, String year) {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "SELECT UNIQUE(PFM_ID) FROM RESOURCE_ALLOCATION WHERE CEC_ID=? AND STATUS='APPROVED' "
					+ "AND QUARTER=? AND YEAR1=?";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_id);
			ps.setString(2, quarter);
			ps.setString(3, year);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setId(rs.getString(1));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<ProjectData> getProjectEmployeeCount(String CEC_id, String quarter, String year) {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "WITH PRGTS(PFM) AS (SELECT UNIQUE(PFM_ID) FROM RESOURCE_ALLOCATION "
					+ "WHERE STATUS='APPROVED' AND CEC_ID=? AND QUARTER=? AND YEAR1=?), EMP(PF,COUNT_CEC) AS "
					+ "(SELECT PFM,COUNT(CEC_ID) FROM EMPLOYEE_PROJECT,PRGTS WHERE "
					+ "PRGTS.PFM=EMPLOYEE_PROJECT.PFM_ID GROUP BY PFM) SELECT PF,PROJECT_NAME,COUNT_CEC "
					+ "FROM EMP,PROJECT1 WHERE PROJECT1.PFM_ID = EMP.PF";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_id);
			ps.setString(2, quarter);
			ps.setString(3, year);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setId(rs.getString(1));
				project.setName(rs.getString(2));
				project.setNoOfEmployees(rs.getString(3));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<ProjectData> getManagingProjects(String CEC_id) { // Used
																			// in
																			// projectmanager/employee.jsp
																			// |
																			// projectmanager/employeeDetail.jsp
																			// |
																			// reportingmanager/emplyeeDetail.jsp
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "select pfm_id,project_name from project1 where project_manager=?";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_id);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setId(rs.getString(1));
				project.setName(rs.getString(2));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<EmployeeData> getProjectAllocation(String CEC_id, String quarter, String project_id,
			String year) {
		List<EmployeeData> data = Collections.emptyList();
		try {
			String sql = "WITH MNTHS(PFM,M1,M2,M3) AS (SELECT PFM_ID,M1,M2,M3 FROM RESOURCE_ALLOCATION "
					+ "WHERE CEC_ID=? AND QUARTER=? AND PFM_ID=? AND YEAR1=? AND STATUS='APPROVED') SELECT UNIQUE(PFM_ID),"
					+ "PROJECT_NAME,M1,M2,M3 FROM PROJECT1,MNTHS WHERE PFM=PFM_ID";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_id);
			ps.setString(2, quarter);
			ps.setString(3, project_id);
			ps.setString(4, year);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employee = new EmployeeData();
				ProjectData project = new ProjectData();
				project.setId(rs.getString(1));
				project.setName(rs.getString(2));
				employee.setProject(project);
				employee.setM1(rs.getString(3));
				employee.setM2(rs.getString(4));
				employee.setM3(rs.getString(5));
				data.add(employee);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<ProjectData> getProjectsAlloc(String CEC_id, String quarter, String year, String flag) {
		List<ProjectData> data = Collections.emptyList();
		try {

			String sql = "";
			if (flag.equalsIgnoreCase("p")) {
				sql = "with pfms(pfm,m1,m2,m3) as (select unique(pfm_id),sum(m1),sum(m2),sum(m3) from resource_allocation where cec_id=? and status='APPROVED' and year1=? and quarter=? group by pfm_id), projs(pfm,pnme,pcec) as (select pfm_id,project_name,project_manager from project1,pfms where pfm_id=pfm), pgmnme(pfm,pnme,pcec,pmnme) as (select pfm,pnme,cec_id,display_name from employee,projs where pcec=cec_id) select pfms.pfm,pnme,pgmnme.pcec,pmnme,m1,m2,m3 from pfms,pgmnme where pfms.pfm=pgmnme.pfm";
			} else {
				sql = "with pfms(pfm,m1,m2,m3) as (select unique(pfm_id),sum(m1),sum(m2),sum(m3) from temp_resource_allocation where temp_cec_id=? and status='APPROVED' and year1=? and quarter=? group by pfm_id), projs(pfm,pnme,pcec) as (select pfm_id,project_name,project_manager from project1,pfms where pfm_id=pfm), pgmnme(pfm,pnme,pcec,pmnme) as (select pfm,pnme,cec_id,display_name from employee,projs where pcec=cec_id) select pfms.pfm,pnme,pgmnme.pcec,pmnme,m1,m2,m3 from pfms,pgmnme where pfms.pfm=pgmnme.pfm";
			}
			Connection conn = ConnectionProvider.getCon();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_id);
			ps.setString(3, quarter);
			ps.setString(2, year);
			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setId(rs.getString(1));
				project.setName(rs.getString(2));
				project.setIdProjectManager(rs.getString(3));
				project.setProjectManager(rs.getString(4).split("\\(")[0]);
				project.setM1(rs.getString(5));
				project.setM2(rs.getString(6));
				project.setM3(rs.getString(7));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static String getEmployeeDesignation(String CEC_id) {
		String design = "";
		try {
			String sql = "select designation from employee where cec_id=?";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_id);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				design = rs.getString(1);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return design;
	}

	public static String getEmployeeId(String CEC_id) {
		String empid = "";
		try {
			String sql = "select emp_id from employee where cec_id=?";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_id);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				empid = rs.getString(1);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return empid;
	}

	public static List<EmployeeData> getAllDetail(String CEC_id, String flag) {
		List<EmployeeData> data = Collections.emptyList();
		try {
			String sql = "";
			if (flag.equalsIgnoreCase("p")) {
				sql = "with emp_dets(cec,con,reg,loc) as (select cec_id,phone_no,region,location1 from employee_details where cec_id=?) select cec_id,display_name,emp_id,designation,mail,con,reg,loc,reporting_manager from employee,emp_dets where cec_id=cec";
			} else {
				sql = "select temp_cec_id,display_name,temp_emp_id,designation,mail,phone_no,region,location1,reporting_manager from temp_employee where temp_cec_id=?";
			}
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_id);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employee = new EmployeeData();
				employee.setCec_id(rs.getString(1));
				employee.setName(rs.getString(2).split("\\(")[0]);
				employee.setEmp_id(rs.getString(3));
				employee.setDesignation(rs.getString(4));
				employee.setMail(rs.getString(5));
				employee.setContact(rs.getString(6));
				employee.setRegion(rs.getString(7));
				employee.setLocation(rs.getString(8));
				employee.setReportingManager(getName(rs.getString(9)).split("\\(")[0]);
				data.add(employee);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;
	}

	public static int getNumberofUnreadNotifications(String CEC_ID) {
		String sql = "select count(*) from notify where to1=? and seen_status='unseen'";
		int numberofnotification = 0;
		Connection conn = ConnectionProvider.getCon();
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_ID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				numberofnotification = rs.getInt(1);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return numberofnotification;
	}

	public static boolean updateNotificationStatus(String CEC_ID) {
		boolean result = false;
		try {
			Connection con = ConnectionProvider.getCon();
			String sql = "update notify set seen_status='seen' where to1=? and seen_status='unseen'";
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, CEC_ID);
			con.setAutoCommit(false);

			int rows = ps.executeUpdate();
			if (rows == 1) {
				con.commit();
				result = true;
			}
			ConnectionProvider.close(con, ps);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;

	}

	/*------------   In a format requested by UI developer  --------*/
	public static String getSkills(String CEC_id) { // Used in
		// projectmanager/employee.jsp
		// |
		// projectmanager/employeeDetail.jsp
		// |
		// reportingmanager/emplyeeDetail.jsp
		List<String> data = Collections.emptyList();
		try {
			String sql = "WITH SKILLID(SKID) AS (SELECT SKILL_ID FROM EMP_SKILL WHERE CEC_ID=?) SELECT UNIQUE(SKILL_ID),SKILL_NAME FROM SKILL,SKILLID WHERE SKID=SKILL_ID";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_id);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				data.add(rs.getString(2));
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// return JsonProvider.getGson().toJson(data);

		StringBuilder builder = new StringBuilder("");

		for (int i = 0; i < data.size(); i++) {
			builder.append(data.get(i));
			if (i < data.size() - 1)
				builder.append(",");
		}
		// Varun requested format
		return builder.toString();
	}

}
