package com.cisco.rmtool;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import com.sun.javafx.css.CalculatedValue;

import jdk.nashorn.internal.scripts.JS;
import oracle.net.aso.p;

@Path("api/v1/")
public class RestAPI {

	static protected Message insufficientData = new Message();

	static {
		insufficientData.setResult(Message.FAILURE);
		insufficientData.setTitle("Insufficient data");
		insufficientData.setMessage("The parameters are not passed");
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getName() {
		Message message = new Message();
		message.setMessage("For more information about our API V1 look into our documentation");
		message.setTitle("This is the base API page");
		message.getUrl().put("documentation", "http://localhost:8080/RM_Tool/Documentation.jsp");
		return JsonProvider.getGson().toJson(message);
	}

	@GET
	@Path("/projectmanager")
	@Produces(MediaType.TEXT_PLAIN)
	public String getWelcomeString() {
		Message message = new Message();
		message.setTitle("Project Manager API");
		message.setMessage(
				"This is the base link for Project Manager API, please look into the documentation for further reference.");
		message.getUrl().put("documentaion", "http://localhost:8080/RM_Tool/Documentation.jsp");
		message.getUrl().put("projectmanager",
				"Manages projects and allocates Resources. He is responsible for the projects");
		return JsonProvider.getGson().toJson(message);
	}

	// PROJECT MANAGER

	@GET
	@Path("/projectmanager/projects")
	@Produces(MediaType.TEXT_PLAIN)
	public String getProjects(@Context HttpHeaders hh, @DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(quarter)) {
			QandY qY = new QandY(checkQuarter(quarter));
			String id = getIdFromHeader(hh);
			if (id.equalsIgnoreCase("")) {
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
			} else {
				List<ProjectData> data = ProjectManagerDao.getProjects(id, qY.getQuarter(), qY.getYear());
				return JsonProvider.getGson().toJson(data);
			}
		} else
			return JsonProvider.getGson().toJson(insufficientData);

	}

	@GET
	@Path("/projectmanager/getProjectResource")
	@Produces(MediaType.TEXT_PLAIN)
	public String getProjectResources(@Context HttpHeaders hh, @DefaultValue("") @QueryParam("proj_id") String proj_id,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(proj_id, quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (id.equalsIgnoreCase("")) {
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
			} else {
				List<EmployeeData> data = ProjectManagerDao.getProjectResources(proj_id, qY.getQuarter(), qY.getYear());
				return JsonProvider.getGson().toJson(data);
			}
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/projectmanager/clonerequest")
	@Produces(MediaType.TEXT_PLAIN)
	public String cloneRequest(@Context HttpHeaders hh, @DefaultValue("") @QueryParam("from") String from,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(from, quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<EmployeeData> data = ProjectManagerDao.cloneRequest(from, qY.getQuarter(), qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);

	}

	@GET
	@Path("/projectmanager/getempforskill/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getEmployeeForSkill(@Context HttpHeaders hh, @DefaultValue("") @QueryParam("skill") String skill,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(skill, quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<EmployeeData> data = ProjectManagerDao.getEmployeeForSkill(skill, qY.getYear(), qY.getQuarter());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);

	}

	/*
	 * @GET
	 * 
	 * @Path("/projectmanager/getskilljson/") public String getSkillsJson(){
	 * List<EmployeeData> data = ProjectManagerDao.getSkillsJson(); return
	 * JsonProvider.getGson().toJson(data); }
	 */

	@GET
	@Path("/projectmanager/getprojdetails/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getProjectDetails(@Context HttpHeaders hh, @DefaultValue("") @QueryParam("pfm_id") String pfm_id) {
		if (validate(pfm_id)) {
			// QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				ProjectData data = ProjectManagerDao.getProjectDetails(pfm_id);
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/projectmanager/getRMsandEmployeesCount/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getRMsandEmployeesCount(@Context HttpHeaders hh,
			@DefaultValue("") @QueryParam("pfm_id") String pfm_id,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(pfm_id, quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<ProjectData> data = ProjectManagerDao.getRMsandEmployeesCount(pfm_id, qY.getQuarter(),
						qY.getYear(), EmployeeData.APPROVED);
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	/*
	 * This function has been removed need to check with Varun for more Details.
	 * 
	 * @GET
	 * 
	 * @Path(
	 * "/projectmanager/getregionandemp/{pfm_id}/{quarter}/{year}/{status}")
	 * public String getRegionAndEmployee(@PathParam("pfm_id") String
	 * pfm_id, @PathParam("quarter") String quarter, @PathParam("status") String
	 * status) { QandY qY = new QandY(quarter); List<ProjectData> data =
	 * ProjectManagerDao.getRegionAndEmployee(pfm_id, qY.getQuarter(),
	 * qY.getYear(), status); return JsonProvider.getGson().toJson(data); }
	 */

	// REPORTING MANAGER
	// TODO
	@GET
	@Path("/reportingmanager/repempdetails/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getReportingEmployeeDetails(@Context HttpHeaders hh,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<EmployeeData> data = ReportingManagerDao.getReportingEmployeeDetails(id, qY.getQuarter(),
						qY.getYear());
				for (int i = 0; i < data.size(); i++) {
					List<ProjectData> emp_proj = EmployeeDao.getProjectsAlloc(data.get(i).getCec_id(), qY.getQuarter(),
							qY.getYear());
					data.get(i).setProjects(emp_proj);
				}
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/allempalloc/")
	public String getAllEmployeeAllocation(@Context HttpHeaders hh,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<EmployeeData> data = ReportingManagerDao.getAllEmployeeAllocation(id, qY.getQuarter(),
						qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getAllEmployeesUnderRM(@Context HttpHeaders hh) {
		String id = getIdFromHeader(hh);
		if (!id.equalsIgnoreCase("")) {
			List<EmployeeData> data = ReportingManagerDao.getAllEmployeesUnderRM(id);
			return JsonProvider.getGson().toJson(data);
		} else
			return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
	}

	@GET
	@Path("/reportingmanager/getallcount/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getCountOfAllEmployees(@Context HttpHeaders hh) {
		String id = getIdFromHeader(hh);
		if (!id.equalsIgnoreCase("")) {
			int data = ReportingManagerDao.getCountOfAllEmployees(id);
			return JsonProvider.getGson().toJson(data);
		} else
			return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
	}

	@GET
	@Path("/reportingmanager/gettotalnoofprojects/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getTotalNoOfProjects(@Context HttpHeaders hh,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				int data = ReportingManagerDao.getTotalNoOfProjects(id, qY.getQuarter(), qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/getprojectresources/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getProjectResourcess(@Context HttpHeaders hh, @DefaultValue("") @QueryParam("proj_id") String proj_id,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(proj_id, quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<EmployeeData> data = ReportingManagerDao.getProjectResources(proj_id, qY.getQuarter(),
						qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	/*
	 * @GET
	 * 
	 * @Path("/reportingmanager/getprojdetails/{id}/") public class
	 * getProjectDetails(@PathParam("id") String id){ ProjectData data =
	 * ReportingManager.getProjectDetails(id); return
	 * JsonProvider.getGson().toJson(data); }
	 */

	@GET
	@Path("/reportingmanager/resourcerequest/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getResourceRequest(@Context HttpHeaders hh, @DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<EmployeeData> data = ReportingManagerDao.getResourceRequest(id, qY.getQuarter(), qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/getmyprojects/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getMyProjects(@Context HttpHeaders hh, @DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<ProjectData> data = ReportingManagerDao.getMyProjects(id, qY.getQuarter(), qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/getnoofemployees/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getNumberOfEmployees(@Context HttpHeaders hh, @DefaultValue("") @QueryParam("rm_id") String rm_id,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(rm_id, quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				int data = ReportingManagerDao.getNumberOfEmployees(rm_id, qY.getQuarter(), qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/getlistemp/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getListOfEmployees(@Context HttpHeaders hh, @DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<EmployeeData> data = ReportingManagerDao.getListOfEmployees(id, qY.getQuarter());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/getmyempproj/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getMyEmployeesProject(@Context HttpHeaders hh,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<ProjectData> data = ReportingManagerDao.getMyEmployeesProject(id, qY.getQuarter());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/getrepemp/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getReportingEmployee(@Context HttpHeaders hh,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<ProjectData> data = ReportingManagerDao.getMyEmployeesProject(id, qY.getQuarter());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/getprojallemp/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getProjectsOfAllEmployees(@Context HttpHeaders hh,
			@DefaultValue("") @QueryParam("rm_id") String rm_id,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(rm_id, quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<ProjectData> data = ReportingManagerDao.getProjectsOfAllEmployees(rm_id, qY.getQuarter(),
						qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/gettotempeachprojunderrm/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getTotalEmployeesInEachProjectUnderRM(@Context HttpHeaders hh,
			@DefaultValue("") @QueryParam("rm_id") String rm_id,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(rm_id, quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<ProjectData> data = ReportingManagerDao.getTotalEmployeesInEachProjectUnderRM(rm_id,
						qY.getQuarter(), qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/getallregionsemp/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getAllRegionsOfEmployees(@Context HttpHeaders hh, @DefaultValue("") @QueryParam("rm_id") String rm_id,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(rm_id, quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<EmployeeData> data = ReportingManagerDao.getAllRegionsOfEmployees(rm_id, qY.getQuarter(),
						qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/getallregions/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getAllRegions(@Context HttpHeaders hh, @DefaultValue("") @QueryParam("rm_id") String rm_id) {
		if (validate(rm_id)) {
			// QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<EmployeeData> data = ReportingManagerDao.getAllRegions(rm_id);
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/getnooregionsofallemp/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getNoOfRegionsOfAllEmployees(@Context HttpHeaders hh,
			@DefaultValue("") @QueryParam("rm_id") String rm_id) {
		if (validate(rm_id)) {
			// QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				int data = ReportingManagerDao.getNoOfRegionsOfAllEmployees(rm_id);
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/getunderallocated/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getUnderAllocated(@Context HttpHeaders hh, @DefaultValue("") @QueryParam("rm_id") String rm_id,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(rm_id, quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<EmployeeData> data = ReportingManagerDao.getUnderAllocated(rm_id, qY.getQuarter(), qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/getnoofunderallocemp/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getNoOfUnderAllocatedEmployees(@Context HttpHeaders hh,
			@DefaultValue("") @QueryParam("rm_id") String rm_id,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(rm_id, quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				int data = ReportingManagerDao.getNoOfUnderAllocatedEmployees(rm_id, qY.getQuarter());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/getoverallocated/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getOverAllocatedEmployees(@Context HttpHeaders hh,
			@DefaultValue("") @QueryParam("rm_id") String rm_id,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(rm_id, quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<EmployeeData> data = ReportingManagerDao.getOverAllocatedEmployees(rm_id, qY.getQuarter(),
						qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/getnoofoverallocemp/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getNoOfOverAllocatedEmployees(@Context HttpHeaders hh,
			@DefaultValue("") @QueryParam("rm_id") String rm_id,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(rm_id, quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				int data = ReportingManagerDao.getNoOfOverAllocatedEmployees(rm_id, qY.getQuarter(), qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/getregionandemp/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getRegionAndEmployees(@Context HttpHeaders hh, @DefaultValue("") @QueryParam("rm_id") String rm_id,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(rm_id, quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<ProjectData> data = ReportingManagerDao.getRegionAndEmployees(rm_id, qY.getQuarter(),
						qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/getallprojempcount/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getAllProjectEmployeeCount(@Context HttpHeaders hh,
			@DefaultValue("") @QueryParam("rm_id") String rm_id) {
		if (validate(rm_id)) {
			// QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<ProjectData> data = ReportingManagerDao.getAllProjectEmployeeCount(rm_id);
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/gettotempeachprojirrofrm/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getTotalEmployeesInEachProjectIrrespectiveOfRM(@Context HttpHeaders hh,
			@DefaultValue("") @QueryParam("rm_id") String rm_id,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(rm_id, quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<ProjectData> data = ReportingManagerDao.getTotalEmployeesInEachProjectIrrespectiveOfRM(rm_id,
						qY.getQuarter(), qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/getallregion/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getAllRegionRM(@Context HttpHeaders hh, @DefaultValue("") @QueryParam("pfm_id") String pfm_id,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(pfm_id, quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<EmployeeData> data = ReportingManagerDao.getAllRegion(pfm_id, qY.getQuarter(), qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/reportingmanager/getregionandemppfmid/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getRegionAndEmployeeRM(@Context HttpHeaders hh, @DefaultValue("") @QueryParam("pfm_id") String pfm_id,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(pfm_id, quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<ProjectData> data = ReportingManagerDao.getRegionAndEmployee(pfm_id, qY.getQuarter(),
						qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	// EMPLOYEE

	@GET
	@Path("/employee/getempregion/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getEmployeeRegion(@Context HttpHeaders hh) {
		String id = getIdFromHeader(hh);
		if (validate(id)) {
			if (!id.equalsIgnoreCase("")) {
				String data = EmployeeDao.getEmployeeRegion(id);
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/employee/getrepmanager/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getReportingManager(@Context HttpHeaders hh) {
		String id = getIdFromHeader(hh);
		if (validate(id)) {
			if (!id.equalsIgnoreCase("")) {
				String data = EmployeeDao.getReportingManager(id);
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/employee/getaddress/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getAddress(@Context HttpHeaders hh) {
		String id = getIdFromHeader(hh);
		if (validate(id)) {
			if (!id.equalsIgnoreCase("")) {
				String data = EmployeeDao.getAddress(id);
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/employee/getmail/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getMail(@Context HttpHeaders hh) {
		String id = getIdFromHeader(hh);
		if (validate(id)) {
			if (!id.equalsIgnoreCase("")) {
				String data = EmployeeDao.getMail(id);
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/employee/getphoneno/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getPhoneNo(@Context HttpHeaders hh) {
		String id = getIdFromHeader(hh);
		if (validate(id)) {
			if (!id.equalsIgnoreCase("")) {
				String data = EmployeeDao.getPhoneNo(id);
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/employee/getname/")
	public String getName(@Context HttpHeaders hh) {
		String id = getIdFromHeader(hh);
		if (validate(id)) {
			if (!id.equalsIgnoreCase("")) {
				String data = EmployeeDao.getName(id);
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/employee/getprojmanagers/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getProjectManagers(@Context HttpHeaders hh, @DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<EmployeeData> data = EmployeeDao.getProjectManagers(id, qY.getQuarter(), qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	/*
	 * @GET
	 * 
	 * @Path("/employee/getprojects/{CEC_id}/{quarter}/{year}") public String
	 * getProjects(@PathParam("CEC_id") String CEC_id, @PathParam("quarter")
	 * String quarter,
	 * 
	 * @PathParam("year") String year) { List<ProjectData> data =
	 * EmployeeDao.getProjects(CEC_id, quarter, year); return
	 * JsonProvider.getGson().toJson(data); }
	 * 
	 */

	@GET
	@Path("/employee/getprojectids/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getProjectIDs(@Context HttpHeaders hh, @DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<ProjectData> data = EmployeeDao.getProjectIDs(id, qY.getQuarter(), qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/employee/getprojemplcount/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getProjectEmployeeCount(@Context HttpHeaders hh,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<ProjectData> data = EmployeeDao.getProjectEmployeeCount(id, qY.getQuarter(), qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/employee/getmanagingprojs/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getManagingProjects(@Context HttpHeaders hh) {
		String id = getIdFromHeader(hh);
		if (validate(id)) {
			if (!id.equalsIgnoreCase("")) {
				List<ProjectData> data = EmployeeDao.getManagingProjects(id);
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/employee/getskills/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getSkills(@Context HttpHeaders hh) {
		String id = getIdFromHeader(hh);
		if (validate(id)) {
			if (!id.equalsIgnoreCase("")) {
				String data = EmployeeDao.getSkills(id);
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	@GET
	@Path("/employee/getprojalloc/{proj_id}/{quarter}")
	@Produces(MediaType.TEXT_PLAIN)
	public String getProjectAllocation(@Context HttpHeaders hh, @DefaultValue("") @QueryParam("proj_id") String proj_id,
			@DefaultValue("") @QueryParam("quarter") String quarter) {
		if (validate(proj_id, quarter)) {
			QandY qY = new QandY(quarter);
			String id = getIdFromHeader(hh);
			if (!id.equalsIgnoreCase("")) {
				List<EmployeeData> data = EmployeeDao.getProjectAllocation(id, qY.getQuarter(), proj_id, qY.getYear());
				return JsonProvider.getGson().toJson(data);
			} else
				return JsonProvider.getGson().toJson(RememberMe.NoUserFound());
		} else
			return JsonProvider.getGson().toJson(insufficientData);
	}

	/*
	 * @POST
	 * 
	 * @Path("/postData")
	 * 
	 * @Consumes(MediaType.APPLICATION_FORM_URLENCODED) public String
	 * getResponse(@FormParam("input")String input){ return "Hi there..! " +
	 * input; }
	 * 
	 * @GET
	 * 
	 * @Path("/data/{name}/{condition}") public String
	 * getDetails(@PathParam("name") String v1, @PathParam("condition") String
	 * v2){ return "Hi there..." + v1 + "<br>" + "How is your......" + v2 +
	 * "<a href=\"../kavin/heart\">Get heart</a>"; }
	 */
	@Path("/getCookie")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String get(@Context HttpHeaders hh) {
		Map<String, javax.ws.rs.core.Cookie> pathParams = hh.getCookies();
		return pathParams.get("HASH").getValue().toString();
	}

	// Functions required for easy developement
	protected String checkQuarter(String quarter) {
		if (quarter.equalsIgnoreCase("")) {
			Calendar c = Calendar.getInstance();
			return QandY.getCombi(c.get(Calendar.MONTH), c.get(Calendar.YEAR));
		} else {
			return quarter;
		}
	}

	protected String getHashFromHeader(HttpHeaders hh) {
		Map<String, javax.ws.rs.core.Cookie> cookies = hh.getCookies();
		if (cookies.get("HASH") != null)
			return cookies.get("HASH").getValue() + "";
		else
			return "";
	}

	protected boolean validate(String... data) {
		boolean result = true;
		for (String val : data) {
			result = !val.equalsIgnoreCase("") && result;
		}
		return result;
	}

	protected String getIdFromHeader(HttpHeaders hh) {
		String cookieHash = getHashFromHeader(hh);
		return RememberMe.getIdForCookie(cookieHash);
	}

}
