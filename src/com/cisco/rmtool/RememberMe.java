package com.cisco.rmtool;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RememberMe {

	private static String hash = "HASH";

	public static boolean createCookie(HttpServletResponse response, HttpServletRequest request, String cec_id) {
		// Create cookies for first and last names.
		String md5 = getHash(request);
		Cookie CookieHash = new Cookie(hash, md5);

		boolean insert = insertCookieToTable(cec_id, md5);
		if (insert) {
			// Set expairy time
			// CookieHash.setMaxAge(-1);

			// Add both the cookies in the response header.
			response.addCookie(CookieHash);
		} else {
			clearCookie(request, response);
		}
		return insert;
	}

	public static String getHash(HttpServletRequest request) {
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		String string = ipAddress + System.currentTimeMillis();
		byte[] thedigest = null;

		try {
			byte[] bytesOfMessage = string.getBytes("UTF-8");

			MessageDigest md = MessageDigest.getInstance("MD5");
			thedigest = md.digest(bytesOfMessage);
		} catch (Exception e) {
			e.printStackTrace();
		}
		BigInteger bigInt = new BigInteger(1, thedigest);
		String hashtext = bigInt.toString(16);
		return hashtext;
	}

	public static void clearCookie(HttpServletRequest request, HttpServletResponse response) {
		Cookie[] cookies = request.getCookies();
		if (cookies != null)
			for (int i = 0; i < cookies.length; i++) {
				cookies[i].setValue("");
				cookies[i].setPath("/");
				cookies[i].setMaxAge(0);
				response.addCookie(cookies[i]);
			}
	}

	public static String getIdForCookie(HttpServletRequest request) {
		String id = "";
		Cookie[] cookies = null;
		// Get an array of Cookies associated with this domain
		cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(hash)) {
					id = getIdFromTable(cookie.getValue());
				}
			}
		}
		return id;
	}

	public static String getIdForCookie(String hash) {
		return getIdFromTable(hash);
	}

	public static Message NoUserFound() {
		Message message = new Message();
		message.setTitle("No user found");
		message.setMessage("Please ckeck your cookie being sent or get a new cookie by loging in again.");
		message.setResult(Message.FAILURE);
		return message;
	}

	private static boolean insertCookieToTable(String id, String hash) {
		boolean result = false;
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "insert into cookies values(?,?,current_timestamp)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, hash);
			ps.setString(2, id);
			ps.executeQuery();
			conn.close();
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}

	private static String getIdFromTable(String md5) {
		String result = "";
		if(md5.equalsIgnoreCase(""))
			return "";
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "select cec_id from cookies where cook_id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, md5);
			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				while (rs.next())
					result = rs.getString(1);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
			result = "";
		}
		return result;
	}
}
