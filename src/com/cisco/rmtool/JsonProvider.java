package com.cisco.rmtool;

import com.google.gson.Gson;

public class JsonProvider {

	final static Gson gson = new Gson();
	
	public static Gson getGson(){
		return gson;
	}
	
}
