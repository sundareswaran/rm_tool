package com.cisco.rmtool;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class TempAllocation
 */
@WebServlet("/TempAllocation")
public class TempAllocation extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TempAllocation() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("session") == null) {
			response.sendRedirect("index.jsp");
		} else {
			String fName = request.getParameter("firstname");
			String lName = request.getParameter("lastname");
			String email = request.getParameter("email");
			String rm = request.getParameter("rm");
			String phone = request.getParameter("phone");
			String region = request.getParameter("region");
			String location = request.getParameter("location");
			String company = request.getParameter("company");
			String emptype = request.getParameter("emptype");
			String designation = request.getParameter("designation");
			String cec_id = session.getAttribute("id").toString();
			if (cec_id != null) {
				String[] arr = { fName, lName, email, rm, phone, region, location, company, emptype, designation,
						cec_id };
				boolean result = ProjectManagerDao.insertTemporaryResource(arr);
				if (result) {
					response.sendRedirect("projectmanager/addTemporaryResource.jsp?alert=success");
				} else {
					response.sendRedirect("projectmanager/addTemporaryResource.jsp?alert=failed");
				}
			} else {
				response.sendRedirect("index.jsp");
			}
		}
	}

}
