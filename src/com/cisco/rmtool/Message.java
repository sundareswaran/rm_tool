package com.cisco.rmtool;

import java.util.HashMap;
import java.util.Map;

public class Message {
	String result;
	String title;
	String message;
	Map<String, String> url;
	
	public static String FAILURE = "failure";
	public static String SUCCESS = "success";
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public String getTitle() {
		return title;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setUrl(Map<String, String> url) {
		this.url = url;
	}

	public Map<String, String> getUrl() {
		if (this.url == null)
			url = new HashMap<>();
		return url;
	}

}
