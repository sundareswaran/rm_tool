package com.cisco.rmtool;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sun.misc.PerformanceLogger;

/**
 * Servlet implementation class AddProject
 */
@WebServlet("/AddProject")
public class AddProject extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddProject() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("session") == null) {
			response.sendRedirect("index.jsp");
		} else {
			String id = session.getAttribute("id").toString();
			
			ProjectData project = new ProjectData();
			project.setId(request.getParameter("pfm_id"));
			project.setName(request.getParameter("project_name"));
			project.setDescription(request.getParameter("project_description"));
			project.setSolutionManager(request.getParameter("solution_manager"));
			project.setDeliveryManager(request.getParameter("delivery_manager"));
			project.setProjectManager(request.getParameter("project_manager"));
			project.setBudget(request.getParameter("budget"));
			project.setTargetReleaseQuarter(request.getParameter("target_release_quarter"));
			project.setStart(request.getParameter("start_date"));
			project.setEnd(request.getParameter("end_date"));
			project.setStatus(request.getParameter("status"));
			project.setFunding_source(request.getParameter("funding_source"));
			
			
			session.setAttribute("dataForUpload", project);
			// out.print(JsonProvider.getGson().toJson(data));
			// response.sendRedirect("c.jsp");
			boolean upload = AdminDao.addNewProject(project);
			if (upload)
				response.sendRedirect("admin/dashboard.jsp?alert=success");
			else
				response.sendRedirect("admin/dashboard.jsp?alert=failed");

		}
	}
	
}