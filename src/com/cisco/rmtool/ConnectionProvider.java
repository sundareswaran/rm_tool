package com.cisco.rmtool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;

public class ConnectionProvider implements Provider {

	protected static PoolDataSource poolDataSource = PoolDataSourceFactory.getPoolDataSource();

	static {
		try {
			poolDataSource.setConnectionFactoryClassName(Provider.PoolDataSourceClassName);
			poolDataSource.setURL(Provider.CONNECTION_URL);
			poolDataSource.setUser(Provider.USERNAME);
			poolDataSource.setPassword(Provider.PASSWORD);
			poolDataSource.setInitialPoolSize(5);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Connection getCon() {
		Connection con = null;
		try {
			// Class.forName(Provider.DRIVER);
			// con = DriverManager.getConnection(Provider.CONNECTION_URL,
			// Provider.USERNAME, Provider.PASSWORD);
			// con = DriverManager.getConnection(Provider.CONNECTION_STRING);
			
			// Connection from connection pool.
			con = poolDataSource.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;
	}

	public static void close(Connection con, PreparedStatement ps, ResultSet rs) {
		try {
			rs.close();
			ps.close();
			con.close();
			con = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void close(PreparedStatement ps, ResultSet rs) {
		try {
			rs.close();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void close(Connection con, PreparedStatement ps) {
		try {
			ps.close();
			con.close();
			con = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void close(PreparedStatement ps) {
		try {
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void close(Connection con) {
		try {
			con.close();
			con = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}