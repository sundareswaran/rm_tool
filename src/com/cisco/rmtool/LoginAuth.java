package com.cisco.rmtool;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LoginAuth
 */
@WebServlet("/LoginAuth")
public class LoginAuth extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginAuth() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LoginBean obj = new LoginBean();
		obj.setEmail(request.getParameter("email"));
		obj.setPassword(request.getParameter("password"));
		EmployeeData data = LoginDao.validate(obj);
		// String s = (String)request.getAttribute("email");

		HttpSession session = request.getSession();

		if ((!data.getCec_id().equalsIgnoreCase("0") && !data.getName().equalsIgnoreCase(""))) {
			session.setAttribute("session", "TRUE");
			session.setAttribute("name", data.getName());
			session.setAttribute("id", data.getCec_id());
			session.setAttribute("designation", data.getDesignation());

			RememberMe.createCookie(response, request, data.getCec_id());

			if (data.getDesignation().equalsIgnoreCase("project manager"))
				response.sendRedirect("projectmanager/dashboard.jsp");
			else if (data.getDesignation().equalsIgnoreCase("reporting manager"))
				response.sendRedirect("reportingmanager/dashboard.jsp");
			else if (data.getDesignation().equalsIgnoreCase("employee"))
				response.sendRedirect("employee/dashboard.jsp");
			else if (data.getDesignation().equalsIgnoreCase("Admin"))
				response.sendRedirect("admin/dashboard.jsp");
			else {
				RememberMe.clearCookie(request, response);
				response.sendRedirect("index.jsp");
			}
		} else {
			response.sendRedirect("index.jsp?alert=IncorrectCredentials");
		}
	}
}
