package com.cisco.rmtool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReportingManagerDao {

	public static List<EmployeeData> getReportingEmployeeDetails(String id, String quarter, String year) { // Used
																											// in
																											// reportingmanager/dashboard.jsp
		List<EmployeeData> data = Collections.emptyList();
		try {
			String sql = "with cecs(cec,emp,empnm) as (select unique(cec_id),emp_id,display_name from employee where reporting_manager=?), temp_cecs(tcec,temp,tempnm) as (select unique(temp_cec_id),temp_emp_id,display_name from temp_employee where reporting_manager=?), employees(cec,pfm,m1,m2,m3) as (select cec_id,count(distinct pfm_id),sum(m1),sum(m2),sum(m3) from resource_allocation,cecs where cec_id=cec and quarter=? and year1=? and status='APPROVED' group by cec_id), temp_employees(tcec,tpfm,m1,m2,m3) as (select temp_cec_id,count(distinct pfm_id),sum(m1),sum(m2),sum(m3) from temp_resource_allocation,temp_cecs where temp_cec_id=tcec and quarter=? and year1=? and status='APPROVED' group by temp_cec_id), perm_ans (pcec,pemp,pempnm,ppfm,pm1,pm2,pm3,perms) as (select cecs.cec,cecs.emp,cecs.empnm,pfm,m1,m2,m3,'p' from cecs left outer join employees on cecs.cec=employees.cec), temp_ans (tcec,temp,tempnm,tpfm,tm1,tm2,tm3,temps) as (select temp_cecs.tcec,temp_cecs.temp,temp_cecs.tempnm,tpfm,m1,m2,m3,'t' from temp_cecs left outer join temp_employees on temp_cecs.tcec=temp_employees.tcec) select NVL(pcec,tcec),NVL(pemp,temp),NVL(pempnm,tempnm),NVL(ppfm,tpfm),NVL(pm1,tm1),NVL(pm2,tm2),NVL(pm3,tm3),NVL(perms,temps) from perm_ans full outer join temp_ans on pempnm=tempnm order by pcec,tcec";
			Connection conn = ConnectionProvider.getCon();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ps.setString(2, id);
			ps.setString(3, quarter);
			ps.setString(4, year);
			ps.setString(5, quarter);
			ps.setString(6, year);
			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employee = new EmployeeData();
				employee.setCec_id(rs.getString(1));
				employee.setEmp_id(rs.getString(2));
				employee.setName(rs.getString(3));
				employee.setTotalProjects(rs.getString(4));
				employee.setM1(rs.getString(5));
				employee.setM2(rs.getString(6));
				employee.setM3(rs.getString(7));
				employee.setType(rs.getString(8));
				data.add(employee);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}

		for (int i = 0; i < data.size(); i++) {
			if (data.get(i).getProject().getIdProjectManager() != null) {
				try {
					String rmsql = "SELECT DISPLAY_NAME FROM EMPLOYEE WHERE CEC_ID=?";
					Connection conn = ConnectionProvider.getCon();
					PreparedStatement ps = conn.prepareStatement(rmsql);
					ps.setString(1, data.get(i).getProject().getIdProjectManager());
					ResultSet rs = ps.executeQuery();
					if (rs.next()) {
						data.get(i).getProject().setProjectManager(rs.getString(1));
					}
					ConnectionProvider.close(conn, ps, rs);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}

		return data;
	}

	public static List<EmployeeData> getAllEmployeeAllocation(String id, String quarter, String year) {
		List<EmployeeData> data = Collections.emptyList();
		try {
			String sql = "with cecs(cec,empnm) as (select unique(cec_id),display_name from employee where reporting_manager=?), temp_cecs(tcec,tempnm) as (select unique(temp_cec_id),display_name from temp_employee where reporting_manager=?), employees(cec,m1,m2,m3) as (select cec_id,sum(m1),sum(m2),sum(m3) from resource_allocation,cecs where cec_id=cec and quarter=? and year1=? and status='APPROVED' group by cec_id) , temp_employees(tcec,m1,m2,m3) as (select temp_cec_id,sum(m1),sum(m2),sum(m3) from temp_resource_allocation,temp_cecs where temp_cec_id=tcec and quarter=? and year1=? and status='APPROVED' group by temp_cec_id), perm_ans (pcec,pempnm,pm1,pm2,pm3) as (select cecs.cec,cecs.empnm,NVL(m1,0),NVL(m2,0),NVL(m3,0) from cecs left outer join employees on cecs.cec=employees.cec), temp_ans (tcec,tempnm,tm1,tm2,tm3) as (select temp_cecs.tcec,temp_cecs.tempnm,NVL(m1,0),NVL(m2,0),NVL(m3,0) from temp_cecs left outer join temp_employees on temp_cecs.tcec=temp_employees.tcec) select NVL(pcec,tcec),NVL(pempnm,tempnm),NVL(pm1,tm1),NVL(pm2,tm2),NVL(pm3,tm3) from perm_ans full outer join temp_ans on pempnm=tempnm order by tcec,pcec";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ps.setString(2, id);
			ps.setString(3, quarter);
			ps.setString(4, year);
			ps.setString(5, quarter);
			ps.setString(6, year);
			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employee = new EmployeeData();
				employee.setCec_id(rs.getString(1));
				employee.setName(rs.getString(2));
				employee.setM1(rs.getString(3));
				employee.setM2(rs.getString(4));
				employee.setM3(rs.getString(5));
				data.add(employee);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static List<EmployeeData> getAllEmployeesUnderRM(String id) {
		List<EmployeeData> data = Collections.emptyList();
		try {
			String sql = "SELECT UNIQUE(EMPLOYEE.CEC_ID),EMP_NAME FROM EMPLOYEE WHERE REPORTING_MANAGER = (select emp_name from employee where CEC_ID=?)";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employee = new EmployeeData();
				employee.setCec_id(rs.getString(1));
				employee.setName(rs.getString(2));
				data.add(employee);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static int getCountOfAllEmployees(String id) {
		int number = 0;
		try {
			String sql = "WITH PROJ_EMP(CEC,EMPNM) AS (SELECT UNIQUE(EMPLOYEE.CEC_ID),EMP_NAME FROM EMPLOYEE WHERE REPORTING_MANAGER = (select emp_name from employee where CEC_ID=?)) SELECT COUNT(CEC) FROM PROJ_EMP";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				number = rs.getInt(1);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return number;
	}

	public static int getTotalNoOfProjects(String id, String quarter, String year) {
		int number = 0;
		try {
			String sql = "WITH EMPS(CEC,EMPNM) AS (SELECT UNIQUE(EMPLOYEE.CEC_ID),EMP_NAME FROM EMPLOYEE WHERE REPORTING_MANAGER = (select emp_name from employee where CEC_ID=?)), PRGTS(PFM) AS (SELECT UNIQUE(PFM_ID) FROM RESOURCE_ALLOCATION,EMPS WHERE CEC_ID = CEC AND STATUS='APPROVED' AND QUARTER=? AND YEAR1=?) SELECT COUNT(PFM) FROM PRGTS";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ps.setString(2, quarter);
			ps.setString(3, year);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				number = rs.getInt(1);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return number;
	}

	public static List<EmployeeData> getProjectResources(String proj_id, String quarter, String year) { // Used
																										// in
																										// reportingmanager/projects.jsp
		List<EmployeeData> data = Collections.emptyList();
		try {
			Connection con = ConnectionProvider.getCon();
			String sql = "with emps(cec,m1,m2,m3,status) as (select cec_id,m1,m2,m3,status from resource_allocation where pfm_id=? and year1=? and quarter=? and status='APPROVED'), rm_cec(cec,rmcec) as (select cec_id,reporting_manager from employee,emps where cec_id=cec), rm_nm(cec,rmcec,rmnm) as (select cec,cec_id,display_name from employee,rm_cec where cec_id=rmcec order by cec) select unique(emp_id),cec_id,display_name,m1,m2,m3,emps.status,rm_nm.cec,rm_nm.rmnm from employee,emps,rm_nm where emps.cec=cec_id and rm_nm.cec=cec_id";
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, proj_id);
			ps.setString(2, year);
			ps.setString(3, quarter);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employeeData = new EmployeeData();
				employeeData.setEmp_id(rs.getString(1));
				employeeData.setCec_id(rs.getString(2));
				employeeData.setName(rs.getString(3));
				employeeData.setM1(rs.getString(4));
				employeeData.setM2(rs.getString(5));
				employeeData.setM3(rs.getString(6));
				employeeData.setStatus(rs.getString(7));
				employeeData.setReportingManager(rs.getString(9));
				data.add(employeeData);
			}
			ConnectionProvider.close(con, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * data = new ArrayList<>(); for (int i = 0; i < 5; i++) { EmployeeData
		 * employeeData = new EmployeeData(); employeeData.setId("" + (i + 1));
		 * employeeData.setName("employee " + (i + 1)); data.add(employeeData);
		 * }
		 */
		return data;
	}

	public static ProjectData getProjectDetails(String id) { // Used in
																// reportingmanager/emp_project.jsp
																// |
																// reportingmanager/projects.jsp
		ProjectData data = new ProjectData();
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "select project_name, budget, target_release_quarter,start_date,end_date, delivary_manager, solution_manager, project_desc, pfm_id from project1 WHERE pfm_ID = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				data.setPrjName(rs.getString(1));
				data.setBudget(rs.getString(2));
				data.setTargetReleaseQuarter(rs.getString(3));
				data.setStart(rs.getString(4));
				data.setEnd(rs.getString(5));
				data.setDeliveryManager(rs.getString(6));
				data.setSolutionManager(rs.getString(7));
				data.setDeliveryManager(rs.getString(8));
				data.setId(rs.getString(9));
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<EmployeeData> getResourceRequest(String id, String quarter, String year) {
		List<EmployeeData> data = Collections.emptyList();
		try {
			String sql = "with cecs(cec) as (select cec_id from employee where reporting_manager=?), working(cec,pfm,m1,m2,m3,group_id) as (select cec_id,pfm_id,m1,m2,m3,group_id from resource_allocation,cecs where cec_id=cec and quarter=? and year1=? and status='PENDING') select cec,display_name,pfm,project_name,m1,m2,m3,group_id from working,employee,project1 where working.cec=employee.cec_id and working.pfm=project1.pfm_id";
			Connection conn = ConnectionProvider.getCon();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ps.setString(2, quarter);
			ps.setString(3, year);
			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			if (rs.isBeforeFirst()) {
				while (rs.next()) {
					EmployeeData employee = new EmployeeData();
					employee.setCec_id(rs.getString(1));
					employee.setName(rs.getString(2));
					employee.getProject().setId(rs.getString(3));
					employee.getProject().setName(rs.getString(4));
					employee.setM1(rs.getString(5));
					employee.setM2(rs.getString(6));
					employee.setM3(rs.getString(7));
					employee.setGroupId(rs.getString(8));
					employee.setType("p");
					data.add(employee);
				}
			}

			String sql1 = "with cecs(cec) as (select temp_cec_id from temp_employee where reporting_manager=?), working(cec,pfm,m1,m2,m3,group_id) as (select temp_cec_id,pfm_id,m1,m2,m3,group_id from temp_resource_allocation,cecs where temp_cec_id=cec and quarter=? and year1=? and status='PENDING') select cec,display_name,pfm,project_name,m1,m2,m3,group_id from working,temp_employee,project1 where working.cec=temp_employee.temp_cec_id and working.pfm=project1.pfm_id";
			PreparedStatement ps1 = conn.prepareStatement(sql1);
			ps1.setString(1, id);
			ps1.setString(2, quarter);
			ps1.setString(3, year);
			ResultSet rs1 = ps1.executeQuery();
			if (rs1.isBeforeFirst()) {
				data = new ArrayList<>();
				while (rs1.next()) {
					EmployeeData employee = new EmployeeData();
					employee.setCec_id(rs1.getString(1));
					employee.setName(rs1.getString(2));
					employee.getProject().setId(rs1.getString(3));
					employee.getProject().setName(rs1.getString(4));
					employee.setM1(rs1.getString(5));
					employee.setM2(rs1.getString(6));
					employee.setM3(rs1.getString(7));
					employee.setGroupId(rs1.getString(8));
					employee.setType("t");
					data.add(employee);
				}
			}
			ConnectionProvider.close(ps1, rs1);
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;

	}

	public static List<ProjectData> getMyProjects(String id, String quarter, String year) { // Used
																							// in
																							// reportingmanager/myProjects.jsp
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "with pfms(pfm,m1,m2,m3) as (select pfm_id,m1,m2,m3 from resource_allocation where cec_id=? and status='APPROVED' and year1=? and quarter=?), pgcec(pfm,cec) as (select pfm_id,project_manager from project1,pfms where pfm_id=pfm), pgnme(pfm,pgcec,pgnm) as (select pfm,cec_id,display_name from employee,pgcec where cec_id=cec) select pfm_id,project_name,pgcec,pgnm,m1,m2,m3 from project1,pfms,pgnme where pfm_id=pfms.pfm and pfm_id=pgnme.pfm";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ps.setString(2, year);
			ps.setString(3, quarter);

			ResultSet rs = ps.executeQuery();

			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setId(rs.getString(1));
				project.setName(rs.getString(2));
				project.setIdProjectManager(rs.getString(3));
				project.setProjectManager(rs.getString(4));
				project.setM1(rs.getString(5));
				project.setM2(rs.getString(6));
				project.setM3(rs.getString(7));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static int getNumberOfEmployees(String RM_ID, String quarter, String year) {
		int number = 0;
		try {
			String sql = "WITH PROJ_EMP(CEC,EMPNM) AS (SELECT UNIQUE(EMPLOYEE.CEC_ID),EMP_NAME FROM EMPLOYEE,RESOURCE_ALLOCATION WHERE REPORTING_MANAGER = (select emp_name from employee where CEC_ID=?) AND EMPLOYEE.CEC_ID=RESOURCE_ALLOCATION.CEC_ID AND QUARTER=? AND YEAR1=?) SELECT COUNT(CEC) FROM PROJ_EMP";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, RM_ID);
			ps.setString(2, quarter);
			ps.setString(3, year);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				number = rs.getInt(1);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return number;
	}

	public static List<EmployeeData> getListOfEmployees(String id, String quarter) {
		List<EmployeeData> data = Collections.emptyList();
		try {
			String sql = "SELECT UNIQUE(CEC_ID),EMP_NAME FROM EMPLOYEE WHERE REPORTING_MANAGER = (select emp_name from employee where CEC_ID=?)";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ps.setString(2, quarter);
			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employee = new EmployeeData();
				employee.setCec_id(rs.getString(1));
				employee.setName(rs.getString(2));
				data.add(employee);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static List<ProjectData> getMyEmployeesProject(String id, String quarter) {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "WITH EMPS(CEC,EMPNM) AS (SELECT UNIQUE(CEC_ID),EMP_NAME FROM EMPLOYEE WHERE REPORTING_MANAGER = (select emp_name from employee where CEC_ID=?)), PRGTS(PFM) AS (SELECT UNIQUE(PFM_ID) FROM RESOURCE_ALLOCATION,EMPS WHERE CEC_ID = CEC AND STATUS='APPROVED') SELECT PFM,PROJECT_NAME FROM PRGTS,PROJECT1 WHERE PROJECT1.PFM_ID = PRGTS.PFM";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ps.setString(2, quarter);
			ResultSet rs = ps.executeQuery();

			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setId(rs.getString(1));
				project.setName(rs.getString(2));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static List<EmployeeData> getReportingEmployee(String id, String quarter) {
		List<EmployeeData> data = Collections.emptyList();
		try {
			String sql = "SELECT UNIQUE(EMPLOYEE.CEC_ID),EMP_NAME FROM EMPLOYEE,RESOURCE_ALLOCATION WHERE REPORTING_MANAGER = (select emp_name from employee where CEC_ID=?) AND EMPLOYEE.CEC_ID=RESOURCE_ALLOCATION.CEC_ID AND QUARTER=?";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ps.setString(2, quarter);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employee = new EmployeeData();
				employee.setCec_id(rs.getString(1));
				employee.setName(rs.getString(2));
				data.add(employee);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static List<ProjectData> getProjectsOfAllEmployees(String RM_ID, String quarter, String year) { // Used
																											// in
																											// reportingmanager/Reports.jsp
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "with perm_emp(pcec) as (select unique(cec_id) from employee where reporting_manager=?), temp_cec(tcec) as (select unique(temp_cec_id) from temp_employee where reporting_manager=?), perm(ppfm,pcnt) as (select unique(pfm_id),count(unique(cec_id)) from resource_allocation,perm_emp where cec_id=pcec and status='APPROVED' and quarter=? and year1=? group by pfm_id), temp(tpfm,tcnt) as (select unique(pfm_id),count(unique(temp_cec_id)) from temp_resource_allocation,temp_cec where temp_cec_id=tcec and status='APPROVED' and quarter=? and year1=? group by pfm_id), perm_tots(ppfm,pcnt) as (select pfm_id,count(unique(cec_id)) from resource_allocation where status='APPROVED' and quarter=? and year1=? group by pfm_id), temp_tots(tpfm,tcnt) as (select pfm_id,count(unique(temp_cec_id)) from temp_resource_allocation where status='APPROVED' and quarter=? and year1=? group by pfm_id), ans(pfm,pnme,cnt) as (select pfm_id,project_name,(NVL(pcnt,0)+NVL(tcnt,0)) from perm full outer join temp on ppfm=tpfm left outer join project1 on pfm_id in (ppfm,tpfm)), tots(pfm,pnme,tot) as (select pfm_id,project_name,(NVL(pcnt,0)+NVL(tcnt,0)) from perm_tots full outer join temp_tots on ppfm=tpfm left outer join project1 on pfm_id in (ppfm,tpfm)) select ans.pfm,ans.pnme,cnt,tot from ans,tots where ans.pfm=tots.pfm";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, RM_ID);
			ps.setString(2, RM_ID);
			ps.setString(3, quarter);
			ps.setString(4, year);
			ps.setString(5, quarter);
			ps.setString(6, year);
			ps.setString(7, quarter);
			ps.setString(8, year);
			ps.setString(9, quarter);
			ps.setString(10, year);

			ResultSet rs = ps.executeQuery();

			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setId(rs.getString(1));
				project.setName(rs.getString(2));
				project.setNoOfEmployees(rs.getString(3));
				project.setTotalEmployees(rs.getString(4));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static List<ProjectData> getTotalEmployeesInEachProjectUnderRM(String RM_ID, String quarter, String year) {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "with cecs(cec) as (select unique(cec_id) from employee where reporting_manager=?), prjts(pfm) as (select unique(pfm_id) from resource_allocation,cecs where cec_id=cec and status='APPROVED' and year1=? and quarter=?) select pfm_id,count(unique(cec_id)) from prjts,resource_allocation,cecs where pfm_id=pfm and cec_id=cec and status='APPROVED' and year1=? and quarter=? group by pfm_id";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, RM_ID);
			ps.setString(2, year);
			ps.setString(3, quarter);
			ps.setString(4, year);
			ps.setString(5, quarter);

			ResultSet rs = ps.executeQuery();

			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setId(rs.getString(1));
				project.setNoOfEmployees(rs.getString(2));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static List<EmployeeData> getAllRegionsOfEmployees(String RM_ID, String quarter, String year) {
		List<EmployeeData> data = Collections.emptyList();
		try {
			String sql = "WITH EMPS(CEC,EMPNM) AS (SELECT UNIQUE(EMPLOYEE.CEC_ID),EMP_NAME FROM EMPLOYEE,RESOURCE_ALLOCATION WHERE REPORTING_MANAGER = (select emp_name from employee where CEC_ID=?) AND EMPLOYEE.CEC_ID=RESOURCE_ALLOCATION.CEC_ID AND QUARTER=? AND YEAR1=?) SELECT UNIQUE(REGION) FROM EMPS,EMPLOYEE_DETAILS WHERE CEC=CEC_ID";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, RM_ID);
			ps.setString(2, quarter);
			ps.setString(3, year);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employee = new EmployeeData();
				employee.setRegion(rs.getString(1));
				data.add(employee);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static List<EmployeeData> getAllRegions(String RM_ID) {
		List<EmployeeData> data = Collections.emptyList();
		try {
			String sql = "WITH EMPS(CEC,EMPNM) AS (SELECT UNIQUE(EMPLOYEE.CEC_ID),EMP_NAME FROM EMPLOYEE WHERE REPORTING_MANAGER=(SELECT EMP_NAME FROM EMPLOYEE WHERE CEC_ID=?)) SELECT UNIQUE(REGION) FROM EMPLOYEE_DETAILS,EMPS WHERE CEC=CEC_ID";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, RM_ID);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employee = new EmployeeData();
				employee.setRegion(rs.getString(1));
				data.add(employee);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static int getNoOfRegionsOfAllEmployees(String RM_ID, String quarter, String year) {
		int number = 0;
		try {
			String sql = "WITH EMPS(CEC,EMPNM) AS (SELECT UNIQUE(EMPLOYEE.CEC_ID),EMP_NAME FROM EMPLOYEE,RESOURCE_ALLOCATION WHERE REPORTING_MANAGER = (select emp_name from employee where CEC_ID=?) AND EMPLOYEE.CEC_ID=RESOURCE_ALLOCATION.CEC_ID AND QUARTER=? AND YEAR1=?) SELECT COUNT(UNIQUE(REGION)) FROM EMPS,EMPLOYEE_DETAILS WHERE CEC=CEC_ID";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, RM_ID);
			ps.setString(2, quarter);
			ps.setString(3, year);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				number = rs.getInt(1);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return number;
	}

	public static int getNoOfRegionsOfAllEmployees(String RM_ID) {
		int number = 0;
		try {
			String sql = "WITH EMPS(CEC,EMPNM) AS (SELECT UNIQUE(EMPLOYEE.CEC_ID),EMP_NAME FROM EMPLOYEE WHERE REPORTING_MANAGER = (select emp_name from employee where CEC_ID=?)) SELECT COUNT(UNIQUE(REGION)) FROM EMPS,EMPLOYEE_DETAILS WHERE CEC=CEC_ID";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, RM_ID);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				number = rs.getInt(1);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return number;
	}

	public static List<EmployeeData> getUnderAllocated(String RM_ID, String quarter, String year) {
		List<EmployeeData> data = Collections.emptyList();
		try {
			String sql = "WITH EMPS(CEC,EMPNM) AS (SELECT UNIQUE(CEC_ID),EMP_NAME FROM EMPLOYEE WHERE REPORTING_MANAGER = (select emp_name from employee where CEC_ID=?)), ALLOTED(CEC,PFM,M1,M2,M3) AS (SELECT CEC_ID,PFM_ID,M1,M2,M3 FROM RESOURCE_ALLOCATION WHERE QUARTER=? AND YEAR1=? AND STATUS='APPROVED' AND (M1<100 OR M2<100 OR M3<100)) SELECT UNIQUE(EMPS.CEC),EMPNM,PROJECT_NAME,M1,M2,M3 FROM EMPS,ALLOTED,PROJECT1 WHERE EMPS.CEC=ALLOTED.CEC AND PROJECT1.PFM_ID=ALLOTED.PFM ORDER BY EMPS.CEC";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, RM_ID);
			ps.setString(2, quarter);
			ps.setString(3, year);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employee = new EmployeeData();
				employee.setCec_id(rs.getString(1));
				employee.setName(rs.getString(2));
				data.add(employee);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static int getNoOfUnderAllocatedEmployees(String RM_ID, String quarter) {
		int number = 0;
		try {
			String sql = "WITH EMPS(CEC,EMPNM) AS (SELECT UNIQUE(CEC_ID),EMP_NAME FROM EMPLOYEE WHERE REPORTING_MANAGER = (select emp_name from employee where CEC_ID=?)), ALLOTED(CEC,PFM,M1,M2,M3) AS (SELECT CEC_ID,PFM_ID,M1,M2,M3 FROM RESOURCE_ALLOCATION WHERE QUARTER=? AND STATUS='APPROVED' AND (M1<100 OR M2<100 OR M3<100)) SELECT COUNT(UNIQUE(EMPS.CEC)) FROM EMPS,ALLOTED,PROJECT1 WHERE EMPS.CEC=ALLOTED.CEC AND PROJECT1.PFM_ID=ALLOTED.PFM ORDER BY EMPS.CEC";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, RM_ID);
			ps.setString(2, quarter);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				number = rs.getInt(1);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return number;
	}

	public static List<EmployeeData> getOverAllocatedEmployees(String RM_ID, String quarter, String year) {
		List<EmployeeData> data = Collections.emptyList();
		try {
			String sql = "WITH EMPS(CEC,EMPNM) AS (SELECT UNIQUE(CEC_ID),EMP_NAME FROM EMPLOYEE WHERE REPORTING_MANAGER = (select emp_name from employee where CEC_ID=?)), ALLOTED(CEC,M1,M2,M3) AS (SELECT CEC_ID,M1,M2,M3 FROM RESOURCE_ALLOCATION WHERE QUARTER=? AND YEAR1=? AND STATUS='APPROVED' AND (M1>100 OR M2>100 OR M3>100)) SELECT EMPS.CEC,EMPNM,M1,M2,M3 FROM EMPS,ALLOTED WHERE EMPS.CEC=ALLOTED.CEC ORDER BY EMPS.CEC";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, RM_ID);
			ps.setString(2, quarter);
			ps.setString(3, year);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employee = new EmployeeData();
				employee.setCec_id(rs.getString(1));
				employee.setName(rs.getString(2));
				data.add(employee);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static int getNoOfOverAllocatedEmployees(String RM_ID, String quarter, String year) {
		int number = 0;
		try {
			String sql = "WITH EMPS(CEC,EMPNM) AS (SELECT UNIQUE(CEC_ID),EMP_NAME FROM EMPLOYEE WHERE REPORTING_MANAGER = (select emp_name from employee where CEC_ID=?)), ALLOTED(CEC,M1,M2,M3) AS (SELECT CEC_ID,M1,M2,M3 FROM RESOURCE_ALLOCATION WHERE QUARTER=? AND YEAR1=? AND STATUS='APPROVED' AND (M1>100 OR M2>100 OR M3>100)) SELECT COUNT(EMPS.CEC)FROM EMPS,ALLOTED WHERE EMPS.CEC=ALLOTED.CEC ORDER BY EMPS.CEC";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, RM_ID);
			ps.setString(2, quarter);
			ps.setString(3, year);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				number = rs.getInt(1);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return number;
	}

	public static List<ProjectData> getRegionAndEmployees(String RM_ID, String quarter, String year) {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "WITH EMPS(CEC,EMPNM) AS (SELECT UNIQUE(EMPLOYEE.CEC_ID),EMP_NAME FROM EMPLOYEE, RESOURCE_ALLOCATION WHERE REPORTING_MANAGER = (select emp_name from employee where CEC_ID=?) AND EMPLOYEE.CEC_ID=RESOURCE_ALLOCATION.CEC_ID AND QUARTER=? AND YEAR1=?) SELECT REGION,COUNT(CEC) FROM EMPS,EMPLOYEE_DETAILS WHERE CEC=CEC_ID GROUP BY REGION";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, RM_ID);
			ps.setString(2, quarter);
			ps.setString(3, year);

			ResultSet rs = ps.executeQuery();

			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setRegion(rs.getString(1));
				project.setNoOfEmployees(rs.getString(2));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static List<ProjectData> getRegionAndEmployees(String RM_ID) {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "with cecs(cec) as (select unique(cec_id) from employee where reporting_manager=?), regns(cec,reg) as (select unique(cec_id), substr(region,1,2) from employee_details,cecs where cec_id=cec) select reg,count(unique(cec_id)) from regns,employee_details where reg=substr(region,1,2) and cec_id=cec group by reg";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, RM_ID);

			ResultSet rs = ps.executeQuery();

			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setRegion(rs.getString(1));
				project.setNoOfEmployees(rs.getString(2));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static List<ProjectData> getAllProjectEmployeeCount(String RM_ID) {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "WITH EMPS(CEC,EMPNM) AS (SELECT UNIQUE(CEC_ID),EMP_NAME FROM EMPLOYEE WHERE REPORTING_MANAGER = (select emp_name from employee where CEC_ID=?)), PRGTS(PFM) AS (SELECT UNIQUE(PFM_ID) FROM RESOURCE_ALLOCATION,EMPS WHERE CEC_ID = CEC AND STATUS='APPROVED'), EMP(PF,COUNT_CEC) AS (SELECT PFM,COUNT(CEC_ID) FROM EMPLOYEE_PROJECT,PRGTS WHERE PRGTS.PFM=EMPLOYEE_PROJECT.PFM_ID GROUP BY PFM) SELECT PF,PROJECT_NAME,COUNT_CEC FROM EMP,PROJECT1 WHERE PROJECT1.PFM_ID = EMP.PF";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, RM_ID);
			ResultSet rs = ps.executeQuery();

			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setId(rs.getString(1));
				project.setName(rs.getString(2));
				project.setNoOfEmployees(rs.getString(3));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static List<EmployeeData> getAllRegion(String pfm_id, String quarter, String year) {
		List<EmployeeData> data = Collections.emptyList();
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "WITH EMPS(CEC) AS (SELECT CEC_ID FROM RESOURCE_ALLOCATION WHERE PFM_ID=? AND QUARTER=? AND YEAR1=?), REG(CEC,REGION) AS (SELECT CEC_ID,REGION FROM EMPLOYEE_DETAILS,EMPS WHERE CEC_ID=CEC) SELECT UNIQUE(REGION) FROM REG";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, pfm_id);
			ps.setString(2, quarter);
			ps.setString(3, year);

			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				data = new ArrayList<>();
				while (rs.next()) {
					EmployeeData employee = new EmployeeData();
					employee.setRegion(rs.getString(1));
					data.add(employee);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<EmployeeData> getTempResource(String proj_id, String year, String quarter, String status) {
		List<EmployeeData> data = Collections.emptyList();
		try {
			Connection con = ConnectionProvider.getCon();
			// Todo change query so that project_id is first and project name is
			// second
			String sql = "with emps(cec,m1,m2,m3) as (select unique(temp_cec_id),sum(m1),sum(m2),sum(m3) from temp_resource_allocation where pfm_id=? and year1=? and quarter=? and status=? group by temp_cec_id), rm_cec(cec,rmcec) as (select temp_cec_id,NVL(reporting_manager,'') from temp_employee,emps where temp_cec_id=cec) select temp_emp_id,temp_cec_id,display_name,m1,m2,m3,rm_cec.cec,rm_cec.rmcec from temp_employee,emps,rm_cec where emps.cec=temp_cec_id and rm_cec.cec=temp_cec_id";
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, proj_id);
			ps.setString(2, year);
			ps.setString(3, quarter);
			ps.setString(4, status);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employeeData = new EmployeeData();
				employeeData.setEmp_id(rs.getString(1));
				employeeData.setCec_id(rs.getString(2));
				employeeData.setName(rs.getString(3));
				employeeData.setM1(rs.getString(4));
				employeeData.setM2(rs.getString(5));
				employeeData.setM3(rs.getString(6));
				employeeData.setType("t");
				if (rs.getString(8) != null) {
					employeeData.setReportingManager(EmployeeDao.getName(rs.getString(8)));
				} else {
					employeeData.setReportingManager("Not Assigned");
				}
				data.add(employeeData);
			}
			ConnectionProvider.close(con, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<EmployeeData> getProjectEmpDetails(String proj_id, String quarter, String year, String status) { // Used
		// in
		// projectmanager/projects.jsp
		List<EmployeeData> data = Collections.emptyList();
		try {
			Connection con = ConnectionProvider.getCon();
			String sql = "with emps(cec,m1,m2,m3) as (select unique(cec_id),sum(m1),sum(m2),sum(m3) from resource_allocation where pfm_id=? and year1=? and quarter=? and status=? group by cec_id), rm_cec(cec,rmcec) as (select cec_id,reporting_manager from employee,emps where cec_id=cec), rm_nm(cec,rmcec,rmnm) as (select cec,cec_id,display_name from employee,rm_cec where cec_id=rmcec order by cec) select emp_id,cec_id,display_name,m1,m2,m3,rm_nm.cec,rm_nm.rmnm from employee,emps,rm_nm where emps.cec=cec_id and rm_nm.cec=cec_id";
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, proj_id);
			ps.setString(2, year);
			ps.setString(3, quarter);
			ps.setString(4, status);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employeeData = new EmployeeData();
				employeeData.setEmp_id(rs.getString(1));
				employeeData.setCec_id(rs.getString(2));
				employeeData.setName(rs.getString(3));
				employeeData.setM1(rs.getString(4));
				employeeData.setM2(rs.getString(5));
				employeeData.setM3(rs.getString(6));
				employeeData.setReportingManager(rs.getString(8));
				employeeData.setType("p");
				data.add(employeeData);
			}
			ConnectionProvider.close(con, ps, rs);
			data.addAll(getTempResource(proj_id, year, quarter, status));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<ProjectData> getRegionAndEmployeesInProject(String id, String pfm_id, String quarter,
			String year) {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "with cecs(cec) as (select unique(cec_id) from employee where reporting_manager=?), temp_cecs(tcec) as (select unique(temp_cec_id) from temp_employee where reporting_manager=?), pfms(cec) as (select unique(cec_id) from resource_allocation,cecs where status='APPROVED' and pfm_id=? and year1=? and quarter=? and cec_id=cec), temp_pfms(tcec) as (select unique(temp_cec_id) from temp_resource_allocation,temp_cecs where status='APPROVED' and pfm_id=? and year1=? and quarter=? and temp_cec_id=tcec), regns(cec,reg) as (select unique(cec_id), substr(region,1,2) from employee_details,pfms where cec_id=cec), temp_regns(tcec,treg) as (select unique(temp_cec_id), substr(region,1,2) from temp_employee,temp_pfms where temp_cec_id=tcec), perms (reg,cnt) as (select reg,count(unique(cec_id)) from regns,employee_details where reg=substr(region,1,2) and cec_id=cec group by reg), temps (treg,tcnt) as (select treg,count(unique(temp_cec_id)) from temp_regns,temp_employee where treg=substr(region,1,2) and temp_cec_id=tcec group by treg) select NVL(reg,treg),NVL(cnt,0)+NVL(tcnt,0) from perms full outer join temps on reg=treg";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ps.setString(2, id);
			ps.setString(3, pfm_id);
			ps.setString(4, year);
			ps.setString(5, quarter);
			ps.setString(6, pfm_id);
			ps.setString(7, year);
			ps.setString(8, quarter);

			ResultSet rs = ps.executeQuery();

			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setRegion(rs.getString(1));
				project.setNoOfEmployees(rs.getString(2));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static List<EmployeeData> getAllEmployeeAllocationInProject(String id, String pfm_id, String quarter,
			String year) {
		List<EmployeeData> data = Collections.emptyList();
		try {
			String sql = "with cecs(cec,empnm) as (select unique(cec_id),display_name from employee where reporting_manager=?), temp_cecs(tcec,tempnm) as (select unique(temp_cec_id),display_name from temp_employee where reporting_manager=?), perms(pcec,pemp,pm1,pm2,pm3) as (select unique(cec_id),empnm,m1,m2,m3 from resource_allocation,cecs where status='APPROVED' and pfm_id=? and quarter=? and year1=? and cec_id=cec), temps(tcec,temp,tm1,tm2,tm3) as (select unique(temp_cec_id),tempnm,m1,m2,m3 from temp_resource_allocation,temp_cecs where status='APPROVED' and pfm_id=? and quarter=? and year1=? and temp_cec_id=tcec) select NVL(pcec,tcec),NVL(pemp,temp),NVL(pm1,tm1),NVL(pm2,tm2),NVL(pm3,tm3) from perms full outer join temps on pemp=temp";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ps.setString(2, id);
			ps.setString(3, pfm_id);
			ps.setString(4, quarter);
			ps.setString(5, year);
			ps.setString(6, pfm_id);
			ps.setString(7, quarter);
			ps.setString(8, year);
			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employee = new EmployeeData();
				employee.setCec_id(rs.getString(1));
				employee.setName(rs.getString(2));
				employee.setM1(rs.getString(3));
				employee.setM2(rs.getString(4));
				employee.setM3(rs.getString(5));
				data.add(employee);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static boolean updateNotificationStatus(String to, String group_id) {
		boolean result = false;
		try {
			Connection con = ConnectionProvider.getCon();
			String sql = "update notify set seen_status='seen' where to1=? and group_id=?";
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, to);
			ps.setString(2, group_id);
			con.setAutoCommit(false);

			int rows = ps.executeUpdate();
			if (rows == 1) {
				con.commit();
				result = true;
			}
			ConnectionProvider.close(con, ps);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;

	}

	public static List<EmployeeData> getNotifyResReq(String gid, String cec_id) {
		List<EmployeeData> data = Collections.emptyList();
		try {
			// Getting permanent request
			String sql = "with prjt(pfm,grp,to1) as (select unique(pfm_id),group_id,to1 from notify where group_id=? and to1=?), pnme(pfm,pjnm) as (select unique(pfm_id),project_name from project1,prjt where pfm=pfm_id) select pfm_id,pjnm,resource_allocation.cec_id,display_name,m1,m2,m3,resource_allocation.status,resource_allocation.comments from resource_allocation,prjt,pnme,employee where prjt.pfm=pfm_id and pfm_id=pnme.pfm and group_id=grp and employee.cec_id=resource_allocation.cec_id and reporting_manager=prjt.to1 order by status desc";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, gid);
			ps.setString(2, cec_id);

			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				data = new ArrayList<>();
				while (rs.next()) {
					EmployeeData employee = new EmployeeData();
					employee.setCec_id(rs.getString(3));
					employee.setName(rs.getString(4));
					employee.getProject().setId(rs.getString(1));
					employee.getProject().setName(rs.getString(2));
					employee.setM1(rs.getString(5));
					employee.setM2(rs.getString(6));
					employee.setM3(rs.getString(7));
					employee.setStatus(rs.getString(8));
					employee.setComment(rs.getString(9));
					employee.setType("p");
					data.add(employee);
				}
			}
			// Getting temporary resource
			String sql1 = "with prjt(pfm,grp,to1) as (select unique(pfm_id),group_id,to1 from notify where group_id=? and to1=?), pnme(pfm,pjnm) as (select unique(pfm_id),project_name from project1,prjt where pfm=pfm_id) select pfm_id,pjnm,temp_resource_allocation.temp_cec_id,display_name,m1,m2,m3,temp_resource_allocation.status,temp_resource_allocation.comments from pnme,temp_resource_allocation,temp_employee,prjt where pnme.pfm=pfm_id and prjt.pfm=pfm_id and group_id=grp and temp_resource_allocation.temp_cec_id=temp_employee.temp_cec_id and reporting_manager=prjt.to1";
			PreparedStatement ps1 = conn.prepareStatement(sql1);
			ps1.setString(1, gid);
			ps1.setString(2, cec_id);

			ResultSet rs1 = ps1.executeQuery();
			if (rs1.isBeforeFirst()) {
				if (data.size() == 0)
					data = new ArrayList<>();
				while (rs1.next()) {
					EmployeeData employee = new EmployeeData();
					employee.setCec_id(rs1.getString(3));
					employee.setName(rs1.getString(4));
					employee.getProject().setId(rs1.getString(1));
					employee.getProject().setName(rs1.getString(2));
					employee.setM1(rs1.getString(5));
					employee.setM2(rs1.getString(6));
					employee.setM3(rs1.getString(7));
					employee.setStatus(rs1.getString(8));
					employee.setComment(rs1.getString(9));
					employee.setType("t");
					data.add(employee);
				}
			}
			ConnectionProvider.close(ps1, rs1);
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static int getNumberofUnreadNotifications(String CEC_ID) {
		String sql = "select count(unique(group_id)) from notify where (TO1=? and seen_status='unseen')";
		int numberofnotification = 0;
		Connection conn = ConnectionProvider.getCon();

		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_ID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				numberofnotification = rs.getInt(1);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return numberofnotification;
	}

	public static List<ProjectData> getRegionAndEmployee(String pfm_id, String quarter, String year) { // Used
		// in
		// reportingmanager/reports.jsp
		// |
		// called
		// by
		// RegionsChart()
		List<ProjectData> data = Collections.emptyList();
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "with emps(cec) as (select cec_id from resource_allocation where pfm_id=? and year1=? and quarter=? and status='APPROVED') select substr(region,1,2) as reg,count(cec_id) from emps,employee_details where cec_id=cec group by region";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, pfm_id);
			ps.setString(2, year);
			ps.setString(3, quarter);

			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				data = new ArrayList<>();
				while (rs.next()) {
					ProjectData project = new ProjectData();
					project.setRegion(rs.getString(1));
					project.setNoOfEmployees(rs.getString(2));
					data.add(project);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<EmployeeData> getProjectResourcesEmp(String proj_id, String quarter, String year) { // Used
		// in
		// reportingmanager/employeeDetail.jsp
		List<EmployeeData> data = Collections.emptyList();
		try {
			Connection con = ConnectionProvider.getCon();
			String sql = "with emps(cec,m1,m2,m3,status) as (select cec_id,m1,m2,m3,status from resource_allocation where pfm_id=? and year1=? and quarter=?) select emp_id,cec_id,display_name,m1,m2,m3,emps.status from employee,emps where cec=cec_id";
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, proj_id);
			ps.setString(2, year);
			ps.setString(3, quarter);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employeeData = new EmployeeData();
				employeeData.setEmp_id(rs.getString(1));
				employeeData.setCec_id(rs.getString(2));
				employeeData.setName(rs.getString(3));
				employeeData.setM1(rs.getString(3));
				employeeData.setM2(rs.getString(4));
				employeeData.setM3(rs.getString(5));
				employeeData.setStatus(rs.getString(6));
				data.add(employeeData);
			}
			ConnectionProvider.close(con, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * data = new ArrayList<>(); for (int i = 0; i < 5; i++) { EmployeeData
		 * employeeData = new EmployeeData(); employeeData.setId("" + (i + 1));
		 * employeeData.setName("employee " + (i + 1)); data.add(employeeData);
		 * }
		 */
		return data;
	}

	public static List<ProjectData> getRMsandEmployeesCount(String PFM_ID, String quarter, String year) {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "with pfms(cec) as (select cec_id from resource_allocation where pfm_id=? and status='APPROVED' and year1=? and quarter=?), rm(cec,counts) as (select reporting_manager,count(cec_id) from employee,pfms where cec_id=cec group by reporting_manager) select cec_id,display_name,counts from employee,rm where cec=cec_id";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, PFM_ID);
			ps.setString(2, year);
			ps.setString(3, quarter);

			ResultSet rs = ps.executeQuery();

			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setRMId(rs.getString(1));
				project.setRMName(rs.getString(2));
				project.setNoOfEmployees(rs.getString(3));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static boolean updateRequest(List<EmployeeData> data) {
		boolean result = false;
		Connection con = ConnectionProvider.getCon();
		try {
			int i;
			con.setAutoCommit(false);
			String sql;
			for (i = 0; i < data.size(); i++) {
				if (data.get(i).getType().equalsIgnoreCase("p"))
					sql = "UPDATE RESOURCE_ALLOCATION SET LAST_UPDATED=CURRENT_TIMESTAMP,STATUS=?,COMMENTS=? WHERE CEC_ID=? AND PFM_ID=? AND QUARTER=? AND YEAR1=? and group_id=?";
				else
					sql = "UPDATE TEMP_RESOURCE_ALLOCATION SET LAST_UPDATED=CURRENT_TIMESTAMP,STATUS=?,COMMENTS=? WHERE TEMP_CEC_ID=? AND PFM_ID=? AND QUARTER=? AND YEAR1=? and group_id=?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, data.get(i).getStatus());
				ps.setString(2, data.get(i).getComment());
				ps.setString(3, data.get(i).getCec_id());
				ps.setString(4, data.get(i).getProject().getId());
				ps.setString(5, data.get(i).getQuarter());
				ps.setString(6, data.get(i).getYear());
				ps.setString(7, data.get(i).getGroupId());
				ps.executeQuery();
				ConnectionProvider.close(ps);
			}
			if (i == data.size())
				con.commit();
			result = true;
			ConnectionProvider.close(con);
		} catch (SQLException e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	/*------------------------   Code for charts, return type as requested by UI developer   ----------*/

	public static String JSgetAllProjectEmployeeCount(String RM_ID) {
		List<ProjectData> projects = ReportingManagerDao.getAllProjectEmployeeCount(RM_ID);
		StringBuilder builder = new StringBuilder("");
		for (int i = 0; i < projects.size(); i++) {
			builder.append(projects.get(i).getName() + "-" + projects.get(i).getNoOfEmployees());
			if (i < projects.size() - 1)
				builder.append("\n");
		}
		return builder.toString();
	}

	public static String JSgetRegionAndEmployees(String RM_ID, String quarter, String year) {
		List<ProjectData> projects = ReportingManagerDao.getRegionAndEmployees(RM_ID, quarter, year);
		StringBuilder builder = new StringBuilder("");
		for (int i = 0; i < projects.size(); i++) {
			builder.append(projects.get(i).getRegion() + "-" + projects.get(i).getNoOfEmployees());
			if (i < projects.size() - 1)
				builder.append("\n");
		}
		return builder.toString();
	}

	public static String JSgetRegionAndEmployees(String RM_ID) {
		List<ProjectData> projects = ReportingManagerDao.getRegionAndEmployees(RM_ID);
		StringBuilder builder = new StringBuilder("");
		for (int i = 0; i < projects.size(); i++) {
			builder.append(projects.get(i).getRegion() + "-" + projects.get(i).getNoOfEmployees());
			if (i < projects.size() - 1)
				builder.append("\n");
		}
		return builder.toString();
	}

	public static String JSgetOverAllocatedEmployees(String RM_ID, String quarter, String year) {
		List<EmployeeData> employees = ReportingManagerDao.getOverAllocatedEmployees(RM_ID, quarter, year);
		StringBuilder builder = new StringBuilder("");
		for (int i = 0; i < employees.size(); i++) {
			builder.append(employees.get(i).getName());
			if (i < employees.size() - 1)
				builder.append(",");
		}
		return builder.toString();
	}

	public static String JSgetUnderAllocated(String RM_ID, String quarter, String year) {
		List<EmployeeData> employees = ReportingManagerDao.getUnderAllocated(RM_ID, quarter, year);
		StringBuilder builder = new StringBuilder("");
		for (int i = 0; i < employees.size(); i++) {
			builder.append(employees.get(i).getName());
			if (i < employees.size() - 1)
				builder.append(",");
		}
		return builder.toString();
	}

	public static String JSgetAllRegionsOfEmployees(String RM_ID, String quarter, String year) {
		List<EmployeeData> employees = ReportingManagerDao.getAllRegionsOfEmployees(RM_ID, quarter, year);
		StringBuilder builder = new StringBuilder("");
		for (int i = 0; i < employees.size(); i++) {
			builder.append(employees.get(i).getRegion());
			if (i < employees.size() - 1)
				builder.append(",");
		}
		return builder.toString();
	}

	
	public static String JSgetReportingEmployee(String id, String quarter) {
		List<EmployeeData> employees = ReportingManagerDao.getReportingEmployee(id, quarter);
		StringBuilder builder = new StringBuilder("");
		for (int i = 0; i < employees.size(); i++) {
			builder.append(employees.get(i).getName());
			if (i < employees.size() - 1)
				builder.append(",");
		}
		return builder.toString();
	}

	
	public static String JSgetAllEmployeesUnderRM(String id) {
		List<EmployeeData> employees = ReportingManagerDao.getAllEmployeesUnderRM(id);
		StringBuilder builder = new StringBuilder("");
		for (int i = 0; i < employees.size(); i++) {
			builder.append(employees.get(i).getName());
			if (i < employees.size() - 1)
				builder.append(",");
		}
		return builder.toString();
	}

	public static String JSgetRegion(String RM_ID) {
		List<EmployeeData> employee = ReportingManagerDao.getAllRegions(RM_ID);
		StringBuilder builder = new StringBuilder("");
		for (int i = 0; i < employee.size(); i++) {
			builder.append(employee.get(i).getRegion());
			if (i < employee.size() - 1)
				builder.append(",");
		}
		return builder.toString();
	}

	public static String JS_MA_getAllEmployeeNames(String id, String quarter, String year) { // Used
																								// in
																								// reportingmanager/Reports.jsp
		List<EmployeeData> employees = ReportingManagerDao.getAllEmployeeAllocation(id, quarter, year);
		StringBuilder builder = new StringBuilder("");
		for (int i = 0; i < employees.size(); i++) {
			builder.append("'" + employees.get(i).getName() + "'");
			if (i < employees.size() - 1)
				builder.append(",");
		}
		return builder.toString();
	}

	public static String JS_MA_getAllEmployeeM1(String id, String quarter, String year) { // Used
																							// in
																							// reportingmanager/Reports.jsp
		List<EmployeeData> employees = ReportingManagerDao.getAllEmployeeAllocation(id, quarter, year);
		StringBuilder builder = new StringBuilder("");
		for (int i = 0; i < employees.size(); i++) {
			builder.append(Integer.parseInt(employees.get(i).getM1()) - 100);
			if (i < employees.size() - 1)
				builder.append(",");
		}
		return builder.toString();
	}

	public static String JS_MA_getAllEmployeeM2(String id, String quarter, String year) { // Used
																							// in
																							// reportingmanager/Reports.jsp
		List<EmployeeData> employees = ReportingManagerDao.getAllEmployeeAllocation(id, quarter, year);
		StringBuilder builder = new StringBuilder("");
		for (int i = 0; i < employees.size(); i++) {
			builder.append(Integer.parseInt(employees.get(i).getM2()) - 100);
			if (i < employees.size() - 1)
				builder.append(",");
		}
		return builder.toString();
	}

	public static String JS_MA_getAllEmployeeM3(String id, String quarter, String year) { // Used
																							// in
																							// reportingmanager/Reports.jsp
		List<EmployeeData> employees = ReportingManagerDao.getAllEmployeeAllocation(id, quarter, year);
		StringBuilder builder = new StringBuilder("");
		for (int i = 0; i < employees.size(); i++) {
			builder.append(Integer.parseInt(employees.get(i).getM3()) - 100);
			if (i < employees.size() - 1)
				builder.append(",");
		}
		return builder.toString();
	}

	public static String RegionsChart(String RM_ID) // Used in
													// reportingmanager/Reports.jsp
	{
		List<ProjectData> redata = getRegionAndEmployees(RM_ID);
		StringBuilder series = new StringBuilder("");
		series.append(",series: [{ name: 'Percent of Employees', colorByPoint: true, data: [");
		for (int i = 0; i < redata.size(); i++) {
			series.append("{name:'" + redata.get(i).getRegion() + "'," + "y:" + redata.get(i).getNoOfEmployees() + "}");
			if (i < redata.size() - 1)
				series.append(",");
		}
		series.append("]}]");

		return series.toString();
	}


	public static String PWRegionsChart(String RM_ID, String quarter, String year) { // Used
																						// in
																						// reportingmanager/projects.jsp
		List<ProjectData> redata = getRegionAndEmployee(RM_ID, quarter, year);
		StringBuilder series = new StringBuilder("");
		series.append(",series: [{ name: 'Percent of Employees', colorByPoint: true, data: [");
		for (int i = 0; i < redata.size(); i++) {
			series.append("{name:'" + redata.get(i).getRegion() + "'," + "y:" + redata.get(i).getNoOfEmployees() + "}");
			if (i < redata.size() - 1)
				series.append(",");
		}
		series.append("]}]");

		return series.toString();
	}

	public static String RMwiseChart(String PFM_ID) // Added by Varun
	{
		List<ProjectData> redata = getRegionAndEmployees(PFM_ID);
		StringBuilder series = new StringBuilder("");
		series.append(",series: [{ name: 'Percent of Employees', colorByPoint: true, data: [");
		for (int i = 0; i < redata.size(); i++) {
			series.append("{name:'" + redata.get(i).getRegion() + "'," + "y:" + redata.get(i).getNoOfEmployees() + "}");
			if (i < redata.size() - 1)
				series.append(",");
		}
		series.append("]}]");

		return series.toString();
	}

	public static String getRMsandEmployeesCountChart(String PFM_ID, String quarter, String year) // Used in reportingmanager/Reports.jsp
	{
		List<ProjectData> rmdata = getRMsandEmployeesCount(PFM_ID, quarter, year);
		StringBuilder series = new StringBuilder("");
		series.append(",series: [{ name: 'Percent of Employees', colorByPoint: true, data: [");
		for (int i = 0; i < rmdata.size(); i++) {
			series.append("{name:'" + rmdata.get(i).getRMName().split("\\(")[0] + "'," + "y:"
					+ rmdata.get(i).getNoOfEmployees() + "}");
			if (i < rmdata.size() - 1)
				series.append(",");
		}
		series.append("]}]");

		return series.toString();
	}

	public static String RegionsChart(String PFM_ID, String quarter, String year) { // Used
																					// in
																					// reportingmanager/reports.jsp
		List<ProjectData> redata = getRegionAndEmployee(PFM_ID, quarter, year);
		StringBuilder series = new StringBuilder("");
		series.append(",series: [{ name: 'Percent of Employees', colorByPoint: true, data: [");
		for (int i = 0; i < redata.size(); i++) {
			series.append("{name:'" + redata.get(i).getRegion() + "'," + "y:" + redata.get(i).getNoOfEmployees() + "}");
			if (i < redata.size() - 1)
				series.append(",");
		}
		series.append("]}]");

		return series.toString();
	}

}