package com.cisco.rmtool;

import java.util.List;

import com.google.gson.Gson;

public class ProjectData {
	String name;
	String id;
	String start;
	String end;
	String budget;
	String status;
	String deliveryManager;
	String solutionManager;
	String projectManager;
	String noOfEmployees;
	String region;
	String IdProjectManager;
	String Funding_source;
	String description;
	String progress;
	String targetReleaseQuarter;
	String prj_name;
	String TotalEmployees;
	String M1;
	String M2;
	String M3;
	String RMId;
	String RMName;
	String quarter;
	String year;
	String QFY;

	public String getProgress() {
		if (progress == null)
			return "0";
		if (Integer.parseInt(progress) > 100)
			return "100";
		return progress;
	}

	public void setProgress(String progress) {
		this.progress = progress;
	}

	public String getFunding_source() {
		return Funding_source;
	}

	public void setFunding_source(String funding_source) {
		Funding_source = funding_source;
	}

	public String getIdProjectManager() {
		if (IdProjectManager == null)
			return "Not Available";
		return IdProjectManager;
	}

	public void setIdProjectManager(String idProjectManager) {
		IdProjectManager = idProjectManager;
	}

	public String getRegion() {
		if (region == null)
			return "Not Assigned";
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getProjectManager() {
		if (projectManager == null)
			return "Not Assigned";
		return projectManager;
	}

	public String getNoOfEmployees() {
		if (noOfEmployees == null)
			return "0";
		return noOfEmployees;
	}

	public void setNoOfEmployees(String noOfEmployees) {
		this.noOfEmployees = noOfEmployees;
	}

	public void setProjectManager(String projectManager) {
		this.projectManager = projectManager;
	}

	public String getDeliveryManager() {
		if (deliveryManager == null)
			return "Not Assigned";
		return deliveryManager;
	}

	public String getSolutionManager() {
		if (solutionManager == null)
			return "Not Assigned";
		return solutionManager;
	}

	public void setDeliveryManager(String deliveryManager) {
		this.deliveryManager = deliveryManager;
	}

	public void setSolutionManager(String solutionManager) {
		this.solutionManager = solutionManager;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBudget() {
		if (budget == null)
			return "Not Set";
		return budget;
	}

	public String getEnd() {
		if (end == null)
			return "Not Set";
		return end;
	}

	public String getStart() {
		return start;
	}

	public void setBudget(String budget) {
		this.budget = budget;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		if (name == null)
			return "";
		return name;
	}

	public void setTargetReleaseQuarter(String targetReleaseQuarter) {
		this.targetReleaseQuarter = targetReleaseQuarter;
	}

	public String getTargetReleaseQuarter() {
		return targetReleaseQuarter;
	}

	public void setPrjName(String prj_name) {
		this.prj_name = prj_name;
	}

	public String getPrjName() {
		if (prj_name == null)
			return "Not Assigned";
		return prj_name;
	}

	public void setM1(String M1) {
		this.M1 = M1;
	}

	public String getM1() {
		return M1;
	}

	public void setM2(String M2) {
		this.M2 = M2;
	}

	public String getM2() {
		return M2;
	}

	public void setM3(String M3) {
		this.M3 = M3;
	}

	public String getM3() {
		return M3;
	}

	public String getDescription() {
		if (description == null)
			return "-";
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static String getProjectJson(ProjectData data) {
		Gson gson = new Gson();
		return gson.toJson(data);
	}

	public void setTotalEmployees(String TotalEmployees) {
		this.TotalEmployees = TotalEmployees;
	}

	public String getTotalEmployees() {
		if (TotalEmployees == null)
			return "0";
		return TotalEmployees;
	}

	public void setRMName(String RMName) {
		this.RMName = RMName;
	}

	public String getRMName() {
		return RMName;
	}

	public void setRMId(String RMId) {
		this.RMId = RMId;
	}

	public String getRMId() {
		return RMId;
	}
	
	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	public String getQuarter() {
		return quarter;
	}
	
	public void setYear(String year) {
		this.year = year;
	}

	public String getYear() {
		return year;
	}
	
	public void setQFY(String QFY) {
		this.QFY = QFY;
	}

	public String getQFY() {
		return QFY;
	}

	public static String getProjectJSON(List<ProjectData> data) {
		Gson gson = new Gson();
		return gson.toJson(data);
	}
}