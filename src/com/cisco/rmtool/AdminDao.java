package com.cisco.rmtool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AdminDao {

	public static List<ProjectData> getAllProjects(String quarter, String year) { // Used
																					// in
																					// dashboard.jsp
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "with pfms(pfm) as (select unique(pfm_id) from project_elapse where months in (?,?,?)), "
					+ "pgm_cec(pfm,cec) as (select pfm_id,project_manager from project1,pfms where pfm_id=pfm), "
					+ "pgm_nme(pfm,cec,pgnme) as (select pfm,cec_id,display_name from employee,pgm_cec where cec=cec_id), "
					+ "NUMER(NUM_DT,PFM) AS (SELECT (trunc(sysdate)-to_date(to_char(start_date, 'DD-MM-YYYY'))),PFM_ID FROM "
					+ "PROJECT1,pfms WHERE PFM_ID=PFM), DENOM(DEN_DT,PFM) AS "
					+ "(SELECT (to_date(to_char(end_date, 'DD-MM-YYYY'))-to_date(to_char(start_date,'DD-MM-YYYY'))),"
					+ "PFM_ID FROM PROJECT1,pfms WHERE PFM_ID=PFM), RESULTS(ANS,PFM) AS (SELECT NUM_DT/DEN_DT,NUMER.PFM FROM NUMER,"
					+ "DENOM WHERE NUMER.PFM=DENOM.PFM) select unique(pfm_id),project_name,cec,pgnme,delivary_manager,solution_manager,"
					+ "status,floor(ans*100) as progress from project1,pfms,pgm_nme,results where pfm_id=pfms.pfm and pfm_id=pgm_nme.pfm "
					+ "and pfm_id=results.pfm";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			QandY qY = new QandY(quarter + "FY" + (Integer.parseInt(year) % 100));
			ps.setString(1, qY.getM1() + "-" + qY.getY1());
			ps.setString(2, qY.getM2() + "-" + qY.getY2());
			ps.setString(3, qY.getM3() + "-" + qY.getY3());
			ResultSet rs = ps.executeQuery();

			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setId(rs.getString(1));
				project.setName(rs.getString(2));
				project.setIdProjectManager(rs.getString(3));
				project.setProjectManager(rs.getString(4));
				project.setDeliveryManager(rs.getString(5));
				project.setSolutionManager(rs.getString(6));
				project.setStatus(rs.getString(7));
				project.setProgress(rs.getString(8));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static List<EmployeeData> getAllEmployees() { // Used in
															// EmployeeDetails.jsp
		List<EmployeeData> data = Collections.emptyList();
		try {
			String sql = "with perms(pcec,pnm,perm) as (select unique(cec_id),display_name,'p' from employee), temps(tcec,tnm,temp) as (select unique(temp_cec_id),display_name,'t' from temp_employee)select NVL(pcec,tcec),NVL(pnm,tnm),NVL(perm,temp) from perms full outer join temps on pnm=tnm";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData project = new EmployeeData();
				project.setCec_id(rs.getString(1));
				project.setName(rs.getString(2).split("\\(")[0]);
				project.setType(rs.getString(3));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static List<ProjectData> getAllProjects() { // Used in
														// ProjectDetails.jsp
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "select PFM_ID, PROJECT_NAME from project1";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setId(rs.getString(1));
				project.setName(rs.getString(2));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static ProjectData getProjectDetails(String id) { // Used in
																// ProjectDetails.jsp
		ProjectData data = new ProjectData();
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "select project_name, budget, target_release_quarter,start_date,end_date, delivary_manager, solution_manager, project_desc, pfm_id from project1 WHERE pfm_ID = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				data.setPrjName(rs.getString(1));
				data.setBudget(rs.getString(2));
				data.setTargetReleaseQuarter(rs.getString(3));
				data.setStart(rs.getString(4));
				data.setEnd(rs.getString(5));
				data.setDeliveryManager(rs.getString(6));
				data.setSolutionManager(rs.getString(7));
				data.setDescription(rs.getString(8));
				data.setId(rs.getString(9));
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<EmployeeData> getProjectEmployeeDetails(String pfm_id, String quarter, String year,
			String status) { // Used in ProjectDetails.jsp
		List<EmployeeData> data = Collections.emptyList();
		try {
			Connection con = ConnectionProvider.getCon();
			String sql = "with pfms(cec,m1,m2,m3) as (select cec_id,m1,m2,m3 from resource_allocation where status=? and pfm_id=? and quarter=? and year1=?), temp_pfms(tcec,tm1,tm2,tm3) as (select temp_cec_id,m1,m2,m3 from temp_resource_allocation where status=? and pfm_id=? and quarter=? and year1=?), perm_ans(cec,emp,empnm,m1,m2,m3,perms) as (select cec_id,emp_id,display_name,m1,m2,m3,'p' from employee,pfms where cec_id=cec), temp_ans(tcec,temp,tempnm,tm1,tm2,tm3,temps) as (select temp_cec_id,temp_emp_id,display_name,tm1,tm2,tm3,'t' from temp_employee,temp_pfms where temp_cec_id=tcec) select NVL(cec,tcec),NVL(emp,temp),NVL(empnm,tempnm),NVL(m1,tm1),NVL(m2,tm2),NVL(m3,tm3),NVL(perms,temps) from perm_ans full outer join temp_ans on emp=temp";
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, status);
			ps.setString(2, pfm_id);
			ps.setString(3, quarter);
			ps.setString(4, year);
			ps.setString(5, status);
			ps.setString(6, pfm_id);
			ps.setString(7, quarter);
			ps.setString(8, year);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			while (rs.next()) {
				EmployeeData employeeData = new EmployeeData();
				employeeData.setCec_id(rs.getString(1));
				employeeData.setEmp_id(rs.getString(2));
				employeeData.setName(rs.getString(3).split("\\(")[0]);
				employeeData.setM1(rs.getString(4));
				employeeData.setM2(rs.getString(5));
				employeeData.setM3(rs.getString(6));
				employeeData.setType(rs.getString(7));
				data.add(employeeData);
			}
			ConnectionProvider.close(con, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static boolean addNewProject(ProjectData project) {
		boolean result = false;
		try {
			Connection con = ConnectionProvider.getCon();
			String sql = "INSERT INTO PROJECT1(pfm_id,project_name,project_desc,delivary_manager,solution_manager,project_manager,budget,target_release_quarter,start_date,end_date,status,funding_source,last_updated) VALUES(?,?,?,?,?,?,?,?,to_date(?,'YYYY-MM-DD'),to_date(?,'YYYY-MM-DD'),?,?,CURRENT_TIMESTAMP)";
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setInt(1, Integer.parseInt(project.getId()));
			ps.setString(2, project.getName());
			ps.setString(3, project.getDescription());
			ps.setString(4, project.getDeliveryManager());
			ps.setString(5, project.getSolutionManager());
			ps.setString(6, project.getProjectManager());
			ps.setInt(7, Integer.parseInt(project.getBudget()));
			ps.setString(8, project.getTargetReleaseQuarter());
			ps.setString(9, project.getStart());
			ps.setString(10, project.getEnd());
			ps.setString(11, project.getStatus());
			ps.setString(12, project.getFunding_source());

			ResultSet rs = ps.executeQuery();
			ConnectionProvider.close(con, ps, rs);
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}

	public static List<EmployeeData> getEmployeesNotAdminNotRM() {
		List<EmployeeData> data = Collections.emptyList();
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "select unique(cec_id),display_name from employee where admin1='NO' and cec_id not in (select reporting_manager from employee where reporting_manager is not null)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				data = new ArrayList<>();
				while (rs.next()) {
					EmployeeData employee = new EmployeeData();
					employee.setCec_id(rs.getString(1));
					employee.setName(rs.getString(2));
					data.add(employee);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<EmployeeData> getNotifyResReq(String gid) { // Used in
																	// reportingmanager/resourcerequest.jsp
		List<EmployeeData> data = Collections.emptyList();
		try {
			String sql = "with prjt(pfm,grp,to1) as (select unique(pfm_id),group_id,to1 from notify where group_id=? and to1 is null), pnme(pfm,pjnm) as (select unique(pfm_id),project_name from project1,prjt where pfm=pfm_id) select pfm_id,pjnm,temp_resource_allocation.temp_cec_id,display_name,m1,m2,m3,temp_resource_allocation.STATUS from pnme,temp_resource_allocation,temp_employee,prjt where pnme.pfm=pfm_id and prjt.pfm=pfm_id and group_id=grp and temp_resource_allocation.temp_cec_id=temp_employee.temp_cec_id and reporting_manager is null";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, gid);

			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			if (rs.isBeforeFirst()) {
				data = new ArrayList<>();
				while (rs.next()) {
					EmployeeData employee = new EmployeeData();
					employee.setCec_id(rs.getString(3));
					employee.setName(rs.getString(4));
					employee.getProject().setId(rs.getString(1));
					employee.getProject().setName(rs.getString(2));
					employee.setM1(rs.getString(5));
					employee.setM2(rs.getString(6));
					employee.setM3(rs.getString(7));
					employee.setStatus(rs.getString(8));
					data.add(employee);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;

	}

	public static boolean updateRequest(List<EmployeeData> data) { // Used in
																	// reportingmanager/updateRequests.jsp
		boolean result = false;
		Connection con = ConnectionProvider.getCon();
		try {
			int i;
			con.setAutoCommit(false);
			for (i = 0; i < data.size(); i++) {
				String sql = "UPDATE TEMP_RESOURCE_ALLOCATION SET LAST_UPDATED=CURRENT_TIMESTAMP,STATUS=?,COMMENTS=? WHERE TEMP_CEC_ID=? AND PFM_ID=? AND QUARTER=? AND YEAR1=? and group_id=?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, data.get(i).getStatus());
				ps.setString(2, data.get(i).getComment());
				ps.setString(3, data.get(i).getCec_id());
				ps.setString(4, data.get(i).getProject().getId());
				ps.setString(5, data.get(i).getQuarter());
				ps.setString(6, data.get(i).getYear());
				ps.setString(7, data.get(i).getGroupId());
				ps.executeQuery();
				ConnectionProvider.close(ps);
			}
			if (i == data.size())
				con.commit();
			result = true;
			ConnectionProvider.close(con);
		} catch (SQLException e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public static boolean updateRequestNotify(List<EmployeeData> data) { // Used
																			// in
																			// reportingmanager/updateRequests.jsp
		boolean result = false;
		Connection con = ConnectionProvider.getCon();
		try {
			int i;
			con.setAutoCommit(false);
			for (i = 0; i < data.size(); i++) {
				String sql = "update notify set from1=? where status=? and group_id=? and not_desc=? and pfm_id=? and seen_status='unseen' and from1 is null";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, data.get(i).getCec_id());
				ps.setString(2, data.get(i).getStatus());
				ps.setString(3, data.get(i).getGroupId());
				ps.setString(4, data.get(i).getComment());
				ps.setString(5, data.get(i).getProject().getId());
				ps.executeQuery();
				ConnectionProvider.close(ps);
			}
			if (i == data.size())
				con.commit();
			result = true;
			ConnectionProvider.close(con);
		} catch (SQLException e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public static List<String> getProjectMonths(String pfm_id) {
		List<String> months = Collections.emptyList();
		Connection con = ConnectionProvider.getCon();
		String sql = "select months from project_elapse where pfm_id=?";
		PreparedStatement ps;
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, pfm_id);
			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				months = new ArrayList<>();
				while (rs.next()) {
					months.add(rs.getString(1));
				}
			}
			ConnectionProvider.close(con, ps, rs);
		} catch (SQLException e) {
			months = Collections.emptyList();
			e.printStackTrace();
		}
		return months;
	}

	public static List<ProjectData> getTotalEmployees() {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "with perms(cnt) as (select count(unique(cec_id)) from employee), temps(tcnt) as (select count(unique(temp_cec_id)) from temp_employee) select cnt as full_time,tcnt as temp_emp from perms,temps";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			if (rs.isBeforeFirst()) {
				data = new ArrayList<>();
				while (rs.next()) {
					ProjectData project = new ProjectData();
					project.setNoOfEmployees(rs.getString(1));
					project.setTotalEmployees(rs.getString(2));
					data.add(project);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;

	}

	public static List<ProjectData> getAllEmpRegion() {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "with perms(preg,pcnt) as (select substr(region,1,2) as reg,count(unique(cec_id)) from employee_details group by substr(region,1,2)), temps(treg,tcnt) as (select substr(region,1,2) as reg,count(unique(temp_cec_id)) from temp_employee group by substr(region,1,2)) select NVL(preg,treg),NVL(pcnt,0)+NVL(tcnt,0) from perms full outer join temps on preg=treg";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			if (rs.isBeforeFirst()) {
				data = new ArrayList<>();
				while (rs.next()) {
					ProjectData project = new ProjectData();
					project.setRegion(rs.getString(1));
					project.setTotalEmployees(rs.getString(2));
					data.add(project);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;

	}

	public static List<ProjectData> getRMsandEmployeesCount() {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "with cecs(cec,cnt) as (select reporting_manager,count(unique(cec_id)) from employee group by reporting_manager), temp_cecs(tcec,tcnt) as (select reporting_manager,count(unique(temp_cec_id)) from temp_employee group by reporting_manager), perm_ans(pcec,pnm,pcnt) as (select cec_id,display_name,cnt from employee,cecs where cec_id=cec), temp_ans(tcec,tnm,tcnt) as (select cec_id,display_name,tcnt from employee,temp_cecs where cec_id=tcec) select NVL(pcec,tcec),NVL(pnm,tnm),NVL(pcnt,0)+NVL(tcnt,0) from perm_ans full outer join temp_ans on pcec=tcec";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			data = new ArrayList<>();
			while (rs.next()) {
				ProjectData project = new ProjectData();
				project.setRMId(rs.getString(1));
				if (rs.getString(2) == null) {
					project.setRMName("Not assigned to RM");
				} else {
					project.setRMName(rs.getString(2));
				}
				project.setNoOfEmployees(rs.getString(3));
				data.add(project);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;
	}

	public static List<ProjectData> ProjectWiseDetails(String quarter, String year) {
		List<ProjectData> data = Collections.emptyList();
		try {
			String sql = "with pfms(pfm,pcnt) as (select pfm_id,count(unique(cec_id)) from resource_allocation where status='APPROVED' and quarter=? and year1=? group by pfm_id order by pfm_id), temp_pfms(tpfm,tcnt) as (select pfm_id,count(unique(temp_cec_id)) from temp_resource_allocation where status='APPROVED' and quarter=? and year1=? group by pfm_id order by pfm_id), perm_ans(ppfm,ppnme,pcnt) as (select pfm_id,project_name,pcnt from project1,pfms where pfm_id=pfm), temp_ans(tpfm,tpnme,tcnt) as (select pfm_id,project_name,tcnt from project1,temp_pfms where pfm_id=tpfm), fin_ans(pfm,pnme,perms,temps,tots) as (select NVL(ppfm,tpfm),NVL(ppnme,tpnme),NVL(pcnt,0),NVL(tcnt,0),NVL(pcnt,0)+NVL(tcnt,0) from perm_ans full outer join temp_ans on ppfm=tpfm), NUMER(NUM_DT,PFM) AS (SELECT (trunc(sysdate)-to_date(to_char(start_date, 'DD-MM-YYYY'))),PFM_ID FROM PROJECT1,fin_ans WHERE PFM_ID=PFM), DENOM(DEN_DT,PFM) AS (SELECT (to_date(to_char(end_date, 'DD-MM-YYYY'))-to_date(to_char(start_date,'DD-MM-YYYY'))),PFM_ID FROM PROJECT1,fin_ans WHERE PFM_ID=PFM), RESULTS(ANS,PFM) AS (SELECT NUM_DT/DEN_DT,NUMER.PFM FROM NUMER,DENOM WHERE NUMER.PFM=DENOM.PFM) select fin_ans.pfm,pnme,perms,temps,tots,floor(ans*100) as progress from fin_ans,results where fin_ans.pfm=results.pfm";
			Connection conn = ConnectionProvider.getCon();

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, quarter);
			ps.setString(2, year);
			ps.setString(3, quarter);
			ps.setString(4, year);
			ResultSet rs = ps.executeQuery();
			data = new ArrayList<>();
			if (rs.isBeforeFirst()) {
				data = new ArrayList<>();
				while (rs.next()) {
					ProjectData project = new ProjectData();
					project.setId(rs.getString(1));
					project.setName(rs.getString(2));
					project.setTotalEmployees(rs.getString(3));
					project.setNoOfEmployees(rs.getString(4));
					project.setProgress(rs.getString(6));
					data.add(project);
				}
			}
			conn.close();
		} catch (Exception e) {
			data = Collections.emptyList();
			e.printStackTrace();
		}
		return data;

	}

	public static boolean updateNotificationStatus(String group_id) {
		boolean result = false;
		try {
			Connection con = ConnectionProvider.getCon();
			String sql = "update notify set seen_status='seen' where group_id=? and to1 is null";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, group_id);
			con.setAutoCommit(false);

			int rows = ps.executeUpdate();
			if (rows == 1) {
				con.commit();
				result = true;
			}
			ConnectionProvider.close(con, ps);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;

	}

	public static int getNumberofUnreadNotifications() {
		String sql = "select count(*) from notify where to1 is null and seen_status='unseen'";
		int numberofnotification = 0;
		Connection conn = ConnectionProvider.getCon();

		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				numberofnotification = rs.getInt(1);
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return numberofnotification;
	}
}
