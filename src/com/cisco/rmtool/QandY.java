package com.cisco.rmtool;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.omg.PortableServer.ServantRetentionPolicyValue;

public class QandY {

	String m1, m2, m3;
	String QFY;
	String quarter;
	String year;
	String y1, y2, y3;

	public QandY(String data) {
		QFY = data;
		Calendar c = Calendar.getInstance();
		int y = c.get(Calendar.YEAR);
		String[] quarterYear = data.split("FY");
		if (quarterYear.length > 1) {
			quarterYear[1] = "" + (y / 100) + quarterYear[1];
			String actualYear = "";
			quarter = quarterYear[0];
			year = quarterYear[1];
			if (quarterYear[0].equalsIgnoreCase("Q1")) {
				actualYear = "" + (Integer.parseInt(quarterYear[1]) - 1);
				m1 = "08";
				y1 = actualYear;
				m2 = "09";
				y2 = actualYear;
				m3 = "10";
				y3 = actualYear;
			} else if (quarterYear[0].equalsIgnoreCase("Q2")) {
				actualYear = "" + (Integer.parseInt(quarterYear[1]) - 1);
				m1 = "11";
				y1 = actualYear;
				m2 = "12";
				y2 = actualYear;
				m3 = "01";
				y3 = quarterYear[1];
			} else if (quarterYear[0].equalsIgnoreCase("Q3")) {
				m1 = "02";
				y1 = quarterYear[1];
				m2 = "03";
				y2 = quarterYear[1];
				m3 = "04";
				y3 = quarterYear[1];
			} else if (quarterYear[0].equalsIgnoreCase("Q4")) {
				m1 = "05";
				y1 = quarterYear[1];
				m2 = "06";
				y2 = quarterYear[1];
				m3 = "07";
				y3 = quarterYear[1];
			} else {
				m1 = "";
				m2 = "";
				m3 = "";
			}
		}
	}

	public QandY(String quarter, String year) {
		String data = quarter + "FY" + year.charAt(2) + year.charAt(3);
		setValues(data);
	}

	public String getQFY() {
		return QFY;
	}

	public void setQFY(String qFY) {
		QFY = qFY;
	}

	public String getY1() {
		return y1;
	}

	public String getY2() {
		return y2;
	}

	public String getY3() {
		return y3;
	}

	public String getYear() {
		return year;
	}

	public String getM1() {
		return m1;
	}

	public String getM2() {
		return m2;
	}

	public String getM3() {
		return m3;
	}

	public String getQuarter() {
		return quarter;
	}

	public void setM1(String m1) {
		this.m1 = m1;
	}

	public void setM2(String m2) {
		this.m2 = m2;
	}

	public void setM3(String m3) {
		this.m3 = m3;
	}

	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	public void setY1(String y1) {
		this.y1 = y1;
	}

	public void setY2(String y2) {
		this.y2 = y2;
	}

	public void setY3(String y3) {
		this.y3 = y3;
	}

	public void setYear(String year) {
		this.year = year;
	}

	private void setValues(String data) {
		QFY = data;
		Calendar c = Calendar.getInstance();
		int y = c.get(Calendar.YEAR);
		String[] quarterYear = data.split("FY");
		if (quarterYear.length > 1) {
			quarterYear[1] = "" + (y / 100) + quarterYear[1];
			String actualYear = "";
			quarter = quarterYear[0];
			year = quarterYear[1];
			if (quarterYear[0].equalsIgnoreCase("Q1")) {
				actualYear = "" + (Integer.parseInt(quarterYear[1]) - 1);
				m1 = "08";
				y1 = actualYear;
				m2 = "09";
				y2 = actualYear;
				m3 = "10";
				y3 = actualYear;
			} else if (quarterYear[0].equalsIgnoreCase("Q2")) {
				actualYear = "" + (Integer.parseInt(quarterYear[1]) - 1);
				m1 = "11";
				y1 = actualYear;
				m2 = "12";
				y2 = actualYear;
				m3 = "01";
				y3 = quarterYear[1];
			} else if (quarterYear[0].equalsIgnoreCase("Q3")) {
				m1 = "02";
				y1 = quarterYear[1];
				m2 = "03";
				y2 = quarterYear[1];
				m3 = "04";
				y3 = quarterYear[1];
			} else if (quarterYear[0].equalsIgnoreCase("Q4")) {
				m1 = "05";
				y1 = quarterYear[1];
				m2 = "06";
				y2 = quarterYear[1];
				m3 = "07";
				y3 = quarterYear[1];
			} else {
				m1 = "";
				m2 = "";
				m3 = "";
			}
		}

	}
	
	public static String getCombi(int month, int year) {
		switch (month) {
		case 0:
			return "Q2FY" + (year % 100);
		case 1:
			return "Q3FY" + (year % 100);
		case 2:
			return "Q3FY" + (year % 100);
		case 3:
			return "Q3FY" + (year % 100);
		case 4:
			return "Q4FY" + (year % 100);
		case 5:
			return "Q4FY" + (year % 100);
		case 6:
			return "Q4FY" + (year % 100);
		case 7:
			return "Q1FY" + ((year % 100) + 1);
		case 8:
			return "Q1FY" + ((year % 100) + 1);
		case 9:
			return "Q1FY" + ((year % 100) + 1);
		case 10:
			return "Q2FY" + ((year % 100) + 1);
		case 11:
			return "Q2FY" + ((year % 100) + 1);
		default:
			return "";
		}
	}


	public static String getCombi(String month, String year) {
		switch (Integer.parseInt(month)) {
		case 0:
			return "Q2FY" + (Integer.parseInt(year) % 100);
		case 1:
			return "Q3FY" + (Integer.parseInt(year) % 100);
		case 2:
			return "Q3FY" + (Integer.parseInt(year) % 100);
		case 3:
			return "Q3FY" + (Integer.parseInt(year) % 100);
		case 4:
			return "Q4FY" + (Integer.parseInt(year) % 100);
		case 5:
			return "Q4FY" + (Integer.parseInt(year) % 100);
		case 6:
			return "Q4FY" + (Integer.parseInt(year) % 100);
		case 7:
			return "Q1FY" + ((Integer.parseInt(year) % 100) + 1);
		case 8:
			return "Q1FY" + ((Integer.parseInt(year) % 100) + 1);
		case 9:
			return "Q1FY" + ((Integer.parseInt(year) % 100) + 1);
		case 10:
			return "Q2FY" + ((Integer.parseInt(year) % 100) + 1);
		case 11:
			return "Q2FY" + ((Integer.parseInt(year) % 100) + 1);
		default:
			return "";
		}
	}

	public static List<String> getProjectQuarters(List<String> months_year) {
		List<String> list = Collections.emptyList();

		// Record encountered Strings in HashSet.
		HashSet<String> set = new HashSet<>();
		if (months_year.size() > 0)
			list = new ArrayList<>();
		// Loop over argument list.
		for (String item : months_year) {
			String[] my = item.split("-");
			String data = QandY.getCombi((Integer.parseInt(my[0]) - 1) + "", my[1]);

			// If String is not in set, add it to the list and the set.
			if (!set.contains(data)) {
				list.add(data);
				set.add(data);
			}
		}
		return list;
	}

}
