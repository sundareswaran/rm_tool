package com.cisco.rmtool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Notification {
	public static List<NotificationData> getUnreadNotifications(String cec_id) {	//Used in projectmanager/notifications.jsp | reportingmanager/notifications.jsp
		List<NotificationData> unread = Collections.emptyList();
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "with noti(pfm,tot) as (select UNIQUE(group_id),count(not_id) as tot_res_req from notify where to1=? and seen_status='unseen' group by group_id), grp_id(pfm,grp,dte) as (select unique(pfm_id),group_id,(to_date(to_char(sent, 'DD-MM-YYYY'))) from notify,noti where group_id=pfm and to1=?), tme(grp,tm) as (select UNIQUE(group_id), max(EXTRACT(HOUR FROM sent) || ':' ||EXTRACT(MINUTE FROM sent)|| ':' ||EXTRACT(SECOND FROM sent)) from notify,grp_id where pfm_id=pfm and group_id=grp and to1=? group by group_id), pnme(pfm,pnm,pmne) as (select unique(pfm_id),project_name,project_manager from project1,grp_id where pfm_id=pfm), pjmnme(cec,pmnme) as (select unique(cec_id),display_name from employee,pnme where pmne=cec_id) select unique(grp_id.pfm),pnm,pmnme,tot,grp_id.grp,dte,tm from noti,pnme,grp_id,pjmnme,tme where grp_id.pfm=pnme.pfm and pnme.pmne=pjmnme.cec and tme.grp=grp_id.grp and noti.pfm=grp_id.grp order by dte desc, tm desc";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, cec_id);
			ps.setString(2, cec_id);
			ps.setString(3, cec_id);
			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				unread = new ArrayList<>();
				while (rs.next()) {
					NotificationData notification = new NotificationData();
					ProjectData project = new ProjectData();
					project.setId(rs.getString(1));
					project.setName(rs.getString(2));
					project.setProjectManager(rs.getString(3));
					notification.setProject(project);
					notification.setTotResReq(rs.getString(4));
					notification.setGroupId(rs.getString(5));
					notification.setDate(rs.getString(6));
					notification.setTime(rs.getString(7));
					unread.add(notification);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
			unread = Collections.emptyList();
		}
		return unread;
	}
	


	public static List<NotificationData> getReadNotifications(String cec_id) {	//Used in projectmanager/notifications.jsp | reportingmanager/notifications.jsp
		List<NotificationData> unread = Collections.emptyList();
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "with noti(pfm,tot) as (select UNIQUE(group_id),count(not_id) as tot_res_req from notify where to1=? and seen_status='seen' group by group_id), grp_id(pfm,grp,dte) as (select unique(pfm_id),group_id,(to_date(to_char(sent, 'DD-MM-YYYY'))) from notify,noti where group_id=pfm and to1=?), tme(grp,tm) as (select UNIQUE(group_id), max(EXTRACT(HOUR FROM sent) || ':' ||EXTRACT(MINUTE FROM sent)|| ':' ||EXTRACT(SECOND FROM sent)) from notify,grp_id where pfm_id=pfm and group_id=grp and to1=? group by group_id), pnme(pfm,pnm,pmne) as (select unique(pfm_id),project_name,project_manager from project1,grp_id where pfm_id=pfm), pjmnme(cec,pmnme) as (select unique(cec_id),display_name from employee,pnme where pmne=cec_id) select unique(grp_id.pfm),pnm,pmnme,tot,grp_id.grp,dte,tm from noti,pnme,grp_id,pjmnme,tme where grp_id.pfm=pnme.pfm and pnme.pmne=pjmnme.cec and tme.grp=grp_id.grp and noti.pfm=grp_id.grp order by dte desc, tm desc";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, cec_id);
			ps.setString(2, cec_id);
			ps.setString(3, cec_id);
			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				unread = new ArrayList<>();
				while (rs.next()) {
					NotificationData notification = new NotificationData();
					ProjectData project = new ProjectData();
					project.setId(rs.getString(1));
					project.setName(rs.getString(2));
					project.setProjectManager(rs.getString(3));
					notification.setProject(project);
					notification.setTotResReq(rs.getString(4));
					notification.setGroupId(rs.getString(5));
					notification.setDate(rs.getString(6));
					notification.setTime(rs.getString(7));
					unread.add(notification);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
			unread = Collections.emptyList();
		}
		return unread;
	}

	public static List<NotificationData> getNotifications(String last) {
		List<NotificationData> data = Collections.emptyList();
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				data = new ArrayList<>();
				while (rs.next()) {
					NotificationData notification = new NotificationData();

					data.add(notification);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
			data = Collections.emptyList();
		}
		return data;
	}
	
	public static List<NotificationData> getUnreadPMNotifications(String cec_id) {
		List<NotificationData> unread = Collections.emptyList();
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "select from1,notify.pfm_id,project_name,group_id,count(group_id) from notify,project1 where to1=? and seen_status='unseen' and project1.pfm_id=notify.pfm_id group by group_id,from1,project_name,notify.pfm_id order by max(sent) desc";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, cec_id);
			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				unread = new ArrayList<>();
				while (rs.next()) {
					NotificationData notification = new NotificationData();
					ProjectData project = new ProjectData();
					EmployeeData employee = new EmployeeData();
					employee.setReportingManager(rs.getString(1));
					project.setId(rs.getString(2));
					project.setPrjName(rs.getString(3));
					notification.setGroupId(rs.getString(4));
					notification.setCount(rs.getString(5));
					notification.setProject(project);
					notification.setEmployee(employee);
					unread.add(notification);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
			unread = Collections.emptyList();
		}
		return unread;
	}
	
	public static List<NotificationData> getReadPMNotifications(String cec_id) {
		List<NotificationData> unread = Collections.emptyList();
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "select from1,notify.pfm_id,project_name,group_id,count(group_id) from notify,project1 where to1=? and seen_status='seen' and project1.pfm_id=notify.pfm_id group by group_id,from1,project_name,notify.pfm_id order by max(sent) desc";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, cec_id);
			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				unread = new ArrayList<>();
				while (rs.next()) {
					NotificationData notification = new NotificationData();
					ProjectData project = new ProjectData();
					EmployeeData employee = new EmployeeData();
					employee.setReportingManager(rs.getString(1));
					project.setId(rs.getString(2));
					project.setPrjName(rs.getString(3));
					notification.setGroupId(rs.getString(4));
					notification.setCount(rs.getString(5));
					notification.setProject(project);
					notification.setEmployee(employee);
					unread.add(notification);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
			unread = Collections.emptyList();
		}
		return unread;
	}
	
	public static List<NotificationData> getAdminUnreadNotifications() {	//Used in projectmanager/notifications.jsp | reportingmanager/notifications.jsp
		List<NotificationData> unread = Collections.emptyList();
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "with noti(pfm,tot) as (select UNIQUE(group_id),count(not_id) as tot_res_req from notify where to1 is null and seen_status='unseen' group by group_id), grp_id(pfm,grp,dte) as (select unique(pfm_id),group_id,(to_date(to_char(sent, 'DD-MM-YYYY'))) from notify,noti where group_id=pfm and to1 is null), tme(grp,tm) as (select UNIQUE(group_id), max(EXTRACT(HOUR FROM sent) || ':' ||EXTRACT(MINUTE FROM sent)|| ':' ||EXTRACT(SECOND FROM sent)) from notify,grp_id where pfm_id=pfm and group_id=grp and to1 is null group by group_id), pnme(pfm,pnm,pmne) as (select unique(pfm_id),project_name,project_manager from project1,grp_id where pfm_id=pfm), pjmnme(cec,pmnme) as (select unique(cec_id),display_name from employee,pnme where pmne=cec_id) select unique(grp_id.pfm),pnm,pmnme,tot,grp_id.grp,dte,tm from noti,pnme,grp_id,pjmnme,tme where grp_id.pfm=pnme.pfm and pnme.pmne=pjmnme.cec and tme.grp=grp_id.grp and noti.pfm=grp_id.grp order by dte desc,tm desc";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				unread = new ArrayList<>();
				while (rs.next()) {
					NotificationData notification = new NotificationData();
					ProjectData project = new ProjectData();
					project.setId(rs.getString(1));
					project.setPrjName(rs.getString(2));
					project.setProjectManager(rs.getString(3));
					notification.setCount(rs.getString(4));
					notification.setGroupId(rs.getString(5));
					notification.setDate(rs.getString(6));
					notification.setTime(rs.getString(7));
					notification.setProject(project);
					unread.add(notification);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
			unread = Collections.emptyList();
		}
		return unread;
	}
	
	public static List<NotificationData> getAdminReadNotifications() {	//Used in projectmanager/notifications.jsp | reportingmanager/notifications.jsp
		List<NotificationData> unread = Collections.emptyList();
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "with noti(pfm,tot) as (select UNIQUE(group_id),count(not_id) as tot_res_req from notify where to1 is null and seen_status='seen' group by group_id), grp_id(pfm,grp,dte) as (select unique(pfm_id),group_id,(to_date(to_char(sent, 'DD-MM-YYYY'))) from notify,noti where group_id=pfm and to1 is null), tme(grp,tm) as (select UNIQUE(group_id), max(EXTRACT(HOUR FROM sent) || ':' ||EXTRACT(MINUTE FROM sent)|| ':' ||EXTRACT(SECOND FROM sent)) from notify,grp_id where pfm_id=pfm and group_id=grp and to1 is null group by group_id), pnme(pfm,pnm,pmne) as (select unique(pfm_id),project_name,project_manager from project1,grp_id where pfm_id=pfm), pjmnme(cec,pmnme) as (select unique(cec_id),display_name from employee,pnme where pmne=cec_id) select unique(grp_id.pfm),pnm,pmnme,tot,grp_id.grp,dte,tm from noti,pnme,grp_id,pjmnme,tme where grp_id.pfm=pnme.pfm and pnme.pmne=pjmnme.cec and tme.grp=grp_id.grp and noti.pfm=grp_id.grp order by dte desc,tm desc";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				unread = new ArrayList<>();
				while (rs.next()) {
					NotificationData notification = new NotificationData();
					ProjectData project = new ProjectData();
					project.setId(rs.getString(1));
					project.setPrjName(rs.getString(2));
					project.setProjectManager(rs.getString(3));
					notification.setCount(rs.getString(4));
					notification.setGroupId(rs.getString(5));
					notification.setDate(rs.getString(6));
					notification.setTime(rs.getString(7));
					notification.setProject(project);
					unread.add(notification);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
			unread = Collections.emptyList();
		}
		return unread;
	}
	
	public static List<NotificationData> getEmployeeUnreadNotfications(String CEC_ID) {
		List<NotificationData> unread = Collections.emptyList();
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "with notifs(to1,frm,pfm,sta,des,sen,gp) as (select to1,from1,pfm_id,status,not_desc,sent,group_id from notify where to1=? and seen_status='unseen'), part_ans(frm,pfm,m1,m2,m3,sta,cmt,sen) as (select frm,pfm_id,m1,m2,m3,status,comments,sen from resource_allocation,notifs where cec_id=to1 and status=sta and pfm_id=pfm and comments=des and last_updated=sen and group_id=gp), pm_cec(pmcec,pf) as (select project_manager,pfm_id from project1,part_ans where pfm_id=pfm), pm_nme(pmcec,pmnme,pf) as (select cec_id,display_name,pf from employee,pm_cec where cec_id=pmcec), ans(frm,pfm,pmcec,pmnme,m1,m2,m3,sta,cmt,sen) as (select frm,pfm,pmcec,pmnme,m1,m2,m3,sta,cmt,sen from pm_nme,part_ans where pf=pfm), rmnm(rcec,rmnm) as (select cec_id,display_name from employee,part_ans where cec_id=frm) select unique(rcec),rmnm,pfm,pmcec,pmnme,m1,m2,m3,sta,cmt,sen from ans,rmnm where frm=rcec order by sen desc";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_ID);
			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				unread = new ArrayList<>();
				while (rs.next()) {
					NotificationData notification = new NotificationData();
					ProjectData project = new ProjectData();
					EmployeeData employee = new EmployeeData();
					notification.setSenderId(rs.getString(1));
					notification.setSenderName(rs.getString(2).split("\\(")[0]);
					project.setPrjName(ProjectManagerDao.getProjectName(rs.getString(3)));
					project.setIdProjectManager(rs.getString(4));
					project.setProjectManager(rs.getString(5).split("\\(")[0]);
					project.setM1(rs.getString(6));
					project.setM2(rs.getString(7));
					project.setM3(rs.getString(8));
					employee.setStatus(rs.getString(9));
					notification.setMessage(rs.getString(10));
					notification.setEmployee(employee);
					notification.setProject(project);
					unread.add(notification);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
			unread = Collections.emptyList();
		}
		return unread;
	}
	
	public static List<NotificationData> getEmployeeReadNotfications(String CEC_ID) {
		List<NotificationData> read = Collections.emptyList();
		try {
			Connection conn = ConnectionProvider.getCon();
			String sql = "with notifs(to1,frm,pfm,sta,des,sen,gp) as (select to1,from1,pfm_id,status,not_desc,sent,group_id from notify where to1=? and seen_status='seen'), part_ans(frm,pfm,m1,m2,m3,sta,cmt,sen) as (select frm,pfm_id,m1,m2,m3,status,comments,sen from resource_allocation,notifs where cec_id=to1 and status=sta and pfm_id=pfm and comments=des and last_updated=sen and group_id=gp), pm_cec(pmcec,pf) as (select project_manager,pfm_id from project1,part_ans where pfm_id=pfm), pm_nme(pmcec,pmnme,pf) as (select cec_id,display_name,pf from employee,pm_cec where cec_id=pmcec), ans(frm,pfm,pmcec,pmnme,m1,m2,m3,sta,cmt,sen) as (select frm,pfm,pmcec,pmnme,m1,m2,m3,sta,cmt,sen from pm_nme,part_ans where pf=pfm), rmnm(rcec,rmnm) as (select cec_id,display_name from employee,part_ans where cec_id=frm) select unique(rcec),rmnm,pfm,pmcec,pmnme,m1,m2,m3,sta,cmt,sen from ans,rmnm where frm=rcec order by sen desc";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, CEC_ID);
			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) {
				read = new ArrayList<>();
				while (rs.next()) {
					NotificationData notification = new NotificationData();
					ProjectData project = new ProjectData();
					EmployeeData employee = new EmployeeData();
					notification.setSenderId(rs.getString(1));
					notification.setSenderName(rs.getString(2).split("\\(")[0]);
					project.setPrjName(ProjectManagerDao.getProjectName(rs.getString(3)));
					project.setIdProjectManager(rs.getString(4));
					project.setProjectManager(rs.getString(5).split("\\(")[0]);
					project.setM1(rs.getString(6));
					project.setM2(rs.getString(7));
					project.setM3(rs.getString(8));
					employee.setStatus(rs.getString(9));
					notification.setMessage(rs.getString(10));
					notification.setEmployee(employee);
					notification.setProject(project);
					read.add(notification);
				}
			}
			ConnectionProvider.close(conn, ps, rs);
		} catch (Exception e) {
			e.printStackTrace();
			read = Collections.emptyList();
		}
		return read;
	}

}
