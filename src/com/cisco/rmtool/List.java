package com.cisco.rmtool;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

/**
 * Servlet implementation class List
 */
@WebServlet("/List")
public class List extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public List() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String year = request.getParameter("year");
		String quarter = request.getParameter("quarter");
		response.setContentType("text/application");
		PrintWriter out = response.getWriter();
		if (request.getParameterValues("data[]") != null) {

			String[] arr = request.getParameterValues("data[]");
			String s = "";
			for (int i = 0; i < arr.length; i++) {
				if (i > 0)
					s = s + ",";
				s = s + arr[i];
			}

			java.util.List<EmployeeData> data = ProjectManagerDao.getEmployeeForSkill(s, year, quarter);
			StringBuilder builder = new StringBuilder("");
			for (int i = 0; i < data.size(); i++) {
				builder.append(data.get(i).getCec_id());
				builder.append("|");
				builder.append(" &" + data.get(i).getName() + "& ");
				builder.append(" ?" + data.get(i).getReportingManager() + "? ");
				// Add id to the title 
				// This next line has the skills in it add it to the title
				//builder.append("" + data.get(i).getSkills() + "");
				if (i < (data.size() - 1))
					builder.append("|");
			}
			out.println(builder.toString());
		} else {
			Gson gson = new Gson();
			String[] arr = gson.fromJson(ProjectManagerDao.getSkillsJson(), String[].class);
			String s = "";
			for (int i = 0; i < arr.length; i++) {
				if (i > 0)
					s = s + ",";
				s = s + arr[i];
			}
			java.util.List<EmployeeData> data = ProjectManagerDao.getEmployeeForSkill(s, year, quarter);
			StringBuilder builder = new StringBuilder("");
			for (int i = 0; i < data.size(); i++) {
				builder.append(data.get(i).getCec_id());
				builder.append("|");
				builder.append(" &" + data.get(i).getName() + "& ");
				builder.append(" ?" + data.get(i).getReportingManager() + "? ");
				// Add id to the title 
				// This next line has the skills in it add it to the title
				// builder.append("" + data.get(i).getSkills() + "");
				if (i < (data.size() - 1))
					builder.append("|");
			}
			out.println(builder.toString());
		}
	}

}
